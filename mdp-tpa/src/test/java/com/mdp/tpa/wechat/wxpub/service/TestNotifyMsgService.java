package com.mdp.tpa.wechat.wxpub.service;

import com.mdp.msg.client.PushNotifyMsgService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * 发送消息通知
 ***/

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
public class TestNotifyMsgService {

	@Autowired
	PushNotifyMsgService pushNotifyMsgService;

	@Test
	public void send() throws InterruptedException {

		pushNotifyMsgService.pushMsg("4hinb8m16","4hinb8m16","您好啊","");
		Thread.sleep(1000000);
	}
}
