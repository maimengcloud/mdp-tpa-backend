package com.mdp.tpa.wechat.wxpub.service.pub;

import static org.junit.Assert.assertEquals;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.mdp.tpa.wechat.api.MessageSendService;
import com.mdp.tpa.wechat.api.WxUserService;
import com.mdp.tpa.wechat.entity.AccessToken;
import com.mdp.tpa.wechat.wxpub.service.WxMsgTemplateService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.mdp.core.entity.Tips;
import com.mdp.tpa.wechat.api.AccessTokenService;

@RunWith(SpringJUnit4ClassRunner.class) 
@SpringBootTest
public class TestWxTemplateMsgSendService {
	@Autowired
	MessageSendService messageSendService;
	@Autowired
	AccessTokenService accessTokenService;
	
	@Autowired
	WxUserService wxUserService;

	@Autowired
	WxMsgTemplateService wxMsgTemplateService;
	 
	@Test
	public void sendTemplateMessage(){
		AccessToken accessTokenObj=accessTokenService.getAccessToken( );
		String accessToken=accessTokenObj.getAccessToken();

		Map<String,Object> userResult=wxUserService.getUserOpenidList(  "");
		assertEquals(true, userResult.containsKey("data"));
		Map<String,Object> dataMap=(Map<String, Object>) userResult.get("data");
		List<String> openids=(List<String>) dataMap.get("openid"); 
		assertEquals(true, openids!=null && openids.size()>0);
		
		String openid=openids.get(0);
		//openid="o7Q6N588woIwgkdHyMloMvCLFIfM";
		String templateId="MueCKGqIXu2hStRmq9XORHr8l6zRPeIe8-HuG8sVDHE";
		String url="https://wwww.maimengcloud.com";
		Map<String,Object> data=new HashMap<>();
		Map<String,Object> first=new HashMap<>();
		Map<String,Object> keyword1=new HashMap<>();
		Map<String,Object> keyword2=new HashMap<>();
		first.put("value", "您的案件已完结");
		keyword1.put("value", "送审阶段");
		keyword2.put("value", "chentian");
		data.put("first", first);
		data.put("keyword1", keyword1);
		data.put("keyword2", keyword2);
		Map<String,Object> result=messageSendService.sendTemplateMessage( templateId, openid, url, data);
		System.out.println(result);
		assertEquals(true, 0==(int)result.get("errcode"));
		
	
	}
	@Test
	/**
	 * 结算通知
	 */
	public void sendTemplateMessage_taskSettle(){
		String authId="wx_pub_test_001";

		Map<String,Object> userResult=wxUserService.getUserOpenidList( "");
		assertEquals(true, userResult.containsKey("data"));
		Map<String,Object> dataMap=(Map<String, Object>) userResult.get("data");
		List<String> openids=(List<String>) dataMap.get("openid"); 
		assertEquals(true, openids!=null && openids.size()>0);
		
		String openid=openids.get(0);
		String templateId="7VywzVOBccneZw1plwlZMX-xIAQhgpBg7zlI2rLOxMY";
		String url="https://wwww.qingqinkj.com";
		Map<String,Object> data=new HashMap<>();
		Map<String,Object> user=new HashMap<>();
		user.put("userid", "chenyc-001");
		user.put("username", "chenyc"); 
		data.put("user", user);
		Map<String,Object> result=messageSendService.sendTemplateMessage( templateId, openid, url, data);
		System.out.println(result);
		assertEquals(true, 0==(int)result.get("errcode"));
		
	
	}

	
}
