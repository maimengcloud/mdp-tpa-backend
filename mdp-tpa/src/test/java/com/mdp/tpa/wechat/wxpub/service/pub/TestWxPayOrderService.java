package com.mdp.tpa.wechat.wxpub.service.pub;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mdp.core.entity.Tips;
import com.mdp.core.err.BizException;
import com.mdp.core.utils.RequestUtils;

import com.mdp.safe.client.entity.User;
import com.mdp.safe.client.utils.LoginUtils;
import com.mdp.tpa.client.entity.AppTpPay;
import com.mdp.tpa.client.service.AppTpPayService;
import com.mdp.tpa.pay.cm.entity.Orders;
import com.mdp.tpa.pay.cm.entity.UniOrder;
import com.mdp.tpa.pay.cm.service.OrdersService;
import com.mdp.tpa.pay.wxpay.ctrl.WxpayJsapiController;
import com.mdp.tpa.pay.wxpay.service.WxpayPubService;
import com.mdp.tpa.wechat.api.PayService;
import com.mdp.tpa.wechat.wxpaysdk.MyWxPayConfig;
import com.mdp.tpa.wechat.wxpaysdk.WXPayConfig;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.web.bind.annotation.RequestBody;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;


@RunWith(SpringJUnit4ClassRunner.class) 
@SpringBootTest
public class TestWxPayOrderService {
    @Autowired
    WxpayPubService  wxpayPubService;

    ObjectMapper objectMapper=new ObjectMapper();

    @Autowired
    private OrdersService ordersService;


    private Logger logger= LoggerFactory.getLogger(WxpayJsapiController.class);

    @Autowired
    private AppTpPayService appTpPayService;

    @Autowired
    private PayService payService;

    @Test
    public void uniOrder( ) {
        UniOrder order=new UniOrder();

        String authId="mmxmcloud_wxa";
        String tpOpenid="orpmc5JLPJjY-OAqD47LJKVBEhdE";//tpaOpenid -> orpmc5JLPJjY-OAqD47LJKVBEhdE
        order.setId("xxxxdafdfadggrasdfdafsafdfafda");
        order.setOtype("2");

        String outTradeNo=order.getId();
        Map<String,Object> result=new HashMap<String,Object>();
        Map<String,Object> data=new HashMap<String,Object>();

        Tips tips=new Tips("下单成功");
        logger.debug("收到客户端上送订单消息：订单号：" + order.getId() );
        Orders orders = ordersService.getOrders(order);
        try {
            AppTpPay app=wxpayPubService.getAppTpPayByAuthId(authId);
            WXPayConfig config=new MyWxPayConfig(app.getAppid(),app.getPayMchid(),app.getPayKey());
            config.setNotifyUrl(app.getPayNotifyUrl());
            String body=orders.getName();
            String attach=String.format("{\"otype\":\"%s\",\"payAuthId\":\"%s\"}",order.getOtype(),app.getPayAuthId());
            String spbillCreateIp= RequestUtils.getIpAddr(RequestUtils.getRequest());
            payService.unifiedOrder(config,outTradeNo,body,attach,spbillCreateIp,wxpayPubService.DTI100(orders.getPayAt()),"",tpOpenid);
        } catch (BizException e) {
            e.printStackTrace();
            logger.error("执行异常",e);
            tips=e.getTips();
        }catch (Exception e) {
            tips.setErrMsg(e.getMessage());
            e.printStackTrace();
            logger.error("执行异常",e);
        }
        result.put("tips", tips);
        result.put("data", data);
    }
}
