package com.mdp.tpa.wechat.wxpub.service.pub;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.mdp.tpa.wechat.api.AccessTokenService;
import com.mdp.tpa.wechat.api.MessageSendService;
import com.mdp.tpa.wechat.api.WxUserService;
import com.mdp.tpa.wechat.entity.AccessToken;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class) 
@SpringBootTest
public class TestWxMsgSendService {
	@Autowired
	MessageSendService messageSendService;
	@Autowired
	AccessTokenService accessTokenService;
	
	@Autowired
	WxUserService wxUserService;

	@Test
	public void text(){
		List<String> openids=new ArrayList<>();
		openids.add("xxxxxxxxxxxx");
		openids.add("dfadfdasfdf");
		String content="xxxxxxxxxxxx";
		Map<String,Object> result=messageSendService.sendTextByOpenids(  content, openids); 
		System.out.println(result);
		assertEquals(true,  0==(int)result.get("errcode"));
		
	
	}
	@Test
	public void mpnews(){
		Map<String,Object> userResult=wxUserService.getUserOpenidList(  "");
		assertEquals(true, userResult.containsKey("data"));
		Map<String,Object> dataMap=(Map<String, Object>) userResult.get("data");
		List<String> openids=(List<String>) dataMap.get("openid"); 
		String content="xxxxxxxxxxxx";
		Map<String,Object> result=messageSendService.sendMpNewsByOpenids( content, openids); 
		System.out.println(result);
		assertEquals(true,  0==(int)result.get("errcode"));
		
	
	}

	@Test
	public void setTextToOpenids(){
		Map<String,Object> userResult=wxUserService.getUserOpenidList(  "");
		assertEquals(true, userResult.containsKey("data"));
		Map<String,Object> dataMap=(Map<String, Object>) userResult.get("data");
		List<String> openids=(List<String>) dataMap.get("openid"); 
		String content="xxxxxxxxxxxx";
		Map<String,Object> result=messageSendService.sendTextByOpenids(  content, openids); 
		System.out.println(result);
		assertEquals(true,  0==(int)result.get("errcode"));
		
	
	}

	@Test
	public void setTextToOpenid(){
		AccessToken accessTokenObj=accessTokenService.getAccessToken( );
		String accessToken=accessTokenObj.getAccessToken(); 
		Map<String,Object> userResult=wxUserService.getUserOpenidList(  "");
		assertEquals(true, userResult.containsKey("data"));
		Map<String,Object> dataMap=(Map<String, Object>) userResult.get("data");
		List<String> openids=(List<String>) dataMap.get("openid"); 
		String content="xxxxxxxxxxxx";
		Map<String,Object> result=messageSendService.sendTextByOpenid(  content, openids.get(0)); 
		System.out.println(result);
		assertEquals(true,  0==(int)result.get("errcode"));
		
	
	}
	@Test
	public void sendTextToAll(){
		AccessToken accessTokenObj=accessTokenService.getAccessToken( );
		String accessToken=accessTokenObj.getAccessToken(); 
		 
		String content="xxxxxxxxxxxx";
		Map<String,Object> result=messageSendService.sendTextToAll(  content);
		System.out.println(result);
		assertEquals(true,  0==(int)result.get("errcode"));
		
	
	}
	@Test
	public void sendTextToAll2(){
		AccessToken accessTokenObj=accessTokenService.getAccessToken( );
		String accessToken=accessTokenObj.getAccessToken(); 
		 
		String content="xxxxxxxxxxxx";
		Map<String,Object> result=messageSendService.sendTextToAll(  content);
		System.out.println(result);
		assertEquals(true, 0==(int)result.get("errcode"));
		
	
	}
}
