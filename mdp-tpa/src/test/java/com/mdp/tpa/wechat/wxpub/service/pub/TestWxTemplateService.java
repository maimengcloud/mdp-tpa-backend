package com.mdp.tpa.wechat.wxpub.service.pub;

import com.mdp.tpa.wechat.wxpub.service.WxMsgTemplateService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.HashMap;
import java.util.Map;

@RunWith(SpringJUnit4ClassRunner.class) 
@SpringBootTest
public class TestWxTemplateService {

    @Autowired
    WxMsgTemplateService templateService;


    @Test
    public void sendTplMsg(){
        Map<String,Object> msg=new HashMap<>();
        msg.put("tplType","taskNotify");
        msg.put("toUserid","superAdmin");
        msg.put("toUsername","超级管理员");
        msg.put("fromType","notifyMsg");
        msg.put("msg","你好,xxxxxxxxxxxxxxxxxxxfffffffffff");
        templateService.sendTplMsg(msg);
    }

}
