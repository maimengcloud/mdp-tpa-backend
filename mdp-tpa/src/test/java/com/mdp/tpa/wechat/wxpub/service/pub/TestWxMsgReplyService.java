package com.mdp.tpa.wechat.wxpub.service.pub;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.mdp.tpa.wechat.api.MessageReplyService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class) 
@SpringBootTest
public class TestWxMsgReplyService {
	@Autowired
	MessageReplyService messageReplyService;
	@Test
	public void text(){
		String accessToken="xxfafdf";
		String toUserName="xx";
		String fromUserName="xxxxx";
		Integer createTime=100000000;
		String content="xxxxxxxxxxxx";
		String result=messageReplyService.text( toUserName, fromUserName, createTime, content);
		assertEquals(true, result.length()>10);
		System.out.println(result);
	
	}
	
	@Test
	public void image(){
		String accessToken="xxfafdf";
		String toUserName="xx";
		String fromUserName="xxxxx";
		Integer createTime=100000000;
		String mediaId="xxxxxxxxxxxx";
		String result=messageReplyService.image( toUserName, fromUserName, createTime, mediaId);
		assertEquals(true, result.length()>10);
		System.out.println(result);

	} 
	
	@Test
	public void news(){
		
		/**
		 * itemFormat, map.get("archiveTitle"),map.get("archiveAbstract"),map.get("titleImgUrl"),map.get("url")
		 */
		String accessToken="xxfafdf";
		String toUserName="xx";
		String fromUserName="xxxxx";
		Integer createTime=100000000; 
		List<Map<String,Object>> items=new ArrayList<>();
		Map<String,Object> item=new HashMap<>();
		item.put("archiveTitle", "titel1"); 
		item.put("archiveAbstract", "Descriptionxxxxxxxx"); 
		item.put("url", "Urlxxxxxxxxxx"); 
		item.put("titleImgUrl", "PicUrlxxxxxxx");
		items.add(item);
		Map<String,Object> item2=new HashMap<>();
		item2.put("archiveTitle", "titel1"); 
		item2.put("archiveAbstract", "Descriptionxxxxxxxx"); 
		item2.put("url", "Urlxxxxxxxxxx"); 
		item2.put("titleImgUrl", "PicUrlxxxxxxx");
		items.add(item2);
		String result=messageReplyService.news( toUserName, fromUserName, createTime, items);
		assertEquals(true, result.length()>10);
		System.out.println(result);

	} 
}
