package com.mdp.tpa.wechat.wxpub.service;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * WxMsgTemplateService的测试案例
 * 组织 com.mdp<br>
 * 顶级模块 mdp<br>
 * 大模块 wxpub<br>
 * 小模块 <br>
 * 表 WX.wx_msg_template 微信模板消息配置<br>
 * 实体 WxMsgTemplate<br>
 * 表是指数据库结构中的表,实体是指java类型中的实体类<br>
 * 当前实体所有属性名:<br>
 *	templateId,authId,templateType,title,primaryIndustry,deputyIndustry,content,example,wxaAppid,wxaPagepath,url,id,ctime,ltime;<br>
 * 当前表的所有字段名:<br>
 *	template_id,auth_id,template_type,title,primary_industry,deputy_industry,content,example,wxa_appid,wxa_pagepath,url,id,ctime,ltime;<br>
 * 当前主键(包括多主键):<br>
 *	id;<br>
 ***/

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
public class TestWxMenuService {

	@Autowired
	WxMenuService wxMenuService;

	@Test
	public void pushToWeixinTest(){
		String wxAppid="wx9f0f033214e62ae8";
		wxMenuService.pushToWeixin();
	}

	/**
	 * 将字符串类型的日期转成Date对象
	 * @param source 如2015-10-23或者 2015-10-23 15:30:25等
	 *        2015-10-23 15:30:25 对应的pattern 为 yyyy-MM-dd HH:mm:ss
	 * @return
	 */
	public static Date parse(String source){
		 
			SimpleDateFormat sdf=new SimpleDateFormat("yyyyMMddHHmmss");
			try {
				return sdf.parse(source);
			} catch (Exception e) {
			}   
		
		return null;
	}
}
