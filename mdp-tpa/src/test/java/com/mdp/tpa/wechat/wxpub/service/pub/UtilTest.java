package com.mdp.tpa.wechat.wxpub.service.pub;

import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;

import com.mdp.tpa.wechat.util.RandomUtil;
import com.mdp.tpa.wechat.wxpaysdk.WXPayUtil;
import org.junit.Assert;
import org.junit.Test;
import org.junit.internal.runners.JUnit4ClassRunner;
import org.junit.runner.RunWith;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;

@RunWith(JUnit4ClassRunner.class)
public class UtilTest {
	
	@Test
	public void testMapAsciiSort(){
		Map<String,Object> map=new HashMap<String,Object>();
		map.put("a", "a01");
		map.put("b", "b01");
		map.put("c", "c01");
		map.put("d", "d01");
		map.put("a1", "a01");
		map.put("b2", "b02");
		map.put("c3", "c03");
		map.put("d4", "d04");
		Object[] array= (Object[]) map.keySet().toArray();
		Arrays.sort(array);
		for (Object k : array) {
			System.out.println(k+":"+map.get(k));
		}
	
	}
	@Test
	public void testListAsciiSort(){
		String[] array=new String[]{"abcderg","b1","c","d","a1","b2","c3","d4"};
		Arrays.sort(array);
		for (String k : array) {
			System.out.println(k);
		}
	
	}
	
	@Test 
	public void payUnifiedorder(){
		Map<String,Object> p=new HashMap<String,Object>();
		String key="";
		p.put("appid", "");
		p.put("mchid", "");
		String body="测试商品购买支付";
		p.put("body", body);
		String nonceStr= RandomUtil.getRandomStr();
		p.put("nonceStr", nonceStr);
		String notifyUrl="http://www.qingqinkj.com/esb/wxpub/pay/notify";
		p.put("notifyUrl", notifyUrl);
		String outTradeNo="10000";
		p.put("outTradeNo", outTradeNo);
		String spbillCreateIp="192.168.0.1";
		p.put("spbillCreateIp", spbillCreateIp);
		Integer totalFee=(int) 1.5;
		p.put("totalFee", totalFee);
		String tradeType="JSAPI";
		p.put("tradeType", tradeType);
		Object[] array= (Object[]) p.keySet().toArray();
		Arrays.sort(array);
		String str="";
		int i=0;
		for (Object k : array) {
			if(i>0){
				str=str+"&"+k+"="+p.get(k);
			}else{
				str=k+"="+p.get(k);
			}
			i++;
			
		}
		str=str+"&key="+key;
		String sign=getMd5(str).toUpperCase();
		p.put("sign", sign);
	}
	
	@Test 
	public void paySign(){
		Map<String,Object> p=new HashMap<String,Object>();
		p.put("appid", "wxd930ea5d5a258f4f");
		p.put("mchid", "10000100");
		String body="test";
		p.put("body", body);
		String nonceStr="ibuaiVcKdpRxkhJA";
		p.put("nonceStr", nonceStr);
		
		String stringA="appid=wxd930ea5d5a258f4f&body=test&device_info=1000&mch_id=10000100&nonce_str=ibuaiVcKdpRxkhJA";
		String stringSignTemp=stringA+"&key=192006250b4c09247ec02edce69f6a2d";
		String sign=getMd5(stringSignTemp).toUpperCase();
		Assert.assertEquals(sign, "9A0A8659F005D6984697E2CA0A9CF3B7");
		p.put("sign", sign);
	}
	@Test
	public void Testaddprofitsharingaddreceiver() throws Exception {
		String appid = "wxdc4792ea8b97cd10";
		String mchId = "1489796752";
		String type = "PERSONAL_WECHATID";
		String relationType = "STAFF";
		String account = "wxid_xaajcmiaale022";
		String payAuthId = "";
		String spbill_create_ip = "223.73.196.252";
		Map<String, Object> signParams = new HashMap();
		signParams.put("mch_appid", appid);
		signParams.put("mchid", mchId);
		signParams.put("type", type);
		signParams.put("account", account);
		signParams.put("relationType", relationType);
		signParams.put("amount", account);
		signParams.put("spbill_create_ip", spbill_create_ip);
		Map<String,Object> mapL = new HashMap<String,Object>();
		mapL.put("type", type);
		mapL.put("account", account);
		mapL.put("relation_type", relationType);
		mapL.put("name", "郑海文");
		String  receiver= JSON.toJSONString(mapL);
		Map<String,String> p = new HashMap<String,String>();
		String nonceStr=RandomUtil.getRandomStr();
		p.put("appid", appid);
		p.put("mch_id",mchId);
		p.put("nonce_str", nonceStr);
		p.put("sign_type", "HMAC-SHA256");
		p.put("receiver", receiver);
		String sign = paySignDesposit(p,"ffffffffgfhfgjhdfgsdfgsghdshgdfs");
		p.put("sign", sign.toUpperCase());
		WXPayUtil.mapToXml(p);
		System.out.println(WXPayUtil.mapToXml(p));
		//Map<String,Object> m = wxApiPay.profitsharingaddreceiver(appid, mchId, sign, receiver);
	}
	@Test
	public void Testmultiprofitsharing() throws Exception {
		String appid = "wxdc4792ea8b97cd10";
		String mchId = "1489796752";
		String type = "PERSONAL_WECHATID";
		String relationType = "STAFF";
		String account = "wxid_xaajcmiaale022";
		JSONArray jArray = new JSONArray();
		Map<String,Object> mapp = new HashMap<String,Object>();
		mapp.put("type", "PERSONAL_WECHATID");
		mapp.put("account", account);
		mapp.put("amount",1);
		mapp.put("description", "分到个人");
		jArray.add(mapp);
		String receivers = jArray.toString();
		Map<String,String> p = new HashMap<String,String>();
		String nonceStr=RandomUtil.getRandomStr();
		p.put("appid", appid);
		p.put("mch_id",mchId);
		p.put("nonce_str", nonceStr);
		p.put("sign_type", "HMAC-SHA256");
		p.put("receivers", receivers);
		p.put("transaction_id", "4200000418201909110868294474");
		p.put("out_order_no", "3xrmq7rahb");
		String sign = paySignDesposit(p,"ffffffffgfhfgjhdfgsdfgsghdshgdfs");
		p.put("sign", sign.toUpperCase());
		WXPayUtil.mapToXml(p);
		System.out.println(WXPayUtil.mapToXml(p));
	}
	//静态方法，便于作为工具类  
    public static String getMd5(String plainText) {  
        try {  
            MessageDigest md = MessageDigest.getInstance("MD5");  
            md.update(plainText.getBytes());  
            byte b[] = md.digest();  
  
            int i;  
  
            StringBuffer buf = new StringBuffer("");  
            for (int offset = 0; offset < b.length; offset++) {  
                i = b[offset];  
                if (i < 0)  
                    i += 256;  
                if (i < 16)  
                    buf.append("0");  
                buf.append(Integer.toHexString(i));  
            }  
            //32位加密  
            return buf.toString();  
            // 16位的加密  
            //return buf.toString().substring(8, 24);  
        } catch (NoSuchAlgorithmException e) {  
            e.printStackTrace();  
            return null;  
        }  
  
    }
    /**
     * HmacSHA256类型签名
     * @param map
     * @return
     * @throws UnsupportedEncodingException
     * @throws NoSuchAlgorithmException
     * @throws InvalidKeyException
     */
    public static String paySignDesposit(Map<String, String> map, String key) throws UnsupportedEncodingException, NoSuchAlgorithmException, InvalidKeyException{
        Map<String, String> params = new HashMap<String, String>();
        Set<String> set = map.keySet();
        for (String string : set) {
            if(!map.get(string).equals("")){
                params.put(string, String.valueOf(map.get(string)));
            }
        }
        String string1 = createSign(params);
        String stringSignTemp = string1 + "&key=" + key;
        //return DigestUtils.sha256Hex(stringSignTemp).toUpperCase();
        Mac sha256_HMAC = Mac.getInstance("HmacSHA256");
        SecretKeySpec secret_key = new SecretKeySpec(key.getBytes(), "HmacSHA256");
        sha256_HMAC.init(secret_key);
        //  utf-8 : 解决中文加密不一致问题,必须指定编码格式
        return byteArrayToHexString(sha256_HMAC.doFinal(stringSignTemp.getBytes("utf-8"))).toUpperCase();
    }
    /**
     * 将加密后的字节数组转换成字符串
     *
     * @param b 字节数组
     * @return 字符串
     */
    private static String byteArrayToHexString(byte[] b) {
        StringBuilder hs = new StringBuilder();
        String stmp;
        for (int n = 0; b!=null && n < b.length; n++) {
            stmp = Integer.toHexString(b[n] & 0XFF);
            if (stmp.length() == 1)
                hs.append('0');
            hs.append(stmp);
        }
        return hs.toString().toLowerCase();
    }
    /**
     * 构造package
     * @param params
     * @return
     * @throws UnsupportedEncodingException
     */
    public static String createSign(Map<String, String> params) throws UnsupportedEncodingException {
        Set<String> keysSet = params.keySet();
        Object[] keys = keysSet.toArray();
        Arrays.sort(keys);
        StringBuffer temp = new StringBuffer();
        boolean first = true;
        for (Object key : keys) {
            if (first) {
                first = false;
            } else {
                temp.append("&");
            }
            temp.append(key.toString()).append("=");
            Object value = params.get(key);
            String valueString = "";
            if (null != value) {
                valueString = value.toString();
            }
            temp.append(valueString);
        }
        return temp.toString();
    }
    @Test
    public void stringLengthTest(){
    	String a="中国";
    	String ae="11";
    	System.out.println(a.length());
    	System.out.println(ae.length());
    }
}
