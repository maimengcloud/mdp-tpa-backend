package com.mdp.tpa.wechat.wxpub.service.pub;

import static org.junit.Assert.assertEquals;

import java.util.Map;

import com.mdp.tpa.wechat.api.AccessTokenService;
import com.mdp.tpa.wechat.api.WxUserService;
import com.mdp.tpa.wechat.entity.AccessToken;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class) 
@SpringBootTest
public class TestWxUserService {
	@Autowired
	WxUserService wxUserService;
	

	@Autowired
	AccessTokenService accessTokenService;
	
	@Test
	public void getUserOpenidList(){

		Map<String,Object> result=wxUserService.getUserOpenidList(  "");
		assertEquals(true, result.containsKey("data"));
	
	}
	 
}
