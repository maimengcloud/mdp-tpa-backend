package com.mdp.tpa.wechat.wxpub.service.pub;

import static org.junit.Assert.assertEquals;

import com.mdp.tpa.wechat.api.AccessTokenService;
import com.mdp.tpa.wechat.entity.AccessToken;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class) 
@SpringBootTest
public class TestAccessTokenService {
	@Autowired
	AccessTokenService accessTokenService;
	@Test
	public void getAccessToken(){
		AccessToken accessToken=accessTokenService.getAccessToken( );
		String accessTokenStr1=accessToken.getAccessToken();
		System.out.println(accessToken.getAccessToken());
		assertEquals(true, accessToken.getAccessToken().length()>10); 
		AccessToken accessToken2=accessTokenService.getAccessToken( );
		String accessTokenStr2=accessToken2.getAccessToken();
		assertEquals(true, accessTokenStr1.equals(accessTokenStr2)); 

	
	}
	 
}
