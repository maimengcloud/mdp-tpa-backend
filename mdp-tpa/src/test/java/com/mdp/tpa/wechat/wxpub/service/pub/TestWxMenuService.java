package com.mdp.tpa.wechat.wxpub.service.pub;

import java.util.*;

import com.mdp.tpa.wechat.pub.client.service.ArcClientService;
import com.mdp.tpa.wechat.wxpub.service.WxMenuService;
import com.mdp.tpa.wechat.wxpub.service.WxNotifyService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
/**
 * WxMenuService的测试案例
 * 组织 com.mdp<br>
 * 顶级模块 mdp<br>
 * 大模块 wxpub<br>
 * 小模块 <br>
 * 表 WX.wx_menu wx_menu<br>
 * 实体 WxMenu<br>
 * 表是指数据库结构中的表,实体是指java类型中的实体类<br>
 * 当前实体所有属性名:<br>
 *	type,name,url,key,mediaId,appid,pagepath,id,lvl,authId,pid,mtype,mkey,wxAppid;<br>
 * 当前表的所有字段名:<br>
 *	type,name,url,key,media_id,appid,pagepath,id,lvl,auth_id,pid,mtype,mkey,wx_appid;<br>
 * 当前主键(包括多主键):<br>
 *	id;<br>
 ***/

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
public class TestWxMenuService  {

	@Autowired
	WxMenuService wxMenuService;
	@Autowired
    WxNotifyService wxNotifyService;
	
	@Test
	public void pushToWeixin() {
		wxMenuService.pushToWeixin();
	}
	
	/**
	 * 			String msgType=(String) m.get("MsgType"); 
			String fromUserName=(String) m.get("FromUserName");
			String toUserName=(String) m.get("ToUserName");
			String createTime=(String) m.get("CreateTime");
			//事件推送
			if(m.containsKey("Event")){
				String event=(String) m.get("Event");
				//如果是关注事件
				if("event".equalsIgnoreCase(msgType) && event.contains("subscribe")){
	 */
	
	
	@Autowired
    ArcClientService arcClientService;
	@Test
	public void getNews() {
		List<Map<String,Object>> items=arcClientService.getNewList( "VT1001_TODAY_NEWS");
		for (Map<String, Object> map : items) {
			
		}
	}
}
