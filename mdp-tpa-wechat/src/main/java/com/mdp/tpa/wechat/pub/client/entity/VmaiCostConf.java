package com.mdp.tpa.wechat.pub.client.entity;

import java.math.BigDecimal;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * 组织 com.mdp  顶级模块 vmai 大模块 vmai  小模块 <br>
 * 实体 VmaiCostConf所有属性名: <br>
 *	id,serviceCharge;<br>
 * 表 VMAI.vmai_cost_conf vmai_cost_conf的所有字段名: <br>
 *	id,service_charge;<br>
 * 当前主键(包括多主键):<br>
 *	id;<br>
 */
@ApiModel(description="vmai_cost_conf")
public class VmaiCostConf  implements java.io.Serializable {
	
	private static final long serialVersionUID = -1343576700050079043L;

	@ApiModelProperty(notes="主键,主键",allowEmptyValue=true,example="",allowableValues="")
	String id;
  	
	
	@ApiModelProperty(notes="平台收取的百分比",allowEmptyValue=true,example="",allowableValues="")
	BigDecimal serviceCharge;
	
	@ApiModelProperty(notes="商家收取的百分比",allowEmptyValue=true,example="",allowableValues="")
	BigDecimal shopServiceCharge;
	
	@ApiModelProperty(notes="门店收取的百分比",allowEmptyValue=true,example="",allowableValues="")
	BigDecimal locationServiceCharge;

	/**主键**/
	public VmaiCostConf(String id) {
		this.id = id;
	}
    
    /**vmai_cost_conf**/
	public VmaiCostConf() {
	}
	
	/**
	 * 主键
	 **/
	public void setId(String id) {
		this.id = id;
	}
	/**
	 * 平台收取的百分比
	 **/
	public void setServiceCharge(BigDecimal serviceCharge) {
		this.serviceCharge = serviceCharge;
	}
	/**
	 * 商家收取的百分比
	 **/
	public void setShopServiceCharge(BigDecimal shopServiceCharge) {
		this.shopServiceCharge = shopServiceCharge;
	}
	/**
	 * 门店收取的百分比
	 **/
	public void setLocationServiceCharge(BigDecimal locationServiceCharge) {
		this.locationServiceCharge = locationServiceCharge;
	}
	
	/**
	 * 主键
	 **/
	public String getId() {
		return this.id;
	}
	/**
	 * 平台收取的百分比
	 **/
	public BigDecimal getServiceCharge() {
		return this.serviceCharge;
	}
	/**
	 * 商家收取的百分比
	 **/
	public BigDecimal getShopServiceCharge() {
		return this.shopServiceCharge;
	}
	/**
	 * 门店收取的百分比
	 **/
	public BigDecimal getLocationServiceCharge() {
		return this.locationServiceCharge;
	}
	

}
