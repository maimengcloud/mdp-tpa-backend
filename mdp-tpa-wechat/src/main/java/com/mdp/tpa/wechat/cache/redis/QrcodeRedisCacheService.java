package com.mdp.tpa.wechat.cache.redis;

import java.util.concurrent.TimeUnit;

import com.mdp.core.api.CacheHKVService;
import com.mdp.core.api.CacheService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

 @Service
public  class QrcodeRedisCacheService  implements CacheService<String> {
	
	@Autowired
	CacheHKVService cacheHKVService;
	 

	@Override
	public  void put(String key,String o) {
		cacheHKVService.setValue(getCacheKey()+":"+key, o,getDefaultDaysTimeout(),TimeUnit.DAYS);
	}

	public Long getDefaultDaysTimeout() {
		// TODO Auto-generated method stub
		return (long) 1;
	}

	@Override
	public String get(String key) {
		// TODO Auto-generated method stub
		return  (String) cacheHKVService.getValue(getCacheKey()+":"+key);
	}
 

	@Override
	public void refresh() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean containsValue(String value) {
		// TODO Auto-generated method stub
		return false;
	}
	
	public  String getCacheKey(){
		return "wxpub:qrcode";
		
	};
	 
	public boolean expire(String key,long days) {
		// TODO Auto-generated method stub
		return cacheHKVService.expire(key,days , TimeUnit.DAYS);
	}

	@Override
	public void remove(String key) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean containsKey(String key) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean expire(long days) {
		// TODO Auto-generated method stub
		return false;
	}

}