package com.mdp.tpa.wechat.wxpub.ctrl;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.mdp.core.entity.Result;
import com.mdp.tpa.wechat.api.SignService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.mdp.core.entity.Tips;
import com.mdp.tpa.client.service.AppTpAuthService;

@RestController("wxpub.signController")
@RequestMapping(value="/**/sign")
public class WxSignController  {
	
	@Autowired
	SignService wxApiSign;
	
	@Autowired
	AppTpAuthService tpAuthService;
	
	/***
	 * 向微信服务器申请访问jsapi签名
	 */
	@RequestMapping(method=RequestMethod.GET,params={"url"})
	public  Map<String,Object> signUrl(@RequestParam String url){
		Map<String,Object> m=wxApiSign.signUrl(url);
		return m;
	}
	/***
	 * 客户添加卡券时，相关参数由服务端签名后返回
	 * 
	 * {
	 * 	cardList: [
		    {
		      cardId: '',
		      cardExt: '{"code": "", "openid": "", "timestamp": "", "signature":""}'
		    }, {
		      cardId: '',
		      cardExt: '{"code": "", "openid": "", "timestamp": "", "signature":""}'
		    }
		  ]
		 }
	 */
	@RequestMapping(method=RequestMethod.GET,params={"cardId"})
	public Result signForAddCard(
											  @RequestParam(name="bizType",required=false) String bizType,
											  @RequestParam("cardId") String cardId,
											  @RequestParam("openid") String openid,
											  @RequestParam("outerStr") String outerStr){
		Map<String,Object> m=new HashMap<String,Object>();
		Tips tips=new Tips("签名成功");
		List<Map<String, Object>> cardListMap;
		try {
			List<String> cardList=new ArrayList();
			cardList.add(cardId);

			cardListMap = wxApiSign.signForAddCard(cardList, openid, outerStr);
		} catch (Exception e) {
			return Result.error("sign-error","签名失败");
		}
		return Result.ok().put("cardList",cardListMap);
	}	
}
