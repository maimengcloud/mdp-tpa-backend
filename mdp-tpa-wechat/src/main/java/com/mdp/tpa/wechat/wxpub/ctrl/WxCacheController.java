package com.mdp.tpa.wechat.wxpub.ctrl;


import java.util.HashMap;
import java.util.Map;

import com.mdp.core.entity.Result;
import com.mdp.tpa.wechat.api.SecurityService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.mdp.core.entity.Tips;
import com.mdp.core.err.BizException;
import com.mdp.tpa.client.service.AppTpAuthService;

@RestController("wxpub.cacheController")
@RequestMapping(value="/**/cache")
public class WxCacheController {

	private Logger logger= LoggerFactory.getLogger(WxCacheController.class);
	
	@Autowired
	AppTpAuthService tpAuthService;
	
	@Autowired
	SecurityService securityService;
	
	@RequestMapping(value="/clearAppTpAuthCache",method=RequestMethod.POST)
	public  Result clearAppTpAuthCache(@RequestBody Map<String,Object> params){
		try{
			String authId=(String) params.get("authId");
			if(StringUtils.isEmpty(authId)) {
				return Result.error("authId-required","authId必传");
			}else {

				securityService.clearCache();
				tpAuthService.clearOne(authId);
				return Result.ok();
			}
		}catch (BizException e) {
			logger.error("",e);
			return Result.error(e);
		}catch (Exception e) {
			logger.error("",e);
			return Result.error(e.getMessage());
		}
	}
}
