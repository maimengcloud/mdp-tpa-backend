package com.mdp.tpa.wechat.wxpub.utils;

import java.math.BigDecimal;

public class NumberUtil {
	
	public static BigDecimal getBigDecimal(Object v){
		if(v==null){
			return null;
		}
		BigDecimal amount=BigDecimal.ZERO;
		Object amountObj=v;
		if(amountObj instanceof BigDecimal){ 
			return (BigDecimal) amountObj;
		}else if(amountObj instanceof Double){
			amount=new BigDecimal(amountObj.toString());
		}else if(amountObj instanceof Integer){
			amount=new BigDecimal(amountObj.toString());
		}else{
			amount=new BigDecimal((String) amountObj);
		}
		return amount;
	}
	
	public static  Integer getInteger(Object v){
		if(v==null){
			return null;
		}
		Integer amount=0;
		Object amountObj=v;
		if(amountObj instanceof Integer){
			amount=(Integer)amountObj;
		}else if(amountObj instanceof Double){
			amount=((Double)amountObj).intValue();
		}else{
			amount=Integer.valueOf((String) amountObj);
		}
		return amount;
	}
}

