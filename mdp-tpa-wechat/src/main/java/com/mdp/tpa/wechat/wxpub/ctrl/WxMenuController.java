package com.mdp.tpa.wechat.wxpub.ctrl;

import java.util.*;

import com.mdp.core.entity.LangTips;
import com.mdp.meta.client.service.ItemService;
import com.mdp.tpa.wechat.api.MenuService;
import com.mdp.tpa.wechat.wxpub.service.WxMenuService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import io.swagger.annotations.*;

import com.mdp.core.entity.Result;
import com.mdp.core.entity.Tips;
import com.mdp.core.err.BizException;

/**
 * @author maimeng-mdp code-gen
 * @since 2023-9-21
 */
@RestController
@RequestMapping(value="/**/wechat/wxpub/wxMenu")
@Api(tags={"wx_menu-操作接口"})
public class WxMenuController {
	
	static Logger logger =LoggerFactory.getLogger(WxMenuController.class);
	@Autowired
	WxMenuService menuService;

	@Autowired
	ItemService itemService;

	@ApiOperation( value = "根据主键修改一条wx_menu信息",notes="editWxMenu")
	@RequestMapping(value="/pushToWeixin",method=RequestMethod.GET)
	public Result pushToWeixin(  Map<String,Object> params) {
		try{
			return this.menuService.pushToWeixin();
		}catch (BizException e) {
			logger.error("",e);
			return Result.error(e);
		}catch (Exception e) {
			logger.error("",e);
			return Result.error(e.getMessage());
		}
	}
}
