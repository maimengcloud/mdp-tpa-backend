package com.mdp.tpa.wechat.wxpub.ctrl;

import java.util.*;

import com.mdp.core.utils.ObjectTools;
import com.mdp.meta.client.service.ItemService;
import com.mdp.tpa.pub.client.SysClientService;
import com.mdp.tpa.pub.entity.SysUserTpa;
import com.mdp.tpa.wechat.api.MessageSendService;
import com.mdp.tpa.wechat.wxpub.service.WxMsgTemplateService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import io.swagger.annotations.*;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.mdp.core.entity.Result;
import com.mdp.core.query.QueryTools;
import com.mdp.core.err.BizException;

/**
 * @author maimeng-mdp code-gen
 * @since 2023-9-21
 */
@RestController
@RequestMapping(value="/**/wechat/wxpub/wxMsgTemplate")
@Api(tags={"微信模板消息配置-操作接口"})
public class WxMsgTemplateController {
	
	static Logger logger =LoggerFactory.getLogger(WxMsgTemplateController.class);

	@Autowired
	MessageSendService messageSendService;

	@Autowired
	SysClientService sysClientService;

	@Autowired
	WxMsgTemplateService templateService;


	/***/
	@ApiOperation( value = "推送消息到微信用户",notes="sendTplMsg,仅需要上传主键字段")
	@ApiResponses({
			@ApiResponse(code = 200, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'}")
	})
	@RequestMapping(value="/sendTplMsg",method=RequestMethod.POST)
	public Result sendTplMsg(@RequestBody Map<String,Object> msgParams) {
		try{
			 templateService.sendTplMsg(msgParams);
			return Result.ok();
		}catch (BizException e) {
			logger.error("",e);
			return Result.error(e);
		}catch (Exception e) {
			logger.error("",e);
			return Result.error(e.getMessage());
		}
	}
}
