package com.mdp.tpa.wechat.pub.client.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.math.BigDecimal;
import java.util.Date;

/**
 * 组织 com.mdp  顶级模块 vmai 大模块 vmai  小模块 <br>
 * 实体 VmaiUserBalance所有属性名: <br>
 *	userid,username,totalVmai,enabledVmai,freezeVmai,acctStatus,createTime,lastupdateTime,lasttermVmai,checkValue,signature,deptid,branchId,companyId,companyName,shopId,lockStatus,lockNo,lockDate,accountId,accountName,locationId;<br>
 * 表 VMAI.vmai_user_balance vmai_user_balance的所有字段名: <br>
 *	userid,username,total_vmai,enabled_vmai,freeze_vmai,acct_status,create_time,lastupdate_time,lastterm_vmai,check_value,signature,deptid,branch_id,company_id,company_name,shop_id,lock_status,lock_no,lock_date,account_id,account_name,location_id;<br>
 * 当前主键(包括多主键):<br>
 *	account_id;<br>
 */
@ApiModel(description="vmai_user_balance")
public class VmaiUserBalance  implements java.io.Serializable {
	
	private static final long serialVersionUID = 1L;
	
	@ApiModelProperty(notes="账户编号,主键",allowEmptyValue=true,example="",allowableValues="")
	String accountId;
  	
	
	@ApiModelProperty(notes="客户编号",allowEmptyValue=true,example="",allowableValues="")
	String userid;
	
	@ApiModelProperty(notes="用户名",allowEmptyValue=true,example="",allowableValues="")
	String username;
	
	@ApiModelProperty(notes="总虚拟麦数",allowEmptyValue=true,example="",allowableValues="")
	BigDecimal totalVmai;
	
	@ApiModelProperty(notes="可用虚拟麦数",allowEmptyValue=true,example="",allowableValues="")
	BigDecimal enabledVmai;
	
	@ApiModelProperty(notes="冻结麦数",allowEmptyValue=true,example="",allowableValues="")
	BigDecimal freezeVmai;
	
	@ApiModelProperty(notes="状态0生效1冻结",allowEmptyValue=true,example="",allowableValues="")
	String acctStatus;
	
	@ApiModelProperty(notes="创建时间",allowEmptyValue=true,example="",allowableValues="")
	Date createTime;
	
	@ApiModelProperty(notes="最新更新时间",allowEmptyValue=true,example="",allowableValues="")
	Date lastupdateTime;
	
	@ApiModelProperty(notes="上期总麦数",allowEmptyValue=true,example="",allowableValues="")
	BigDecimal lasttermVmai;
	
	@ApiModelProperty(notes="检验码",allowEmptyValue=true,example="",allowableValues="")
	String checkValue;
	
	@ApiModelProperty(notes="关键数据签名验证",allowEmptyValue=true,example="",allowableValues="")
	String signature;
	
	@ApiModelProperty(notes="机构编号",allowEmptyValue=true,example="",allowableValues="")
	String deptid;
	
	@ApiModelProperty(notes="云用户机构编号",allowEmptyValue=true,example="",allowableValues="")
	String branchId;
	
	@ApiModelProperty(notes="公司编号",allowEmptyValue=true,example="",allowableValues="")
	String companyId;
	
	@ApiModelProperty(notes="公司名称",allowEmptyValue=true,example="",allowableValues="")
	String companyName;
	
	@ApiModelProperty(notes="商铺ID",allowEmptyValue=true,example="",allowableValues="")
	String shopId;
	
	@ApiModelProperty(notes="0未锁定1已锁定(任务执行期间先锁定避免重复计算)",allowEmptyValue=true,example="",allowableValues="")
	String lockStatus;
	
	@ApiModelProperty(notes="独占锁编号(任务执行期间先锁定避免重复计算)",allowEmptyValue=true,example="",allowableValues="")
	String lockNo;
	
	@ApiModelProperty(notes="加锁时间",allowEmptyValue=true,example="",allowableValues="")
	Date lockDate;
	
	@ApiModelProperty(notes="账户名称",allowEmptyValue=true,example="",allowableValues="")
	String accountName;
	
	@ApiModelProperty(notes="门店编号",allowEmptyValue=true,example="",allowableValues="")
	String locationId;

	/**账户编号**/
	public VmaiUserBalance(String accountId) {
		this.accountId = accountId;
	}
    
    /**vmai_user_balance**/
	public VmaiUserBalance() {
	}
	
	/**
	 * 客户编号
	 **/
	public void setUserid(String userid) {
		this.userid = userid;
	}
	/**
	 * 用户名
	 **/
	public void setUsername(String username) {
		this.username = username;
	}
	/**
	 * 总虚拟麦数
	 **/
	public void setTotalVmai(BigDecimal totalVmai) {
		this.totalVmai = totalVmai;
	}
	/**
	 * 可用虚拟麦数
	 **/
	public void setEnabledVmai(BigDecimal enabledVmai) {
		this.enabledVmai = enabledVmai;
	}
	/**
	 * 冻结麦数
	 **/
	public void setFreezeVmai(BigDecimal freezeVmai) {
		this.freezeVmai = freezeVmai;
	}
	/**
	 * 状态0生效1冻结
	 **/
	public void setAcctStatus(String acctStatus) {
		this.acctStatus = acctStatus;
	}
	/**
	 * 创建时间
	 **/
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	/**
	 * 最新更新时间
	 **/
	public void setLastupdateTime(Date lastupdateTime) {
		this.lastupdateTime = lastupdateTime;
	}
	/**
	 * 上期总麦数
	 **/
	public void setLasttermVmai(BigDecimal lasttermVmai) {
		this.lasttermVmai = lasttermVmai;
	}
	/**
	 * 检验码
	 **/
	public void setCheckValue(String checkValue) {
		this.checkValue = checkValue;
	}
	/**
	 * 关键数据签名验证
	 **/
	public void setSignature(String signature) {
		this.signature = signature;
	}
	/**
	 * 机构编号
	 **/
	public void setDeptid(String deptid) {
		this.deptid = deptid;
	}
	/**
	 * 云用户机构编号
	 **/
	public void setBranchId(String branchId) {
		this.branchId = branchId;
	}
	/**
	 * 公司编号
	 **/
	public void setCompanyId(String companyId) {
		this.companyId = companyId;
	}
	/**
	 * 公司名称
	 **/
	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}
	/**
	 * 商铺ID
	 **/
	public void setShopId(String shopId) {
		this.shopId = shopId;
	}
	/**
	 * 0未锁定1已锁定(任务执行期间先锁定避免重复计算)
	 **/
	public void setLockStatus(String lockStatus) {
		this.lockStatus = lockStatus;
	}
	/**
	 * 独占锁编号(任务执行期间先锁定避免重复计算)
	 **/
	public void setLockNo(String lockNo) {
		this.lockNo = lockNo;
	}
	/**
	 * 加锁时间
	 **/
	public void setLockDate(Date lockDate) {
		this.lockDate = lockDate;
	}
	/**
	 * 账户编号
	 **/
	public void setAccountId(String accountId) {
		this.accountId = accountId;
	}
	/**
	 * 账户名称
	 **/
	public void setAccountName(String accountName) {
		this.accountName = accountName;
	}
	/**
	 * 门店编号
	 **/
	public void setLocationId(String locationId) {
		this.locationId = locationId;
	}
	
	/**
	 * 客户编号
	 **/
	public String getUserid() {
		return this.userid;
	}
	/**
	 * 用户名
	 **/
	public String getUsername() {
		return this.username;
	}
	/**
	 * 总虚拟麦数
	 **/
	public BigDecimal getTotalVmai() {
		return this.totalVmai;
	}
	/**
	 * 可用虚拟麦数
	 **/
	public BigDecimal getEnabledVmai() {
		return this.enabledVmai;
	}
	/**
	 * 冻结麦数
	 **/
	public BigDecimal getFreezeVmai() {
		return this.freezeVmai;
	}
	/**
	 * 状态0生效1冻结
	 **/
	public String getAcctStatus() {
		return this.acctStatus;
	}
	/**
	 * 创建时间
	 **/
	public Date getCreateTime() {
		return this.createTime;
	}
	/**
	 * 最新更新时间
	 **/
	public Date getLastupdateTime() {
		return this.lastupdateTime;
	}
	/**
	 * 上期总麦数
	 **/
	public BigDecimal getLasttermVmai() {
		return this.lasttermVmai;
	}
	/**
	 * 检验码
	 **/
	public String getCheckValue() {
		return this.checkValue;
	}
	/**
	 * 关键数据签名验证
	 **/
	public String getSignature() {
		return this.signature;
	}
	/**
	 * 机构编号
	 **/
	public String getDeptid() {
		return this.deptid;
	}
	/**
	 * 云用户机构编号
	 **/
	public String getBranchId() {
		return this.branchId;
	}
	/**
	 * 公司编号
	 **/
	public String getCompanyId() {
		return this.companyId;
	}
	/**
	 * 公司名称
	 **/
	public String getCompanyName() {
		return this.companyName;
	}
	/**
	 * 商铺ID
	 **/
	public String getShopId() {
		return this.shopId;
	}
	/**
	 * 0未锁定1已锁定(任务执行期间先锁定避免重复计算)
	 **/
	public String getLockStatus() {
		return this.lockStatus;
	}
	/**
	 * 独占锁编号(任务执行期间先锁定避免重复计算)
	 **/
	public String getLockNo() {
		return this.lockNo;
	}
	/**
	 * 加锁时间
	 **/
	public Date getLockDate() {
		return this.lockDate;
	}
	/**
	 * 账户编号
	 **/
	public String getAccountId() {
		return this.accountId;
	}
	/**
	 * 账户名称
	 **/
	public String getAccountName() {
		return this.accountName;
	}
	/**
	 * 门店编号
	 **/
	public String getLocationId() {
		return this.locationId;
	}

	@Override
	public String toString() {
		return "VmaiUserBalance [accountId=" + accountId + ", userid=" + userid + ", username=" + username
				+ ", totalVmai=" + totalVmai + ", enabledVmai=" + enabledVmai + ", freezeVmai=" + freezeVmai
				+ ", acctStatus=" + acctStatus + ", createTime=" + createTime + ", lastupdateTime=" + lastupdateTime
				+ ", lasttermVmai=" + lasttermVmai + ", checkValue=" + checkValue + ", signature=" + signature
				+ ", deptid=" + deptid + ", branchId=" + branchId + ", companyId=" + companyId + ", companyName="
				+ companyName + ", shopId=" + shopId + ", lockStatus=" + lockStatus + ", lockNo=" + lockNo
				+ ", lockDate=" + lockDate + ", accountName=" + accountName + ", locationId=" + locationId + "]";
	}
	

}