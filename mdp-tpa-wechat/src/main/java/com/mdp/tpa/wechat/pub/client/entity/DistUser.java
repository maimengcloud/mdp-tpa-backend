package com.mdp.tpa.wechat.pub.client.entity;

import java.util.Date;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * 组织 com.mdp  顶级模块 mk 大模块 dist  小模块 <br>
 * 实体 DistUser所有属性名: <br>
 *	userid,puserid,locationId,rootUserid,scene,createDate,lvlId,state,username,shopId,id;<br>
 * 表 MK.dist_user 分销用户关系表的所有字段名: <br>
 *	userid,puserid,location_id,root_userid,scene,create_date,lvl_id,state,username,shop_id,id;<br>
 * 当前主键(包括多主键):<br>
 *	id;<br>
 */
@ApiModel(description="分销用户关系表")
public class DistUser  implements java.io.Serializable {
	
	private static final long serialVersionUID = 1L;
	
	@ApiModelProperty(notes="主键,主键",allowEmptyValue=true,example="",allowableValues="")
	String id;
  	
	
	@ApiModelProperty(notes="用户ID",allowEmptyValue=true,example="",allowableValues="")
	String userid;
	
	@ApiModelProperty(notes="上一级用户ID，即用户是通过谁进入此平台的",allowEmptyValue=true,example="",allowableValues="")
	String puserid;
	
	@ApiModelProperty(notes="宣传门店的ID",allowEmptyValue=true,example="",allowableValues="")
	String locationId;
	
	@ApiModelProperty(notes="根用户ID",allowEmptyValue=true,example="",allowableValues="")
	String rootUserid;
	
	@ApiModelProperty(notes="用户通过什么方式进入此平台，0表示宣传，1表示付款，2表示其它",allowEmptyValue=true,example="",allowableValues="")
	String scene;
	
	@ApiModelProperty(notes="数据创建时间",allowEmptyValue=true,example="",allowableValues="")
	Date createDate;
	
	@ApiModelProperty(notes="级别编号",allowEmptyValue=true,example="",allowableValues="")
	String lvlId;
	
	@ApiModelProperty(notes="状态1有效0无效",allowEmptyValue=true,example="",allowableValues="")
	String state;
	
	@ApiModelProperty(notes="名字",allowEmptyValue=true,example="",allowableValues="")
	String username;
	
	@ApiModelProperty(notes="商户编号",allowEmptyValue=true,example="",allowableValues="")
	String shopId;

	/**主键**/
	public DistUser(String id) {
		this.id = id;
	}
    
    /**分销用户关系表**/
	public DistUser() {
	}
	
	public void setUserid(String userid) {
		this.userid = userid;
	}
	public void setPuserid(String puserid) {
		this.puserid = puserid;
	}
	public void setLocationId(String locationId) {
		this.locationId = locationId;
	}
	public void setRootUserid(String rootUserid) {
		this.rootUserid = rootUserid;
	}
	public void setScene(String scene) {
		this.scene = scene;
	}
	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}
	public void setLvlId(String lvlId) {
		this.lvlId = lvlId;
	}
	public void setState(String state) {
		this.state = state;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public void setShopId(String shopId) {
		this.shopId = shopId;
	}
	public void setId(String id) {
		this.id = id;
	}
	
	public String getUserid() {
		return this.userid;
	}
	public String getPuserid() {
		return this.puserid;
	}
	public String getLocationId() {
		return this.locationId;
	}
	public String getRootUserid() {
		return this.rootUserid;
	}
	public String getScene() {
		return this.scene;
	}
	public Date getCreateDate() {
		return this.createDate;
	}
	public String getLvlId() {
		return this.lvlId;
	}
	public String getState() {
		return this.state;
	}
	public String getUsername() {
		return this.username;
	}
	public String getShopId() {
		return this.shopId;
	}
	public String getId() {
		return this.id;
	}

}