package com.mdp.tpa.wechat.pub.client.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * 组织 com.mdp  顶级模块 ac 大模块 tpa  小模块 <br>
 * 实体 SettleAccount所有属性名: <br>
 *	accountId,settChannel,tpaAccountType,tpaAccountId,tpaAccountName,tpaRelationType,tpaCustomRelation,tpaAppid,tpaMchId,syncStatus,syncRemark,paccountId,id;<br>
 * 表 AC.tpa_settle_account tpa_settle_account的所有字段名: <br>
 *	account_id,sett_channel,tpa_account_type,tpa_account_id,tpa_account_name,tpa_relation_type,tpa_custom_relation,tpa_appid,tpa_mch_id,sync_status,sync_remark,paccount_id,id;<br>
 * 当前主键(包括多主键):<br>
 *	id;<br>
 */
@ApiModel(description="tpa_settle_account")
public class SettleAccount  implements java.io.Serializable {
	
	private static final long serialVersionUID = 1L;
	
	@ApiModelProperty(notes="主键,主键",allowEmptyValue=true,example="",allowableValues="")
	String id;
  	
	
	@ApiModelProperty(notes="mdp平台分配的账户，对应tpa_account_id",allowEmptyValue=true,example="",allowableValues="")
	String accountId;
	
	@ApiModelProperty(notes="分账渠道0微信1支付宝3银行4其它",allowEmptyValue=true,example="",allowableValues="")
	String settChannel;
	
	@ApiModelProperty(notes="MERCHANT_ID：商户ID,PERSONAL_WECHATID：个人微信号,PERSONAL_OPENID：个人openid",allowEmptyValue=true,example="",allowableValues="")
	String tpaAccountType;
	
	@ApiModelProperty(notes="分账渠道方账户编号",allowEmptyValue=true,example="",allowableValues="")
	String tpaAccountId;
	
	@ApiModelProperty(notes="分账渠道方账户名称",allowEmptyValue=true,example="",allowableValues="")
	String tpaAccountName;
	
	@ApiModelProperty(notes="与分账方关系",allowEmptyValue=true,example="",allowableValues="")
	String tpaRelationType;
	
	@ApiModelProperty(notes="自定义关系",allowEmptyValue=true,example="",allowableValues="")
	String tpaCustomRelation;
	
	@ApiModelProperty(notes="分账渠道分配的公众号appid",allowEmptyValue=true,example="",allowableValues="")
	String tpaAppid;
	
	@ApiModelProperty(notes="微信支付分配的商户号",allowEmptyValue=true,example="",allowableValues="")
	String tpaMchId;
	
	@ApiModelProperty(notes="mdp平台与第三方分账平台的同步状态0新建1申请中2申请通过3申请不通过4申请未知错误",allowEmptyValue=true,example="",allowableValues="")
	String syncStatus;
	
	@ApiModelProperty(notes="mdp平台与第三方分账平台的同步情况备注",allowEmptyValue=true,example="",allowableValues="")
	String syncRemark;
	
	@ApiModelProperty(notes="mdp平台上级资金账户，对应tpa_mch_id",allowEmptyValue=true,example="",allowableValues="")
	String paccountId;

	/**主键**/
	public SettleAccount(String id) {
		this.id = id;
	}
    
    /**tpa_settle_account**/
	public SettleAccount() {
	}
	
	/**
	 * mdp平台分配的账户，对应tpa_account_id
	 **/
	public void setAccountId(String accountId) {
		this.accountId = accountId;
	}
	/**
	 * 分账渠道0微信1支付宝3银行4其它
	 **/
	public void setSettChannel(String settChannel) {
		this.settChannel = settChannel;
	}
	/**
	 * MERCHANT_ID：商户ID,PERSONAL_WECHATID：个人微信号,PERSONAL_OPENID：个人openid
	 **/
	public void setTpaAccountType(String tpaAccountType) {
		this.tpaAccountType = tpaAccountType;
	}
	/**
	 * 分账渠道方账户编号
	 **/
	public void setTpaAccountId(String tpaAccountId) {
		this.tpaAccountId = tpaAccountId;
	}
	/**
	 * 分账渠道方账户名称
	 **/
	public void setTpaAccountName(String tpaAccountName) {
		this.tpaAccountName = tpaAccountName;
	}
	/**
	 * 与分账方关系
	 **/
	public void setTpaRelationType(String tpaRelationType) {
		this.tpaRelationType = tpaRelationType;
	}
	/**
	 * 自定义关系
	 **/
	public void setTpaCustomRelation(String tpaCustomRelation) {
		this.tpaCustomRelation = tpaCustomRelation;
	}
	/**
	 * 分账渠道分配的公众号appid
	 **/
	public void setTpaAppid(String tpaAppid) {
		this.tpaAppid = tpaAppid;
	}
	/**
	 * 微信支付分配的商户号
	 **/
	public void setTpaMchId(String tpaMchId) {
		this.tpaMchId = tpaMchId;
	}
	/**
	 * mdp平台与第三方分账平台的同步状态0新建1申请中2申请通过3申请不通过4申请未知错误
	 **/
	public void setSyncStatus(String syncStatus) {
		this.syncStatus = syncStatus;
	}
	/**
	 * mdp平台与第三方分账平台的同步情况备注
	 **/
	public void setSyncRemark(String syncRemark) {
		this.syncRemark = syncRemark;
	}
	/**
	 * mdp平台上级资金账户，对应tpa_mch_id
	 **/
	public void setPaccountId(String paccountId) {
		this.paccountId = paccountId;
	}
	/**
	 * 主键
	 **/
	public void setId(String id) {
		this.id = id;
	}
	
	/**
	 * mdp平台分配的账户，对应tpa_account_id
	 **/
	public String getAccountId() {
		return this.accountId;
	}
	/**
	 * 分账渠道0微信1支付宝3银行4其它
	 **/
	public String getSettChannel() {
		return this.settChannel;
	}
	/**
	 * MERCHANT_ID：商户ID,PERSONAL_WECHATID：个人微信号,PERSONAL_OPENID：个人openid
	 **/
	public String getTpaAccountType() {
		return this.tpaAccountType;
	}
	/**
	 * 分账渠道方账户编号
	 **/
	public String getTpaAccountId() {
		return this.tpaAccountId;
	}
	/**
	 * 分账渠道方账户名称
	 **/
	public String getTpaAccountName() {
		return this.tpaAccountName;
	}
	/**
	 * 与分账方关系
	 **/
	public String getTpaRelationType() {
		return this.tpaRelationType;
	}
	/**
	 * 自定义关系
	 **/
	public String getTpaCustomRelation() {
		return this.tpaCustomRelation;
	}
	/**
	 * 分账渠道分配的公众号appid
	 **/
	public String getTpaAppid() {
		return this.tpaAppid;
	}
	/**
	 * 微信支付分配的商户号
	 **/
	public String getTpaMchId() {
		return this.tpaMchId;
	}
	/**
	 * mdp平台与第三方分账平台的同步状态0新建1申请中2申请通过3申请不通过4申请未知错误
	 **/
	public String getSyncStatus() {
		return this.syncStatus;
	}
	/**
	 * mdp平台与第三方分账平台的同步情况备注
	 **/
	public String getSyncRemark() {
		return this.syncRemark;
	}
	/**
	 * mdp平台上级资金账户，对应tpa_mch_id
	 **/
	public String getPaccountId() {
		return this.paccountId;
	}
	/**
	 * 主键
	 **/
	public String getId() {
		return this.id;
	}

}