package com.mdp.tpa.wechat.wxpub.listerner;

import com.alibaba.fastjson.JSON;
import com.mdp.core.utils.ObjectTools;
import com.mdp.mq.sp.Subscriber;
import com.mdp.tpa.wechat.wxpub.service.WxMsgTemplateService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.connection.Message;
import org.springframework.stereotype.Service;

import java.util.Map;

@Service
public class SubscriberNotifyMsgService implements Subscriber {


    private static final String DEFAULT_TOPIC_NAME = "DEFAULT_TOPIC";
    @Autowired
    WxMsgTemplateService templateService;

    @Override
    public void receiveMessage(String channelName, Object msg) {
        if(ObjectTools.isEmpty(channelName)){
            return;
        }
        if(!channelName.equals(DEFAULT_TOPIC_NAME)){
            return;
        }
        if(msg==null){
            return;
        }
        Map<String,Object> message= JSON.parseObject(((Message)msg).toString());
        if(message==null || message.isEmpty() || !message.containsKey("toUserid")){
            return;
        }
        if(!message.containsKey("tplType")){
            message.put("tplType","taskNotify");
        }
        message.put("fromType","notifyMsg");
        templateService.sendTplMsg(message);
    }
}
