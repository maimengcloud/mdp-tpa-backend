package com.mdp.tpa.wechat.wxpub.service;
import com.mdp.core.entity.LangTips;
import com.mdp.core.entity.Result;
import com.mdp.meta.client.service.ItemService;
import com.mdp.tpa.wechat.api.AccessTokenService;
import com.mdp.tpa.wechat.api.MenuService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mdp.core.entity.Tips;
import com.mdp.tpa.client.service.AppTpAuthService;

/**
 * 父类已经支持增删改查操作,因此,即使本类什么也不写,也已经可以满足一般的增删改查操作了.<br> 
 * 组织 com.mdp  顶级模块 mdp 大模块 wxpub 小模块 <br>
 * 实体 WxMenu 表 WX.wx_menu 当前主键(包括多主键): id; 
 ***/
@Service("mdp.wxpub.wxMenuService")
public class WxMenuService {
	
	@Autowired
	AccessTokenService tokenService;
	@Autowired
	MenuService menuService;
	
	@Autowired
	AppTpAuthService tpAuthService;

	@Autowired
	ItemService itemService;

	public Result pushToWeixin() {
			String json=itemService.getSysParam("wechat_wxpub_menu",null);
			if(json==null){
				return Result.error("wechat_wxpub_menu-is-required","请配置系统参数%s","wechat_wxpub_menu");
			}
			//推送删除菜单操作
			menuService.deleteMenu();

			Tips tips=menuService.deleteMenu( );
			if(tips.isOk()) {
				//推送创建菜单操作
				tips=menuService.createMenu(  json );
			}
			return Result.build(LangTips.fromTips(tips));
	}

}

