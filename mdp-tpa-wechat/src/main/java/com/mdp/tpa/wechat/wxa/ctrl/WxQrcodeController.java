package com.mdp.tpa.wechat.wxa.ctrl;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.mdp.tpa.wechat.api.AccessTokenService;
import com.mdp.tpa.wechat.api.WxaService;
import com.mdp.tpa.wechat.cache.redis.QrcodeRedisCacheService;
import com.mdp.tpa.wechat.entity.AccessToken;
import com.mdp.tpa.wechat.wxpub.utils.NumberUtil;
import com.mdp.core.err.BizException;
import com.mdp.safe.client.entity.User;
import com.mdp.safe.client.utils.LoginUtils;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.CollectionUtils;
import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController("wxa.qrcodeController")
@RequestMapping(value = "/**/wxa/qrcode")
public class WxQrcodeController {

	Logger logger = LoggerFactory.getLogger(WxQrcodeController.class);

	@Autowired
	AccessTokenService accessTokenService;


	@Autowired
	QrcodeRedisCacheService qrcodeCacheService;

	@Autowired
	WxaService wxaService;

	/***
	 * 向微信服务器申请二维码的ticket
	 * https://developers.weixin.qq.com/miniprogram/dev/framework/open-ability/qr-code.html
	 * https://developers.weixin.qq.com/miniprogram/dev/api-backend/open-api/qr-code/wxacode.getUnlimited.html
	 * 生成的二维码没有次数限制，永久的
	 * "scene": "[[${scene}]]", "page": "[[${page}]]", "width": "[[${width}]]",
	 * "auto_color": "[[${autoColor}]]", "line_color":"[[${lineColor}]]"
	 * 正确的Json返回结果:
	 * {"ticket":"gQH47joAAAAAAAAAASxodHRwOi8vd2VpeGluLnFxLmNvbS9xL2taZ2Z3TVRtNzJXV1Brb3ZhYmJJAAIEZ23sUwMEmm3sUw==","expire_seconds":60,"url":"http:\/\/weixin.qq.com\/q\/kZgfwMTm72WWPkovabbI"}
	 */
	@RequestMapping(value = "/getwxacodeunlimit")
	public Map<String, Object> getwxacodeunlimit(@RequestParam("authId") String authId,
			@RequestBody Map<String, Object> map) {
		String scene = (String) map.get("scene");
		String page = (String) map.get("page");
		Integer width = NumberUtil.getInteger(map.get("width"));
		// String userid=(String) map.get("userid");
		// String appid=(String)map.get("appid");
		String autoColor = "true";
		String lineColor = "";
		Map<String, Object> m = wxaService.getWxacodeUnlimitAutoColor( scene,page,width);
		return m;
	}



}
