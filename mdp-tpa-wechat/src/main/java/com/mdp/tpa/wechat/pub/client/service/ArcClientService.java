package com.mdp.tpa.wechat.pub.client.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.mdp.micro.client.CallBizService;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mdp.core.entity.Tips;
import com.mdp.core.err.BizException;
import com.mdp.mq.queue.Push;
import com.mdp.tpa.client.entity.AppTpAuth;
import com.mdp.tpa.client.service.AppTpAuthService;

@Service
public class ArcClientService {
	Log log=LogFactory.getLog(ArcClientService.class);
	@Autowired
    CallBizService callBizService;
	
	@Autowired
	ObjectMapper om;

    @Autowired
    Push push;
    
    @Autowired
    AppTpAuthService tpAuthService;
	
	@Value("${mdp.api-gate:http://gate}")
	String apiGate="";//http://localhost:7041
	
	
	  /**
     * urlPath访问路径
     * params请求的参数 
     * post请求
     **/
    public Map<String,Object> dealWithMallm(String urlPath,Map<String,Object> params,String requestType){
        String url = apiGate + "/arc/arc/"+urlPath;
        Map<String, Object>  result= callBizService.postForMap(url,params);
        Tips tips = (Tips) result.get("tips");
        if (!tips.isOk()) {
            throw new BizException(tips);
        }
        return result;
    }
    
	  /**
     * urlPath访问路径
     * params请求的参数 
     * post请求
     **/
    public List<Map<String,Object>> getNewList( String eventKey){
        String url = apiGate + "/arc/arc/archive/listNews?categoryId={categoryId}&archiveType={archiveType}&pageSize={pageSize}&pageNum={pageNum}&count=true&orderBy=CREATE_DATE desc";
         Map<String,Object> params=new HashMap<>();
        params.put("archiveType","0");
        params.put("categoryId",eventKey);
        params.put("pageNum", 1);
        params.put("pageSize", 4);
        params.put("count", true);
        Map<String, Object> result  = callBizService.getForMap(url, params);
        Tips tips = (Tips) result.get("tips");
        if (!tips.isOk()) {
            throw new BizException(tips);
        }
        return (List<Map<String, Object>>) result.get("data");
    }
}
