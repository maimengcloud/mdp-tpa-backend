package com.mdp.tpa.wechat.wxpub.listerner;

import com.alibaba.fastjson.JSONObject;
import com.mdp.core.utils.ObjectTools;
import com.mdp.mq.queue.MessageListener;
import com.mdp.tpa.pub.client.SysClientService;
import com.mdp.tpa.pub.entity.SysUserTpa;
import com.mdp.tpa.wechat.api.UniformMessageService;
import com.mdp.tpa.client.entity.AppTpAuth;
import com.mdp.tpa.client.entity.UserTpa;
import com.mdp.tpa.client.service.AppTpAuthService;
import com.mdp.tpa.client.service.UserTpaService;
import com.mdp.tpa.wechat.wxpub.service.WxMsgTemplateService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import javax.annotation.PostConstruct;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import static com.mdp.core.utils.BaseUtils.*;

@Service
public class ImMsgListerner extends MessageListener<JSONObject> {


    @Autowired
    WxMsgTemplateService templateService;



    Logger logger= LoggerFactory.getLogger(ImMsgListerner.class);


    @Override
    public Object getQueueKey() {
        return "mdpim_to_wxpub_tplmsg";
    }

    @Override
    public void handleMessage(JSONObject message) {
        try {
            templateService.sendTplMsg(message.toJavaObject(Map.class));
        }catch (Exception e){
            logger.error("",e);
        }


    }




    public static void main(String[] args) {
        JSONObject message = new JSONObject();
        message.put("sendContent","您处理,流程:【mainTitle】,任务:【taskName】,意见建议:commentMsg,链接:bizUrl");

    }
    @PostConstruct
    public void init(){
        this.popMessage();
    }
}
