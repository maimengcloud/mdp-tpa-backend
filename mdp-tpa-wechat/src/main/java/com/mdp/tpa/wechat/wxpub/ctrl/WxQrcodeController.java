package com.mdp.tpa.wechat.wxpub.ctrl;


import java.util.HashMap;
import java.util.Map;

import com.mdp.tpa.wechat.api.AccessTokenService;
import com.mdp.tpa.wechat.entity.AccessToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mdp.core.utils.RequestUtils;

@RestController("wxpub.qrcodeController")
@RequestMapping(value="/**/qrcode")
public class WxQrcodeController {
	
	@Autowired
	AccessTokenService as;

 
	/***
	 * 向微信服务器申请二维码的ticket
	 * 正确的Json返回结果:
			{"ticket":"gQH47joAAAAAAAAAASxodHRwOi8vd2VpeGluLnFxLmNvbS9xL2taZ2Z3TVRtNzJXV1Brb3ZhYmJJAAIEZ23sUwMEmm3sUw==","expire_seconds":60,"url":"http:\/\/weixin.qq.com\/q\/kZgfwMTm72WWPkovabbI"}				
	 */
	@RequestMapping(value="/create")
	public  Map<String,Object> create(@RequestBody Map<String,Object> map){
		Map<String,Object> m=null;
		String actionName="QR_LIMIT_STR_SCENE"; //永久二维码
		String sceneStr="测试测试隔得好";
		
		Map<String,Object> p = new HashMap<String,Object>();
		p.put("actionName", actionName);
		p.put("sceneStr", sceneStr);
		p.put("accessToken", as.getToken());
		
		

		return m;
	}
 
}
