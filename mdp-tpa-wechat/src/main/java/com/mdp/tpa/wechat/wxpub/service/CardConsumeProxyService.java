package com.mdp.tpa.wechat.wxpub.service;

import com.mdp.tpa.wechat.api.AccessTokenService;
import com.mdp.tpa.wechat.api.CardConsumeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.mdp.core.service.BaseService;

/**
 * 卡券消费核销
 * 暂时注释，需要改成微服务调用方式，避免依赖mk_card包20190513
 * @author 陈裕财
 *
 */
@Service
public class CardConsumeProxyService extends BaseService {

	@Autowired
	CardConsumeService wxApiCardConsume;
/**
	@Autowired
	UserReceiveService urs;

	@Autowired
	UserVerifyService uvs;

	@Autowired
	BonusService bonusService;

	@Autowired
	BonusFlowService bonusFlowService;
	**/
	@Autowired
	AccessTokenService accessTokenService;

	/**
	 * 1.2 核销Code接口
	 * 1.仅支持核销有效状态的卡券，若卡券处于异常状态，均不可核销。（异常状态包括：卡券删除、未生效、过期、转赠中、转赠退回、失效）
	 * 我们强烈建议开发者在调用核销code接口之前调用查询code接口，并在核销之前对非法状态的code(如转赠中、已删除、已核销等)做出处理。
	 * 消耗code接口是核销卡券的唯一接口,开发者可以调用当前接口将用户的优惠券进行核销，该过程不可逆。
	 */
	@Async
	@Transactional
	public void codeConsumeFromApi( String orderId,String tpOrderId, String amount, boolean isBalancePay) {
		/**
		List<UserReceive> lur = null;
		try {
			AccessToken accessToken=accessTokenService.getAccessToken(authId);
			lur = (List) super.selectList("Wxpub.card.selectListUserReceiveByOrderId", orderId);
			for (UserReceive ur : lur) {
				String code = ur.getUserCardCode();
				Map<String, Object> m = wxApiCardConsume.codeConsume(accessToken.getAccessToken(),code); 

			}

			// 积分变化
			Map<String, Object> map = new HashMap<>();
			map.put("orderId", orderId);
			Map<String, Object> ruleResult = super.selectOne("Wxpub.bonusRule.selectBonusRuleByOrderId", map);
			if (ruleResult != null) {
				BigDecimal sum = (BigDecimal) ruleResult.get("SUM");// 订单总金额
				BigDecimal costMonetUnit = (BigDecimal) ruleResult.get("COST_MONEY_UNIT");// 规则：消费多少。
				BigDecimal increaseBonus = (BigDecimal) ruleResult.get("INCREASE_BONUS");// 规则：对应增加多少积分
				BigDecimal maxIncreaseBonus = (BigDecimal) ruleResult.get("MAX_INCREASE_BONUS");// 规则：用户单次可获取的积分上限

				Integer costMoney = costMonetUnit.intValue() / 100;// 转换单位
				Integer allBonus = (int) (sum.intValue() / costMoney.intValue()) * increaseBonus.intValue();
				if (allBonus.compareTo( maxIncreaseBonus.intValue())>0) {
					allBonus = maxIncreaseBonus.intValue();
				}

				// 积分总数变化
				Bonus bonus = new Bonus();
				BigDecimal bonusValue=(BigDecimal) ruleResult.get("BONUS");
				bonus.setBonus(allBonus + ((BigDecimal) ruleResult.get("BONUS")).intValue());
				bonus.setCode((String) ruleResult.get("CODE"));
				bonus.setLastUpdateTime(new Date());
				bonusService.update("activeUpdateBonusByCode", bonus);

				SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-dd-MM mm:hh:ss");
				// 积分流水变化
				BonusFlow bonusFlow = new BonusFlow();
				bonusFlow.setAfterBonus(bonus.getBonus());
				bonusFlow.setBeforeBonus(bonusValue.intValue());
				bonusFlow.setCardPkId((String) ruleResult.get("CARD_PK_ID"));
				bonusFlow.setCode((String) ruleResult.get("CODE"));
				bonusFlow.setCreateTime(simpleDateFormat.format(new Date()));
				bonusFlow.setId(sequenceService.getTablePK("BonusFlow", "id"));
				bonusFlow.setIncreaseBonus(allBonus - bonusValue.intValue());
				bonusFlow.setOrderid(orderId);
				bonusFlow.setReduceBonus(0);
				bonusFlowService.insert(bonusFlow);
			}
		} catch (Exception e) {
			log.error("",e);
		}
**/
	}
}
