package com.mdp.tpa.wechat.pub.client.entity;

import java.math.BigDecimal;
import java.util.Date;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * 组织 com.mdp  顶级模块 ac 大模块 acct  小模块 <br>
 * 实体 Account所有属性名: <br>
 *	accountId,userid,username,amount,cashAmount,uncashAmount,freezeCashAmount,freezeUncashAmount,acctProperty,acctStatus,createTime,interestBase,lasttermInterestDat,lastupdateTime,lasttermAmount,checkValue,accountName,signature,deptid,bankCode,bankAccountCode,acctPrjType,branchId,subjectId,subjectName,companyId,companyName;<br>
 * 表 AC.acct_account 账户表的所有字段名: <br>
 *	account_id,userid,username,amount,cash_amount,uncash_amount,freeze_cash_amount,freeze_uncash_amount,acct_property,acct_status,create_time,interest_base,lastterm_interest_dat,lastupdate_time,lastterm_amount,check_value,account_name,signature,deptid,bank_code,bank_account_code,acct_prj_type,branch_id,subject_id,subject_name,company_id,company_name;<br>
 * 当前主键(包括多主键):<br>
 *	account_id;<br>
 */
@ApiModel(description="账户表")
public class Account  implements java.io.Serializable {
	
	private static final long serialVersionUID = 1L;
	
	@ApiModelProperty(notes="账户ID,主键",allowEmptyValue=true,example="",allowableValues="")
	String accountId;
  	
	
	@ApiModelProperty(notes="客户编号",allowEmptyValue=true,example="",allowableValues="")
	String userid;
	
	@ApiModelProperty(notes="用户名",allowEmptyValue=true,example="",allowableValues="")
	String username;
	
	@ApiModelProperty(notes="总金额",allowEmptyValue=true,example="",allowableValues="")
	BigDecimal amount;
	
	@ApiModelProperty(notes="可提现",allowEmptyValue=true,example="",allowableValues="")
	BigDecimal cashAmount;
	
	@ApiModelProperty(notes="不可提现",allowEmptyValue=true,example="",allowableValues="")
	BigDecimal uncashAmount;
	
	@ApiModelProperty(notes="可提现冻结金额",allowEmptyValue=true,example="",allowableValues="")
	BigDecimal freezeCashAmount;
	
	@ApiModelProperty(notes="不可提现冻结金额",allowEmptyValue=true,example="",allowableValues="")
	BigDecimal freezeUncashAmount;
	
	@ApiModelProperty(notes="账户性质1个人2部门3往来单位",allowEmptyValue=true,example="",allowableValues="1,2,3")
	String acctProperty;
	
	@ApiModelProperty(notes="状态0生效1冻结",allowEmptyValue=true,example="",allowableValues="0,1")
	String acctStatus;
	
	@ApiModelProperty(notes="创建时间",allowEmptyValue=true,example="",allowableValues="")
	Date createTime;
	
	@ApiModelProperty(notes="计息倍数",allowEmptyValue=true,example="",allowableValues="")
	BigDecimal interestBase;
	
	@ApiModelProperty(notes="上期计息时间",allowEmptyValue=true,example="",allowableValues="")
	Date lasttermInterestDat;
	
	@ApiModelProperty(notes="最新更新时间",allowEmptyValue=true,example="",allowableValues="")
	Date lastupdateTime;
	
	@ApiModelProperty(notes="上期总金额",allowEmptyValue=true,example="",allowableValues="")
	BigDecimal lasttermAmount;
	
	@ApiModelProperty(notes="检验码",allowEmptyValue=true,example="",allowableValues="")
	String checkValue;
	
	@ApiModelProperty(notes="账户名称",allowEmptyValue=true,example="",allowableValues="")
	String accountName;
	
	@ApiModelProperty(notes="关键数据签名验证",allowEmptyValue=true,example="",allowableValues="")
	String signature;
	
	@ApiModelProperty(notes="机构编号",allowEmptyValue=true,example="",allowableValues="")
	String deptid;
	
	@ApiModelProperty(notes="银行机构代码",allowEmptyValue=true,example="",allowableValues="")
	String bankCode;
	
	@ApiModelProperty(notes="银行账号",allowEmptyValue=true,example="",allowableValues="")
	String bankAccountCode;
	
	@ApiModelProperty(notes="核算项目类别",allowEmptyValue=true,example="",allowableValues="dept,employee,cust,supplier,company,shop,stockgoods,project,other")
	String acctPrjType;
	
	@ApiModelProperty(notes="云用户机构编号",allowEmptyValue=true,example="",allowableValues="")
	String branchId;
	
	@ApiModelProperty(notes="科目编号",allowEmptyValue=true,example="",allowableValues="")
	String subjectId;
	
	@ApiModelProperty(notes="科目名称",allowEmptyValue=true,example="",allowableValues="")
	String subjectName;
	
	@ApiModelProperty(notes="公司编号",allowEmptyValue=true,example="",allowableValues="")
	String companyId;
	
	@ApiModelProperty(notes="公司名称",allowEmptyValue=true,example="",allowableValues="")
	String companyName;

	/**账户ID**/
	public Account(String accountId) {
		this.accountId = accountId;
	}
    
    /**账户表**/
	public Account() {
	}
	
	public void setAccountId(String accountId) {
		this.accountId = accountId;
	}
	public void setUserid(String userid) {
		this.userid = userid;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}
	public void setCashAmount(BigDecimal cashAmount) {
		this.cashAmount = cashAmount;
	}
	public void setUncashAmount(BigDecimal uncashAmount) {
		this.uncashAmount = uncashAmount;
	}
	public void setFreezeCashAmount(BigDecimal freezeCashAmount) {
		this.freezeCashAmount = freezeCashAmount;
	}
	public void setFreezeUncashAmount(BigDecimal freezeUncashAmount) {
		this.freezeUncashAmount = freezeUncashAmount;
	}
	public void setAcctProperty(String acctProperty) {
		this.acctProperty = acctProperty;
	}
	public void setAcctStatus(String acctStatus) {
		this.acctStatus = acctStatus;
	}
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	public void setInterestBase(BigDecimal interestBase) {
		this.interestBase = interestBase;
	}
	public void setLasttermInterestDat(Date lasttermInterestDat) {
		this.lasttermInterestDat = lasttermInterestDat;
	}
	public void setLastupdateTime(Date lastupdateTime) {
		this.lastupdateTime = lastupdateTime;
	}
	public void setLasttermAmount(BigDecimal lasttermAmount) {
		this.lasttermAmount = lasttermAmount;
	}
	public void setCheckValue(String checkValue) {
		this.checkValue = checkValue;
	}
	public void setAccountName(String accountName) {
		this.accountName = accountName;
	}
	public void setSignature(String signature) {
		this.signature = signature;
	}
	public void setDeptid(String deptid) {
		this.deptid = deptid;
	}
	public void setBankCode(String bankCode) {
		this.bankCode = bankCode;
	}
	public void setBankAccountCode(String bankAccountCode) {
		this.bankAccountCode = bankAccountCode;
	}
	public void setAcctPrjType(String acctPrjType) {
		this.acctPrjType = acctPrjType;
	}
	public void setBranchId(String branchId) {
		this.branchId = branchId;
	}
	public void setSubjectId(String subjectId) {
		this.subjectId = subjectId;
	}
	public void setSubjectName(String subjectName) {
		this.subjectName = subjectName;
	}
	public void setCompanyId(String companyId) {
		this.companyId = companyId;
	}
	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}
	
	public String getAccountId() {
		return this.accountId;
	}
	public String getUserid() {
		return this.userid;
	}
	public String getUsername() {
		return this.username;
	}
	public BigDecimal getAmount() {
		return this.amount;
	}
	public BigDecimal getCashAmount() {
		return this.cashAmount;
	}
	public BigDecimal getUncashAmount() {
		return this.uncashAmount;
	}
	public BigDecimal getFreezeCashAmount() {
		return this.freezeCashAmount;
	}
	public BigDecimal getFreezeUncashAmount() {
		return this.freezeUncashAmount;
	}
	public String getAcctProperty() {
		return this.acctProperty;
	}
	public String getAcctStatus() {
		return this.acctStatus;
	}
	public Date getCreateTime() {
		return this.createTime;
	}
	public BigDecimal getInterestBase() {
		return this.interestBase;
	}
	public Date getLasttermInterestDat() {
		return this.lasttermInterestDat;
	}
	public Date getLastupdateTime() {
		return this.lastupdateTime;
	}
	public BigDecimal getLasttermAmount() {
		return this.lasttermAmount;
	}
	public String getCheckValue() {
		return this.checkValue;
	}
	public String getAccountName() {
		return this.accountName;
	}
	public String getSignature() {
		return this.signature;
	}
	public String getDeptid() {
		return this.deptid;
	}
	public String getBankCode() {
		return this.bankCode;
	}
	public String getBankAccountCode() {
		return this.bankAccountCode;
	}
	public String getAcctPrjType() {
		return this.acctPrjType;
	}
	public String getBranchId() {
		return this.branchId;
	}
	public String getSubjectId() {
		return this.subjectId;
	}
	public String getSubjectName() {
		return this.subjectName;
	}
	public String getCompanyId() {
		return this.companyId;
	}
	public String getCompanyName() {
		return this.companyName;
	}

}