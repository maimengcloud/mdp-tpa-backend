package com.mdp.tpa.pub.service;

import com.baomidou.dynamic.datasource.annotation.DS;
import com.mdp.core.api.Sequence;
import com.mdp.core.err.BizException;
import com.mdp.core.query.QueryTools;
import com.mdp.core.utils.BaseUtils;
import com.mdp.core.utils.ObjectTools;
import com.mdp.mq.queue.Push;
import com.mdp.safe.client.entity.User;
import com.mdp.safe.client.pwd.SafePasswordEncoder;
import com.mdp.tpa.pub.client.SysClientService;
import com.mdp.tpa.pub.entity.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.util.DigestUtils;
import org.springframework.util.StringUtils;

import java.util.*;
import java.util.stream.Collectors;
@DS("adm-ds")
@Service
public class UserDataBaseService {

    Logger logger= LoggerFactory.getLogger(UserDataBaseService.class);

    @Autowired
    InviteCacheService inviteCacheService;


    @Autowired
    SysClientService sysClientService;


    PasswordEncoder passwordEncoder=new SafePasswordEncoder();

    @Autowired
    Sequence sequence;

    @Autowired
    Push push;


    public SysUser converUserTpaToUser(SysUserTpa userTpa) {
        SysUser user = new SysUser();
        user.setHeadimgurl(userTpa.getHeadimgurl());
        user.setUsername(userTpa.getUsername());
        user.setNickname(userTpa.getNickname());
        user.setCity(userTpa.getCity());
        user.setSex(userTpa.getGender());
        user.setPhoneno(userTpa.getPhoneno());
        user.setUnionid("M_"+sequence.getUserid());
        user.setUserid(user.getUnionid());
        user.setAtype("1");
        user.setBranchId(user.getUserid());
        user.setOpen("1");
        user.setGradeId("mg_001");
        user.setGradeName("一星学徒");
        user.setIlvlId("1");
        user.setGuardId("0");
        user.setProvince(userTpa.getProvince());
        user.setCpaType("0");
        user.setCpaOrg("0");
        user.setMemType("0");
        return user;
    }

    /**
     * 将userTpa 注册到数据库
     * @param tpa
     * @return
     */
    @DS("adm-ds")
    public SysUser registerUserByTpa(String inviteId,SysUserTpa tpa,boolean tpadbExists){
         return sysClientService.registerUserByTpa(inviteId,tpa,tpadbExists);
    }
    public SysUser invite(String state, SysUserTpa tpa,boolean tpadbExists) {
        SysUserTpaInvite invite=inviteCacheService.getInvite(state);
        if(invite==null){
            throw new BizException("state-not-exists","邀请已不存在");
        }
        return sysClientService.invite(state,tpa,tpadbExists);
    }

    public SysUser bind(String state, SysUserTpa tpa, boolean tpadbExists) {
        SysUserTpaInvite invite= inviteCacheService.getInvite(state);
        if(invite==null){
            throw new BizException("state-not-exists","邀请已不存在");
        }
        return sysClientService.bind(state,tpa,tpadbExists);
    }
    public String initPassword(){
        String passwordJsMd5= DigestUtils.md5DigestAsHex(("888888").getBytes());
        String passwordJavaMd5=passwordEncoder.encode(passwordJsMd5);
        return passwordJavaMd5;
    }

    public SysUser claim(String state, SysUserTpa tpa, boolean tpadbExists) {
        SysUserTpaInvite invite=inviteCacheService.getInvite(state);
        if(invite==null){
            throw new BizException("state-not-exists","邀请已不存在");
        }
        SysUser sysUser=sysClientService.claim(state,tpa,tpadbExists);
        return sysUser;
    }
}
