package com.mdp.tpa.wechat.client.service;

import com.mdp.core.entity.Tips;
import com.mdp.micro.client.CallBizService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

@Service
public class WechatMsgPushService {

    @Autowired
    CallBizService callBizService;

    /**
     * 消息模板字典 wechat-wxpub-tpl
     */
    public static String ITEM_CODE="wechat-wxpub-tpl";

    /**
     *
     * @param tplType 系统参数中的 itemCode，比如itemCode='wechat_msg_tpl_oa'
     * @param openid
     * @param url
     * @param data
     * @return
     */
    public Tips pushTemplateMsg(String tplType, String openid, String url, Map<String,Object> data){
        Map<String,Object> params=new HashMap<>();
        params.put("url",url);
        params.put("tplType",tplType);
        params.put("openid",openid);
        params.put("data",data);
        return callBizService.postForTips("/tpa/wechat/wxpub/wxMsgTemplate/sendTplMsg",params);
    }

    /**
     * 发送微信模板消息
     * @param templateId
     * @param openid
     * @param url
     * @param data
     * @return
     */
    public Tips pushTplMsgByOpenid(String templateId,String openid,String url,Map<String,Object> data){
        Map<String,Object> params=new HashMap<>();
        params.put("templateId",templateId);
        params.put("openid",openid);
        params.put("url",url);
        params.put("data",data);
        return callBizService.postForTips("/tpa/wechat/wxpub/wxMsgTemplate/sendTplMsg",params);
    }

    /**
     * 发送微信模板通知
     * 适合不知道openid的情况，由消息中心根据toUserid查找openid再进行消息发送
     * @param templateId
     * @param toUserid
     * @param url
     * @param data
     * @return
     */
    public Tips pushTplMsgByUserid(String templateId,String toUserid,String url,Map<String,Object> data){
        Map<String,Object> params=new HashMap<>();
        params.put("templateId",templateId);
        params.put("toUserid",toUserid);
        params.put("url",url);
        params.put("data",data);
        return callBizService.postForTips("/tpa/wechat/wxpub/wxMsgTemplate/sendTplMsg",params);
    }

    /**
     * 发送微信模板通知
     * 适合不知道openid的情况，由消息中心根据toUserid查找openid再进行消息发送
     *
     * 你好，微OA有新任务
     * 任务名称：报销单等待审批
     * 相关人员：「微语科技」晋明会
     * 请尽快进行处理
     *
     * {{first.DATA}}
     * 任务名称：{{keyword1.DATA}}
     * 相关人员：{{keyword2.DATA}}
     * {{remark.DATA}}
     *
     *
     * @param toUserid 接收消息的用户编号，后台会转为openid
     * @param url 跳转链接
     * @param context 对应first.DATA的内容 提示内容
     * @param taskName 对应keyword1.DATA的内容 任务名称
     * @param usernames 对应keyword2.DATA的内容 相关人员，逗号分割多个
     * @param remark 对应remark.DATA的内容 补充提示内容
     * @return
     */
    public Tips pushTaskTplMsg(String toUserid,String url,String context,String taskName,String usernames,String remark){
        Map<String,Object> params=new HashMap<>();
        Map<String,Object> data=new HashMap<>();
        params.put("tplType",ITEM_CODE);//消息模板字典
        params.put("toUserid",toUserid);
        params.put("url",url);
        params.put("data",data);

        Map<String,Object> first=new HashMap<>();
        Map<String,Object> keyword1=new HashMap<>();
        Map<String,Object> keyword2=new HashMap<>();
        Map<String,Object> remarkM=new HashMap<>();
        first.put("value", context);
        keyword1.put("value", taskName);
        keyword2.put("value", usernames);
        remarkM.put("value", remark);
        data.put("first", first);
        data.put("keyword1", keyword1);
        data.put("keyword2", keyword2);
        data.put("remark", remarkM);

        return callBizService.postForTips("/tpa/wechat/wxpub/wxMsgTemplate/sendTplMsg",params);
    }
}
