package com.mdp;

import org.springframework.boot.SpringApplication;
import org.springframework.cloud.client.SpringCloudApplication;

@SpringCloudApplication
public class TpaApplication {


    public static void main(String[] args) {
        SpringApplication.run(TpaApplication.class,args);

    }
}