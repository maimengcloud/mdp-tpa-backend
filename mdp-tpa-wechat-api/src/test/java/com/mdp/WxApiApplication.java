package com.mdp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
//org.springframework.boot.web.support.SpringBootServletInitializer;

@SpringBootApplication
public class WxApiApplication  {
	
 
	public static void main(String[] args) {
		SpringApplication.run(WxApiApplication.class,args);

	}

}
