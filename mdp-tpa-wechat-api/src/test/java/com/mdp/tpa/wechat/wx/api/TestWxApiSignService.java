package com.mdp.tpa.wechat.wx.api;

import java.util.HashMap;
import java.util.Map;

import com.mdp.tpa.wechat.api.AccessTokenService;
import com.mdp.tpa.wechat.api.SignService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * 测试从微信公众平台加载access_token的服务
 * @author cyc
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
public class TestWxApiSignService {

	@Autowired
	public AccessTokenService ats;
	String openid="oCFMX69qocTU4OEdJ0OaMcfsJ0hM";
	String openid2="oCFMX63ILM5oEpLsEsaqKf-jnnng";
	String appid="wx406faa04557b40c8";
	String authId="wx_pub_test_001";

	@Autowired
    SignService WxApiSign;
	@Test
	public void signPayMd5(){
		Map<String,Object> p=new HashMap<>();
		p.put("appid", 1);
		p.put("mch_id",2);
		p.put("body", 3);
		p.put("openid", 4);
		p.put("attach", 5);
		p.put("nonce_str", 6);
		p.put("notify_url", 7);
		p.put("out_trade_no", 8);
		p.put("spbill_create_ip", 9);
		p.put("total_fee", 0);
		String tradeType="JSAPI";
		p.put("trade_type", tradeType);
		WxApiSign.signPayMd5(p,"4444");
	}
}
