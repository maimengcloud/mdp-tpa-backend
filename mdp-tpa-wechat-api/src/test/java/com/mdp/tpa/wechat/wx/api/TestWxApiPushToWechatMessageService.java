package com.mdp.tpa.wechat.wx.api;

import com.mdp.core.entity.Tips;
import com.mdp.tpa.wechat.api.AccessTokenService;
import com.mdp.tpa.wechat.api.push.PushToWechatMessageService;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.HashMap;
import java.util.Map;

/**
 * 测试从微信公众平台加载access_token的服务
 * @author cyc
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
public class TestWxApiPushToWechatMessageService {
	
	@Autowired
	PushToWechatMessageService pushToWechatMessageService;


	@Autowired
	public AccessTokenService ats;
	
	String openid="o4lAmuHOrN1IWrloisfvQzaxjPAM";
	String openid2="04lAmuOpxEYju417YxjL2A-HkYs";
	String appid="";
	String authId="wx_pub_test_001";



	@Test
	public void bizsend(){
		String touser="oCFMX69qocTU4OEdJ0OaMcfsJ0hM";
		String templateId="7VywzVOBccneZw1plwlZMX-xIAQhgpBg7zlI2rLOxMY";
		String page="";
		Map<String,Object> miniprogram=new HashMap<>();
		Map<String,Object> data=new HashMap<>();
		Tips tips =pushToWechatMessageService.bizsend( touser, templateId,page,miniprogram,data);
		 
		Assert.assertEquals(true,tips.isOk());
	}

}
