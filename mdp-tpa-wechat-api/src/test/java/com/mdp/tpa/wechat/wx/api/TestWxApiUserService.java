package com.mdp.tpa.wechat.wx.api;

import com.mdp.tpa.wechat.api.AccessTokenService;
import com.mdp.tpa.wechat.api.WxUserService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * 测试从微信公众平台加载access_token的服务
 * @author cyc
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
public class TestWxApiUserService {
	

	
	String appid="";
	
	@Autowired
	public WxUserService apiUser;

	@Autowired
	public AccessTokenService ats;
	String openid="oCFMX69qocTU4OEdJ0OaMcfsJ0hM";
	String openid2="oCFMX63ILM5oEpLsEsaqKf-jnnng";
	String authId="wx_pub_test_001";
	
	String openidWxa="oPO_z0J2m_R9lzlqKiW09W-V9Fmg";//支持小程序中的openid
	//String openidWxa="oBi4O0ZO-JL1Ey_s9kWcQBmg3NwY";//支持小程序中的openid
	
	//String openidWxpub="o4lAmuHOrN1IWrloisfvQzaxjPAM";//支持公众号中的openid
	String openidWxpub="oSNImxAko6hTxo4UQUAbh1ycrnlc";//支持公众号中的openid
	
	@Test
	public void getUserinfoWxpub(){ 
		System.out.println(apiUser.getUserinfo( openidWxpub));
	}
	
	@Test
	public void getUserinfoWxa(){ 
		System.out.println(apiUser.getUserinfo( openidWxa));
	}
}
