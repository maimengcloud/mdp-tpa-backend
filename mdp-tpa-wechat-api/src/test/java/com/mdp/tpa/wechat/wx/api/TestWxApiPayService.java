package com.mdp.tpa.wechat.wx.api;

import java.util.HashMap;
import java.util.Map;

import com.mdp.tpa.wechat.api.AccessTokenService;
import com.mdp.tpa.wechat.api.PayService;
import com.mdp.tpa.wechat.util.RandomUtil;
import com.mdp.tpa.wechat.wxpaysdk.MyWxPayConfig;
import com.mdp.tpa.wechat.wxpaysdk.WXPayConfig;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.mdp.core.err.BizException;

/**
 * 测试从微信公众平台加载access_token的服务
 * @author cyc
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
public class TestWxApiPayService {

	@Autowired
	public AccessTokenService ats;
	String openid="oCFMX69qocTU4OEdJ0OaMcfsJ0hM";
	String openid2="oCFMX63ILM5oEpLsEsaqKf-jnnng";
	String appid="wx406faa04557b40c8";
	String authId="wx_pub_test_001";
	String openidWxa="xxxxxxxxxxxxxxxxxxxxxxxxx";
	String openidWxpub="";

	@Bean
	WXPayConfig iniConfig(){

		/**
		 *
		 # 微信支付相关参数
		 # 商户号
		 wxpay.mch-id=1389170202
		 # 商户API证书序列号
		 wxpay.mch-serial-no=11713378AC9AECB82E0503A905072C79C203C2D7

		 # 商户私钥文件——地址为/分隔
		 #wxpay.private-key-path=C:/Users/ASUS/Desktop/mall-backend/mall-backend/mall-server/apiclient_key.pem
		 wxpay.private-key-path=/certs/1389170202/apiclient_key.pem
		 # APIv3密钥
		 wxpay.api-v3-key=qqkj_maimengcloud_apiv3_19830320
		 # APPID
		 wxpay.appid=wx9f0f033214e62ae8
		 # 微信服务器地址
		 wxpay.domain=https://api.mch.weixin.qq.com
		 # 接收结果通知地址
		 wxpay.notify-domain=https://www.qingqinkj.com/api/m1/

		 # APIv2密钥——不使用v2版本
		 #wxpay.partnerKey:
		 */
		WXPayConfig config = null;
		try {
			config = new MyWxPayConfig("wx4b979392319468b2","1389170202","jfkdsjfklkgjkkkdkkdkdkjgklajgjlg");
			config.setNotifyUrl("https://www.maimengcloud.com/api/m1/tpa/pay/wxpay/notify/jsapi");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return config;
	}


	@Autowired
    PayService wxApiPay;
	
	/**
	 * 	 * 小程序下单必须使用小程序对应的openid，不能使用公众号中的openid
	 * 	接口返回：
	 * 	 * <xml></p>
		   <return_code><![CDATA[SUCCESS]]></return_code></p>
		   <return_msg><![CDATA[OK]]></return_msg></p>
		   <appid><![CDATA[wx2421b1c4370ec43b]]></appid></p>
		   <mch_id><![CDATA[10000100]]></mch_id></p>
		   <nonce_str><![CDATA[IITRi8Iabbblz1Jc]]></nonce_str></p>
		   <sign><![CDATA[7921E432F65EB8ED0CE9755F0E86D72F]]></sign></p>
		   <result_code><![CDATA[SUCCESS]]></result_code></p>
		   <prepay_id><![CDATA[wx201411101639507cbf6ffd8b0779950874]]></prepay_id></p>
		   <trade_type><![CDATA[JSAPI]]></trade_type></p>
		</xml></p>"
		
		本程序加工后返回：
		{timeStamp=5555615380, package=prepay_id=wx20170916175628af1297e0660760806062, paySign=F8B53EBC36147498E75898CAC20BFA7B, appId=wx3d57ec32bf63d5fa, signType=MD5, nonceStr=0remCF0KbAc2kt9r}
	 */
	@Test
	public void unifiedOrder_wxa(){
		String authId=this.authId;
		String shopId="shopId001";
		String locationId="locationId001";
		String payType="wxa";
		String outTradeNo= RandomUtil.getRandomStr();
		String body="22222";
		String attach="{order:xxxx,bizType:1}";
		String spbillCreateIp="192.168.0.1";
		int totalFee=20; 
		String goodsTag="3";
		/**
		 * appid wx4b979392319468b2
		 */
		String openid="orpmc5JLPJjY-OAqD47LJKVBEhdE";
		 
		Map<String,Object> result= wxApiPay.unifiedOrder( iniConfig(), outTradeNo, body, attach, spbillCreateIp, totalFee, goodsTag, openid);
		Assert.assertEquals(true,appid.equals(result.get("appId"))); 
	}
	
	@Test
	public void unifiedOrder_wxpub(){
		String authId=this.authId;
		String shopId="shopId001";
		String locationId="locationId001";
		String payType="wxpub";
		String outTradeNo=RandomUtil.getRandomStr();
		String body="22222";
		String attach="{order:xxxx,bizType:1}";
		String spbillCreateIp="192.168.0.1";
		int totalFee=20; 
		String goodsTag="3";
		String openid=openidWxpub;
		 Map<String,Object> attachMap=new HashMap<String,Object>();
		 attachMap.put("bizType","0");
		 attachMap.put("orderId","2222222222222222222222222222220");
		Map<String,Object> result= wxApiPay.unifiedOrder(iniConfig(), outTradeNo, body, attachMap.toString(), spbillCreateIp, totalFee, goodsTag, openid);
		Assert.assertEquals(true,appid.equals(result.get("appId"))); 
	}
	
	/**
	 * 小程序下单必须使用小程序对应的openid，不能使用公众号中的openid
	 */
	@Test
	public void unifiedOrder_wxa_with_openidWxpub(){
		String authId=this.authId;
		String shopId="shopId001";
		String locationId="locationId001";
		String payType="wxpub";
		String outTradeNo=RandomUtil.getRandomStr();
		String body="22222";
		String attach="22222";
		String spbillCreateIp="192.168.0.1";
		int totalFee=20; 
		String goodsTag="3";
		String openid=openidWxpub;
		 try{
			 wxApiPay.unifiedOrder(iniConfig(), outTradeNo, body, attach, spbillCreateIp, totalFee, goodsTag, openid);
		 }catch (BizException e) {
			 Assert.assertEquals(false,e.getTips().isOk()); 
 		}
	}
}
