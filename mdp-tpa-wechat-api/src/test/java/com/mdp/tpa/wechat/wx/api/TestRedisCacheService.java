package com.mdp.tpa.wechat.wx.api;

import com.mdp.tpa.wechat.api.AccessTokenService;
import com.mdp.tpa.wechat.entity.AccessToken;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.mdp.tpa.wechat.api.cache.AccessTokenCache;

/**
 * 测试从微信公众平台加载access_token的服务
 * @author cyc
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
public class TestRedisCacheService {
	@Autowired
	AccessTokenCache cache;

	@Autowired
	public AccessTokenService ats;


	String authId="wx_pub_test_001";

	@Test
	public void putAccessToken(){ 
		String appid="appid--1001";
		AccessToken at=new AccessToken();
		at.setAccessToken("xxxxxxx");
		at.setExpiresIn(10);
		cache.put(ats.getAccessToken( ).getAccessToken(), at);
	}
	
	@Test
	public void getAccessToken(){ 
		String appid="appid--1001";
		AccessToken at=cache.get(appid); 
		System.out.println(at);
	}
}
