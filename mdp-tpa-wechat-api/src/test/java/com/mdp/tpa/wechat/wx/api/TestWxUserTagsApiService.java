package com.mdp.tpa.wechat.wx.api;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.mdp.tpa.wechat.api.AccessTokenService;
import com.mdp.tpa.wechat.api.UserTagsService;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.mdp.core.entity.Tips;

/**
 * 测试从微信公众平台加载access_token的服务
 * @author cyc
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
public class TestWxUserTagsApiService  {
	
	Log log=LogFactory.getLog(TestWxUserTagsApiService.class);
	@Autowired
    UserTagsService wxUserTagsApi;

	@Autowired
	public AccessTokenService ats;
	String openid="oCFMX69qocTU4OEdJ0OaMcfsJ0hM";
	String openid2="oCFMX63ILM5oEpLsEsaqKf-jnnng";
	String appid="wx406faa04557b40c8";
	String authId="wx_pub_test_001";
	  
	@Test
	public void getMemberTags_Test(){ 
		Map<String,Object> result= wxUserTagsApi.getMemberTags( openid);
		Assert.assertEquals(true,result.containsKey("tagid_list") ); 
	} 
	
	@Test
	public void getMemberTags_wxa_Test(){ 
		Map<String,Object> result= wxUserTagsApi.getMemberTags( openid);
		Assert.assertEquals(true,result.containsKey("tagid_list") ); 
	} 
	 
	@Test
	public void tagsGet(){ 
		Map<String,Object> result= wxUserTagsApi.tagsGet( );
		Assert.assertEquals(true,result.containsKey("tags") ); 
	} 
	
	@Test
	public void membersBatchTagging(){ 
		List<String> openids=new ArrayList<>();
		openids.add(openid);
		openids.add(openid);
		Tips tips= wxUserTagsApi.membersBatchTagging( 0, openids);
		Assert.assertEquals(true,tips.isOk()); 
	} 
}