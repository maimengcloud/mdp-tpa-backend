package com.mdp.tpa.wechat.wx.api;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.mdp.core.utils.BaseUtils;
import com.mdp.tpa.wechat.api.AccessTokenService;
import com.mdp.tpa.wechat.api.CardService;
import com.mdp.tpa.wechat.api.MessageSendService;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * 测试从微信公众平台加载access_token的服务
 * @author cyc
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
public class TestWxApiMessageSendService {

	@Autowired
    MessageSendService send;
	
	@Autowired
    CardService cardWxApi;

	@Autowired
	public AccessTokenService ats;
	String openid="oCFMX69qocTU4OEdJ0OaMcfsJ0hM";
	String openid2="oCFMX63ILM5oEpLsEsaqKf-jnnng";
	String appid="wx406faa04557b40c8";
	String authId="wx_pub_test_001";

	@Test
	public void news(){
		List<String> openids=new ArrayList<>();
		openids.add(openid);
		openids.add(openid2);
		Map<String,Object> result=send.sendTextByOpenids( "111", openids);
		 
		Assert.assertEquals(true,"0".equals(result.get("errcode"))); 
	}
	@Test
	public void text(){ 
		List<String> openids=new ArrayList<>();
		openids.add(openid);
		openids.add(openid2);
		Map<String,Object> result=send.sendTextByOpenids( "111", openids);
		 
		Assert.assertEquals(true,"0".equals(result.get("errcode"))); 
	}

	@Test
	public void textAll(){
		Map<String,Object> result=send.sendTextToAll( "测试");
		Assert.assertEquals(true,"0".equals(result.get("errcode")));
	}
	@Test
	public void video(){ 
		List<String> openids=new ArrayList<>();
		openids.add(openid);
		openids.add(openid2);
		Map<String,Object> result=send.sendTextByOpenids( "111", openids);
		 
		Assert.assertEquals(true,"0".equals(result.get("errcode"))); 
	}
	@Test
	public void voice(){ 
		List<String> openids=new ArrayList<>();
		openids.add(openid);
		openids.add(openid2);
		Map<String,Object> result=send.sendTextByOpenids( "111", openids);
		 
		Assert.assertEquals(true,"0".equals(result.get("errcode"))); 
	}
	@Test
	public void image(){ 
		List<String> openids=new ArrayList<>();
		openids.add(openid);
		openids.add(openid2);
		Map<String,Object> result=send.sendTextByOpenids( "111", openids);
		 
		Assert.assertEquals(true,"0".equals(result.get("errcode"))); 
	}
	@Test
	public void wxcard(){ 
		List<String> openids=new ArrayList<>();
		openids.add(openid);
		openids.add(openid2);
		
		Map<String,Object> cardMap=cardWxApi.cardBatchGet( 0, 2, "CARD_STATUS_VERIFY_OK");
		List<String> cardList=(List<String>) cardMap.get("card_id_list");
		Map<String,Object> result=send.sendWxCardByOpenids( cardList.get(0), openids);
		 
		Assert.assertEquals(true,0==(Integer)result.get("errcode")); 
	}

	/**
	 * String accessToken, String templateId,String openid,String url,Map<String,Object> data
	 * 如模板参数{{user.username.DATA}},则 发送的参数格式如 {user:{username:{value:cycsir}}}
	 */
	@Test
	public void sendTemplateMessage(){
		String templateId="7VywzVOBccneZw1plwlZMX-xIAQhgpBg7zlI2rLOxMY";
		String openid=this.openid;
		String url="";
		Map<String,Object> data=new HashMap<>();
		data.put("user", BaseUtils.map("username",BaseUtils.map("value","cycsir")));
		Map<String, Object>  result=send.sendTemplateMessage( templateId,openid,url,data);
		Assert.assertEquals(true,0==(Integer)result.get("errcode"));
	}
}
