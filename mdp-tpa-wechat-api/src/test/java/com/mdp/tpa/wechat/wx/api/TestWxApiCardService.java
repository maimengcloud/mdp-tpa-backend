package com.mdp.tpa.wechat.wx.api;

import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;

import com.mdp.tpa.wechat.api.AccessTokenService;
import com.mdp.tpa.wechat.api.CardService;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.mdp.core.entity.Tips;
import com.mdp.core.utils.BaseUtils;

/**
 * 测试从微信公众平台加载access_token的服务
 * @author cyc
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
public class TestWxApiCardService  {
	
	Log log=LogFactory.getLog(TestWxApiCardService.class);
	@Autowired
    CardService cardWxApi;

	@Autowired
	public AccessTokenService ats;
	String openid="oCFMX69qocTU4OEdJ0OaMcfsJ0hM";
	String openid2="oCFMX63ILM5oEpLsEsaqKf-jnnng";
	String appid="wx406faa04557b40c8";
	String authId="wx_pub_test_001";
	@Test
	public void cardBatchGet_Test(){ 
		Map<String,Object> result=cardWxApi.cardBatchGet( 0, 10, "CARD_STATUS_VERIFY_OK");//["CARD_STATUS_VERIFY_OK", "CARD_STATUS_DISPATCH"]
		Assert.assertEquals(true,result.containsKey("card_id_list") );
		Assert.assertEquals(0,result.get("errcode") );
	} 
	
	@Test
	public void createMemberCard_Test(){ 
		Map<String,Object> memberCard=cardWxApi.createMemberCard( BaseUtils.map("","","",""));
		Assert.assertEquals(true,memberCard!=null);
	} 
	
	@Test
	public void createCoupon_Test(){
		Map<String,Object> coupon =cardWxApi.createCoupon( BaseUtils.map("","","",""));
		Assert.assertEquals(true,coupon!=null);
	} 
	
	@Test
	public void cardGet_Test(){ 
		Map<String,Object> result=cardWxApi.cardBatchGet( 0, 10, "CARD_STATUS_VERIFY_OK");//["CARD_STATUS_VERIFY_OK", "CARD_STATUS_DISPATCH"]
		Assert.assertEquals(true,result.containsKey("card_id_list") );
		List<String> list=(List<String>) result.get("card_id_list");
		log.debug(result);
		String cardId=list.get(0);
		Map<String,Object> result2=cardWxApi.cardGet( cardId);//["CARD_STATUS_VERIFY_OK", "CARD_STATUS_DISPATCH"]
		log.debug(result2);
		Assert.assertEquals(0,result2.get("errcode") );
	} 
	
	@Test
	public void cardDelete_Test(){ 
		Map<String,Object> result=cardWxApi.cardBatchGet( 0, 10, "CARD_STATUS_VERIFY_OK");//["CARD_STATUS_VERIFY_OK", "CARD_STATUS_DISPATCH"]
		Assert.assertEquals(true,result.containsKey("card_id_list") );
		List<String> list=(List<String>) result.get("card_id_list");
		log.debug(result);
		String cardId=list.get(0);
		Tips tips=cardWxApi.cardDelete( cardId);
		Assert.assertEquals(true,tips.isOk()); 
	} 
	@Test
	public void cardUpdate_Test(){ 
		Map<String,Object> result=cardWxApi.cardBatchGet( 0, 10, "CARD_STATUS_VERIFY_OK");//["CARD_STATUS_VERIFY_OK", "CARD_STATUS_DISPATCH"]
		Assert.assertEquals(true,result.containsKey("card_id_list") );
		List<String> list=(List<String>) result.get("card_id_list");
		log.debug(result);
		String cardId=list.get(0);
		Tips tips=cardWxApi.cardUpdate( cardId,BaseUtils.map("","",""));
		Assert.assertEquals(true,tips.isOk()); 
	} 
	@Test
	public void modifyStock_Test(){ 
		Map<String,Object> result=cardWxApi.cardBatchGet( 0, 10, "CARD_STATUS_VERIFY_OK");//["CARD_STATUS_VERIFY_OK", "CARD_STATUS_DISPATCH"]
		Assert.assertEquals(true,result.containsKey("card_id_list") );
		List<String> list=(List<String>) result.get("card_id_list");
		log.debug(result);
		String cardId=list.get(0);
		Tips tips=cardWxApi.modifyStock( cardId,3,0);
		Assert.assertEquals(true,tips.isOk()); 
		Tips tips2=cardWxApi.modifyStock( cardId,0,2);
		Assert.assertEquals(true,tips2.isOk()); 
	} 
	@Test
	public void userGetCardList_Test(){ 
		Map<String,Object> result=cardWxApi.cardBatchGet( 0, 10, "CARD_STATUS_VERIFY_OK");//["CARD_STATUS_VERIFY_OK", "CARD_STATUS_DISPATCH"]
		Assert.assertEquals(true,result.containsKey("card_id_list") );
		List<String> list=(List<String>) result.get("card_id_list");
		log.debug(result);
		String cardId=list.get(0);
		String openid=this.openid;
		Map<String,Object> result2=cardWxApi.userGetCardList( cardId,openid);
		Assert.assertEquals(true,result2.containsKey("card_list")); 
	} 
	  
	@Test
	public void getCardBizuinInfo_Test(){ 
		Calendar calendar=Calendar.getInstance();   
		calendar.setTime(new Date()); 
	    calendar.set(Calendar.DAY_OF_MONTH,calendar.get(Calendar.DAY_OF_MONTH)-10);//让日期加1  
		Date dfrom=calendar.getTime();
		calendar.set(Calendar.DAY_OF_MONTH,calendar.get(Calendar.DAY_OF_MONTH)+9);//让日期加1  
		Date dTo=calendar.getTime();
		Map<String,Object> result2=cardWxApi.getCardBizuinInfo( dfrom,dTo,0);
		Assert.assertEquals(true,result2.containsKey("list")); 
	} 
	@Test
	public void getCardInfo_Test(){ 
		
		Map<String,Object> result=cardWxApi.cardBatchGet( 0, 10, "CARD_STATUS_VERIFY_OK");//["CARD_STATUS_VERIFY_OK", "CARD_STATUS_DISPATCH"]
		Assert.assertEquals(true,result.containsKey("card_id_list") );
		List<String> list=(List<String>) result.get("card_id_list");
		log.debug(result);
		String cardId=list.get(0);
		
		Calendar calendar=Calendar.getInstance();   
		calendar.setTime(new Date()); 
	    calendar.set(Calendar.DAY_OF_MONTH,calendar.get(Calendar.DAY_OF_MONTH)-10);//让日期加1  
		Date dfrom=calendar.getTime();
		calendar.set(Calendar.DAY_OF_MONTH,calendar.get(Calendar.DAY_OF_MONTH)+9);//让日期加1  
		Date dTo=calendar.getTime();
		Map<String,Object> result2=cardWxApi.getCardInfo( cardId,dfrom,dTo,0);
		Assert.assertEquals(true,result2.containsKey("list")); 
	} 
	
	@Test
	public void getMemberCardInfo_Test(){  
		Calendar calendar=Calendar.getInstance();   
		calendar.setTime(new Date()); 
	    calendar.set(Calendar.DAY_OF_MONTH,calendar.get(Calendar.DAY_OF_MONTH)-10);//让日期加1  
		Date dfrom=calendar.getTime();
		calendar.set(Calendar.DAY_OF_MONTH,calendar.get(Calendar.DAY_OF_MONTH)+9);//让日期加1  
		Date dTo=calendar.getTime();
		Map<String,Object> result2=cardWxApi.getMemberCardInfo( dfrom,dTo,0);
		Assert.assertEquals(true,result2.containsKey("list")); 
	} 
	
	@Test
	public void getMemberCardDetail_Test(){ 
		
		Map<String,Object> result=cardWxApi.cardBatchGet( 0, 10, "CARD_STATUS_VERIFY_OK");//["CARD_STATUS_VERIFY_OK", "CARD_STATUS_DISPATCH"]
		Assert.assertEquals(true,result.containsKey("card_id_list") );
		List<String> list=(List<String>) result.get("card_id_list");
		log.debug(result);
		String cardId=list.get(0);
		
		Calendar calendar=Calendar.getInstance();   
		calendar.setTime(new Date()); 
	    calendar.set(Calendar.DAY_OF_MONTH,calendar.get(Calendar.DAY_OF_MONTH)-10);//让日期加1  
		Date dfrom=calendar.getTime();
		calendar.set(Calendar.DAY_OF_MONTH,calendar.get(Calendar.DAY_OF_MONTH)+9);//让日期加1  
		Date dTo=calendar.getTime();
		Map<String,Object> result2=cardWxApi.getMemberCardDetail( cardId,dfrom,dTo);
		Assert.assertEquals(true,result2.containsKey("list")); 
	} 
	 
}