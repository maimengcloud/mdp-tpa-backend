package com.mdp.tpa.wechat.wx.api;

import java.util.List;
import java.util.Map;

import com.mdp.tpa.wechat.api.AccessTokenService;
import com.mdp.tpa.wechat.api.CardConsumeService;
import com.mdp.tpa.wechat.api.CardService;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.mdp.core.entity.Tips;

/**
 * 测试从微信公众平台加载access_token的服务
 * @author cyc
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
public class TestWxApiCardConsumeService  {
	
	Log log=LogFactory.getLog(TestWxApiCardConsumeService.class);
	@Autowired
    CardConsumeService cardWxApiConsume;
	@Autowired
    CardService cardWxApi;

	@Autowired
	public AccessTokenService ats;
	String openid="oCFMX69qocTU4OEdJ0OaMcfsJ0hM";
	String openid2="oCFMX63ILM5oEpLsEsaqKf-jnnng";
	String appid="wx406faa04557b40c8";
	String authId="wx_pub_test_001";
	  
	 
	@Test
	public void codeGet_Test(){ 
		Map<String,Object> result=cardWxApi.cardBatchGet( 0, 10, "CARD_STATUS_VERIFY_OK");//["CARD_STATUS_VERIFY_OK", "CARD_STATUS_DISPATCH"]
		Assert.assertEquals(true,result.containsKey("card_id_list") );
		List<String> list=(List<String>) result.get("card_id_list");
		log.debug(result);
		String cardId=list.get(0);
		
		String openid=this.openid;
		Map<String,Object> result2=cardWxApi.userGetCardList( cardId,openid);
		Assert.assertEquals(true,result2.containsKey("card_list"));  
		List<Map<String,Object>> cardList=(List<Map<String, Object>>) result2.get("card_list"); 
		String code=(String) cardList.get(0).get("code");
		
		Map<String,Object> result3=cardWxApiConsume.codeGet( cardId,code,true);
		Assert.assertEquals(true,result2.containsKey("card")); 
	} 
	@Test
	public void codeConsume_Test(){ 
		Map<String,Object> result=cardWxApi.cardBatchGet( 0, 10, "CARD_STATUS_VERIFY_OK");//["CARD_STATUS_VERIFY_OK", "CARD_STATUS_DISPATCH"]
		Assert.assertEquals(true,result.containsKey("card_id_list") );
		List<String> list=(List<String>) result.get("card_id_list");
		log.debug(result);
		String cardId=list.get(0);
		
		String openid=this.openid;;
		Map<String,Object> result2=cardWxApi.userGetCardList( cardId,openid);
		Assert.assertEquals(true,result2.containsKey("card_list"));  
		List<Map<String,Object>> cardList=(List<Map<String, Object>>) result2.get("card_list"); 
		String code=(String) cardList.get(0).get("code");
		
		Map<String,Object> result3=cardWxApiConsume.codeGet( cardId,code,true);
		Assert.assertEquals(true,result3.containsKey("card")); 
		
		Map<String,Object> result4=cardWxApiConsume.codeConsume( code);
		Assert.assertEquals(true,result4.containsKey("openid")); 
	} 
	@Test
	public void codeUnavailable_Test(){ 
		Map<String,Object> result=cardWxApi.cardBatchGet( 0, 10, "CARD_STATUS_VERIFY_OK");//["CARD_STATUS_VERIFY_OK", "CARD_STATUS_DISPATCH"]
		Assert.assertEquals(true,result.containsKey("card_id_list") );
		List<String> list=(List<String>) result.get("card_id_list");
		log.debug(result);
		String cardId=list.get(0);
		
		String openid=this.openid;;
		Map<String,Object> result2=cardWxApi.userGetCardList( cardId,openid);
		Assert.assertEquals(true,result2.containsKey("card_list"));  
		List<Map<String,Object>> cardList=(List<Map<String, Object>>) result2.get("card_list");
		String code=(String) cardList.get(0).get("code");
		
		String reason="退款";
		Tips tips=cardWxApiConsume.codeUnavailable( cardId,code,reason);
		Assert.assertEquals(true,tips.isOk()); 
	} 
	 
	 
}