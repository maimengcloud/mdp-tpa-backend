package com.mdp.tpa.wechat.wx.api;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.mdp.tpa.wechat.api.AccessTokenService;
import com.mdp.tpa.wechat.api.MessageReplyService;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * 测试从微信公众平台加载access_token的服务
 * @author cyc
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
public class TestWxApiMessageReplyService {
	@Autowired
    MessageReplyService reply;

	@Autowired
	public AccessTokenService ats;
	String openid="oCFMX69qocTU4OEdJ0OaMcfsJ0hM";
	String openid2="oCFMX63ILM5oEpLsEsaqKf-jnnng";
	String appid="wx406faa04557b40c8";
	String authId="wx_pub_test_001";

	@Test
	public void news(){
		List<Map<String,Object>> items=new ArrayList<>(); 
		String toUserName="1111";
		String news=reply.news( toUserName, "2", 2010705, items);
		Assert.assertEquals(true,news.indexOf(toUserName) >0); 
	}
	@Test
	public void text(){ 
		String toUserName="1111";
		String news=reply.text( toUserName, "2", 2010705, "xxxxxxxxxxx");
		Assert.assertEquals(true,news.indexOf(toUserName) >0); 
	}
	@Test
	public void video(){ 
		String toUserName="1111";
		String news=reply.video( toUserName, "2", 2010705, "1","2","3");
		Assert.assertEquals(true,news.indexOf(toUserName) >0); 
	}
	@Test
	public void voice(){ 
		String toUserName="1111";
		String news=reply.voice( toUserName, "2", 2010705, "2222");
		Assert.assertEquals(true,news.indexOf(toUserName) >0); 
	}
	@Test
	public void image(){ 
		String toUserName="1111";
		String news=reply.image( toUserName, "2", 2010705, "2222");
		Assert.assertEquals(true,news.indexOf(toUserName) >0); 
	}
}
