package com.mdp.tpa.wechat.wx.api;

import com.mdp.tpa.wechat.api.AccessTokenService;
import com.mdp.tpa.wechat.api.WxaService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * 测试从微信公众平台加载access_token的服务
 * @author cyc
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
public class TestWxApiWxaService {
	@Autowired
	public AccessTokenService ats;
	String openid="oCFMX69qocTU4OEdJ0OaMcfsJ0hM";
	String openid2="oCFMX63ILM5oEpLsEsaqKf-jnnng";
	String appid="wx406faa04557b40c8";
	String authId="wx_pub_test_001";
	@Autowired
    WxaService wxApiWxa;
	@Test
	public void getWxacodeUnlimitAutoColor(){ 
		wxApiWxa.getWxacodeUnlimitAutoColor( "tttttt", "", 430);
	}
}
