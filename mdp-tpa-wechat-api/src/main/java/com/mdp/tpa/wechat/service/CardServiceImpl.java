package com.mdp.tpa.wechat.service;

import static com.mdp.core.utils.BaseUtils.map;

import java.io.File;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import com.mdp.tpa.wechat.HttpDownload;
import com.mdp.tpa.wechat.HttpRequestUtil;
import com.mdp.tpa.wechat.api.AccessTokenService;
import com.mdp.tpa.wechat.api.ApiUrls;
import com.mdp.tpa.wechat.api.CardService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.FileSystemResource;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import com.mdp.core.entity.Tips;
import com.mdp.core.err.BizException;
import com.mdp.core.utils.DateUtils;

@Service
public class CardServiceImpl implements CardService {

	@Autowired
	AccessTokenService tokenService;
	
	private static String DatePattern = "yyyy-MM-dd";
	
	

	@Override
	public Map<String, Object> cardBatchGet(int offset, int count, String status) {
		Map<String, Object> p = map("offset", offset, "count", count, "status", status,"status_list", Arrays.asList("CARD_STATUS_VERIFY_OK", "CARD_STATUS_DISPATCH"));
		String url=String.format(ApiUrls.CARD_BATCHGET,tokenService.getToken());
		Map<String,Object>	result= HttpRequestUtil.sendPostJson(url,p);
		return result;
	}

	@Override
	public Map<String, Object> createMemberCard( Map<String, Object> cardInfo) {
		Map<String,Object> p=cardInfo;
		String url=String.format(ApiUrls.CARD_CREATE,tokenService.getToken());
		Map<String,Object>	result= HttpRequestUtil.sendPostJson(url,p);
		return result;
	}

	@Override
	public Map<String, Object> createCoupon(Map<String, Object> cardInfo) {
		Map<String,Object> p=cardInfo;
		String url=String.format(ApiUrls.CARD_CREATE,tokenService.getToken());
		Map<String,Object>	result= HttpRequestUtil.sendPostJson(url,p);
		return result;
	}

	@Override
	public Map<String, Object> cardGet(String cardId) {

		Map<String, Object> p = map("card_id", cardId);
		String url=String.format(ApiUrls.CARD_GET,tokenService.getToken());
		Map<String,Object>	result= HttpRequestUtil.sendGetJson(url,p);
		return result;
	}

	@Override
	public Tips cardDelete(String cardId) {
		Tips tips = new Tips("创建优惠券成功");
		Map<String, Object> p = map("card_id", cardId);
		String url=String.format(ApiUrls.CARD_DELETE,tokenService.getToken());
		Map<String,Object>	result= HttpRequestUtil.sendPostJson(url,p);
		Integer errcode= (Integer) result.get("errcode");
		String errmsg= (String) result.get("errmsg");
		if(errcode!=0){
			tips.setErrMsg(errmsg);
		}
		return tips;
	}

	@Override
	public Tips cardUpdate(String cardId, Map<String, Object> cardInfo) {
		Tips tips = new Tips("修改优惠券成功");
		if (!cardInfo.containsKey("cardId")) {
			cardInfo.put("card_id", cardId);
		}
		Map<String,Object> p=cardInfo;
		String url=String.format(ApiUrls.CARD_DELETE,tokenService.getToken());
		Map<String,Object>	result= HttpRequestUtil.sendPostJson(url,p);
		Integer errcode= (Integer) result.get("errcode");
		String errmsg= (String) result.get("errmsg");
		if(errcode!=0){
			tips.setErrMsg(errmsg);
		}
		return tips;
	}

	@Override
	public Tips modifyStock(String cardId, int increaseStockValue, int reduceStockValue) {
		Tips tips = new Tips("修改库存成功");
		Map<String, Object> p = map("card_id", cardId);
		p.put("increase_stock_value", increaseStockValue);
		p.put("reduce_stock_value", reduceStockValue);
		String url=String.format(ApiUrls.CARD_MODIFYSTOCK,tokenService.getToken());
		Map<String,Object>	result= HttpRequestUtil.sendPostJson(url,p);
		Integer errcode= (Integer) result.get("errcode");
		String errmsg= (String) result.get("errmsg");
		if(errcode!=0){
			tips.setErrMsg(errmsg);
		}
		return tips;
	}

	@Override
	public Map<String, Object> userGetCardList(String cardId, String openid) {
		Map<String, Object> p = map("card_id", cardId, "openid", openid);
		String url=String.format(ApiUrls.CARD_USER_GETCARDLIST,tokenService.getToken());
		Map<String,Object>	result= HttpRequestUtil.sendPostJson(url,p);
		return result;
	}

	@Override
	public Map<String, Object> getCardBizuinInfo(Date beginDate, Date endDate, int condSource) {
		Map<String, Object> p = map("begin_date", DateUtils.format(beginDate, DatePattern), "end_date",
				DateUtils.format(endDate, DatePattern), "cond_source", condSource);

		String url=String.format(ApiUrls.CARD_DATACUBE_GETCARDBIZUININFO,tokenService.getToken());
		Map<String,Object>	result= HttpRequestUtil.sendPostJson(url,p);
		return result;
	}

	@Override
	public Map<String, Object> getCardInfo(String cardId, Date beginDate, Date endDate, int condSource) {
		Map<String, Object> p = map("begin_date", DateUtils.format(beginDate, DatePattern), "end_date",
				DateUtils.format(endDate, DatePattern), "cond_source", condSource);
		String url=String.format(ApiUrls.CARD_DATACUBE_GETCARDCARDINFO,tokenService.getToken());
		Map<String,Object>	result= HttpRequestUtil.sendPostJson(url,p);
		return result;
	}

	@Override
	public Map<String, Object> getMemberCardInfo(Date beginDate, Date endDate, int condSource) {
		Map<String, Object> p = map("begin_date", DateUtils.format(beginDate, DatePattern), "end_date",
				DateUtils.format(endDate, DatePattern), "cond_source", condSource);
		String url=String.format(ApiUrls.CARD_DATACUBE_GETCARDMEMBERCARDINFO,tokenService.getToken());
		Map<String,Object>	result= HttpRequestUtil.sendPostJson(url,p);
		return result;
	}

	@Override
	public Map<String, Object> getMemberCardDetail(String cardId, Date beginDate, Date endDate) {
		Map<String, Object> p = map("begin_date", DateUtils.format(beginDate, DatePattern), "end_date",
				DateUtils.format(endDate, DatePattern), "card_id", cardId);
		String url=String.format(ApiUrls.CARD_DATACUBE_GETCARDMEMBERCARDDETAIL,tokenService.getToken());
		Map<String,Object>	result= HttpRequestUtil.sendPostJson(url,p);
		return result;
	}

	@Override
	public Map<String, Object> uploadimg(String path) {
		// TODO Auto-generated method stub
		String url = "https://api.weixin.qq.com/cgi-bin/media/uploadimg?access_token="
				+ tokenService.getToken();
		RestTemplate rest = new RestTemplate();
		 HttpHeaders headers = new HttpHeaders();
		 File file=new File(path);
		 if(!file.exists()){
			 throw new BizException("文件不存在");
		 }
		 FileSystemResource fileSystemResource = new FileSystemResource(file);
		 MediaType type = MediaType.parseMediaType("multipart/form-data");
		 headers.setContentType(type);
		 String cd = "filename=\"" + file.getName() + "\"";
		 headers.add("Content-Disposition", cd);
		 MultiValueMap<String, Object> form = new LinkedMultiValueMap<String,
		 Object>();
		 form.add("buffer", fileSystemResource);
		 HttpEntity<MultiValueMap<String, Object>> files = new
		 HttpEntity<>(form, headers);
		 rest.getMessageConverters().add(new WxMappingJackson2HttpMessageConverter());
		 ResponseEntity<HashMap> responseEntity = null;
		 responseEntity = rest.postForEntity(url, files, HashMap.class);

		
		 Map<String, Object> map = new HashMap<>();
		 map=responseEntity.getBody();
		return map;

	}
	
	@Override
	public Map<String, Object> uploadimgByUrl( String url) {
		String filePath= HttpDownload.download(url);
		return this.uploadimg( filePath);
	}
}
