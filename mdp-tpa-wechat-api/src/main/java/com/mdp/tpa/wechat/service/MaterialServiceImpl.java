package com.mdp.tpa.wechat.service;

import java.util.HashMap;
import java.util.Map;

import com.mdp.tpa.wechat.HttpRequestUtil;
import com.mdp.tpa.wechat.api.AccessTokenService;
import com.mdp.tpa.wechat.api.ApiUrls;
import com.mdp.tpa.wechat.api.MaterialService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mdp.core.entity.Tips;

@Service
public class MaterialServiceImpl implements MaterialService {

	@Autowired
	AccessTokenService tokenService;

	@Override
	public String uploadimg( String filePathAndName, String media) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String uploadNews( String title, String thumbMediaId, String author, String digest, String showCoverPic,
			String content, String contentSourceUrl) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String addNews( String title, String thumbMediaId, String author, String digest, String showCoverPic,
			String content, String contentSourceUrl) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Tips updateNews( int mediaId, int index, String title, String thumbMediaId, String author, String digest,
			String showCoverPic, String content, String contentSourceUrl) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Tips delMaterial( int mediaId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Map<String, Object> batchgetMaterial( String type, int offset, int count) {
		Map<String,Object> map=new HashMap<>();
		map.put("type", type);
		map.put("offset",offset);
		map.put("count",count);
		String url=String.format(ApiUrls.MATERIAL_BATCHGET,tokenService.getToken());
		Map<String,Object>	result= HttpRequestUtil.sendPostJson(url,map);
		return result;
	}

	@Override
	public Map<String, Object> getMaterialcount(  ) {
		Map<String,Object> map=new HashMap<>();
		String url=String.format(ApiUrls.MATERIAL_MATERIALCOUNT,tokenService.getToken());
		Map<String,Object>	result= HttpRequestUtil.sendPostJson(url,map);
		return result;
	}
	
	

}
