package com.mdp.tpa.wechat.api.cache;

import com.mdp.core.api.CacheService;
import com.mdp.tpa.wechat.entity.AccessToken;

public interface AccessTokenCache extends CacheService<AccessToken> {
	
	/**
	 * 多少秒检查一遍token是否过期
	 */
	public static Integer  TokenCheckSeconds = 600;
	
	/**
	 * 多少毫秒检查一遍token是否过期
	 */
	public static long  TokenCheckMilliseconds = 600000;
	
	/**
	 * 剩余多少秒就重新向微信申请新的token
	 */
	public static Integer  LastTokenCheckSeconds = TokenCheckSeconds*2+200;

}
