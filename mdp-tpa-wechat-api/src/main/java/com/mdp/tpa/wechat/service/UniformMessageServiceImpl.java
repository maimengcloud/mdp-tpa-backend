package com.mdp.tpa.wechat.service;

import com.mdp.tpa.wechat.HttpRequestUtil;
import com.mdp.tpa.wechat.api.AccessTokenService;
import com.mdp.tpa.wechat.api.ApiUrls;
import com.mdp.tpa.wechat.api.UniformMessageService;
import com.mdp.tpa.wechat.entity.AccessToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

@Service
public class UniformMessageServiceImpl implements UniformMessageService {

    @Autowired
    AccessTokenService tokenService;
    /**
     *
     * @param touser 用户openid，可以是小程序的openid，也可以是mp_template_msg.appid对应的公众号的openid
     * @param mpTemplateMsg 公众号模板消息相关的信息，可以参考公众号模板消息接口；有此节点并且没有weapp_template_msg节点时，发送公众号模板消息
     *  mpTemplateMsg:{
     *        appid:公众号appid，要求与小程序有绑定且同主体,
     *        template_id: 公众号模板id,
     *        url: 公众号模板消息所要跳转的url,
     *        miniprogram: {公众号模板消息所要跳转的小程序，小程序的必须与公众号具有绑定关系,
     *             "appid":"xiaochengxuappid12345",
     *             "pagepath":"index?foo=bar"
     *        }
     *        data: 公众号模板消息的数据
     *  }
     *
     * @return
     *
     * 请求示例：
     * {
     *     "touser":"OPENID",
     *     "weapp_template_msg":{
     *         "template_id":"TEMPLATE_ID",
     *         "page":"page/page/index",
     *         "form_id":"FORMID",
     *         "data":{
     *             "keyword1":{
     *                 "value":"339208499"
     *             },
     *             "keyword2":{
     *                 "value":"2015年01月05日 12:30"
     *             },
     *             "keyword3":{
     *                 "value":"腾讯微信总部"
     *             },
     *             "keyword4":{
     *                 "value":"广州市海珠区新港中路397号"
     *             }
     *         },
     *         "emphasis_keyword":"keyword1.DATA"
     *     },
     *     "mp_template_msg":{
     *         "appid":"APPID ",
     *         "template_id":"TEMPLATE_ID",
     *         "url":"http://weixin.qq.com/download",
     *         "miniprogram":{
     *             "appid":"xiaochengxuappid12345",
     *             "pagepath":"index?foo=bar"
     *         },
     *         "data":{
     *             "first":{
     *                 "value":"恭喜你购买成功！",
     *                 "color":"#173177"
     *             },
     *             "keyword1":{
     *                 "value":"巧克力",
     *                 "color":"#173177"
     *             },
     *             "keyword2":{
     *                 "value":"39.8元",
     *                 "color":"#173177"
     *             },
     *             "keyword3":{
     *                 "value":"2014年9月22日",
     *                 "color":"#173177"
     *             },
     *             "remark":{
     *                 "value":"欢迎再次购买！",
     *                 "color":"#173177"
     *             }
     *         }
     *     }
     * }
     * 返回数：
     *
     * {
     *  "errcode": 0,
     *  "errmsg": "ok"
     * }
     *
     */
    @Override
    public Map<String, Object> sendTemplateMessage( String touser, Map<String, Object> mpTemplateMsg) {
        Map<String,Object> params=new HashMap<>();
        params.put("touser",touser);
        params.put("mp_template_msg",mpTemplateMsg);
        if(tokenService.getToken()==null){
            return Collections.emptyMap();
        }
        Map<String, Object> result= HttpRequestUtil.sendPostJson(String.format(ApiUrls.UNIFORM_SEND, tokenService.getToken()), params);

        return result;
    }
}
