package com.mdp.tpa.wechat.api;

import com.mdp.tpa.wechat.entity.ApiTicket;
import com.mdp.tpa.wechat.entity.JsapiTicket;

/**
 * 微信公众平台访问微信api的临时凭证，通过accessToken获取
 * @author Administrator
 *
 */
public interface TicketService {

	public ApiTicket getApiTicket();
	
	ApiTicket getApiTicket(String appid,String accessToken,boolean force);
	
	public JsapiTicket getJsapiTicket();
	
	JsapiTicket getJsapiTicket(String appid,String accessToken,boolean force);
}
