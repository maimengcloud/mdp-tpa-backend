package com.mdp.tpa.wechat.service;

import static com.mdp.core.utils.BaseUtils.map;

import java.util.List;
import java.util.Map;

import com.mdp.tpa.wechat.HttpRequestUtil;
import com.mdp.tpa.wechat.api.AccessTokenService;
import com.mdp.tpa.wechat.api.ApiUrls;
import com.mdp.tpa.wechat.api.UserTagsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mdp.core.entity.Tips;

@Service
public class UserTagsServiceImpl   implements UserTagsService {

	@Autowired
	AccessTokenService tokenService;

	@Override
	public Map<String, Object> tagsCreate(String tagName) {
 		Map<String,Object> p=map("tag",map("name",tagName));
		String url=String.format(ApiUrls.TAGS_CREATE,tokenService.getToken());
		Map<String,Object>	result= HttpRequestUtil.sendPostJson(url,p);
		return result;
	}

	@Override
	public Map<String, Object> tagsGet( ) {
		Map<String,Object> p=map();
		String url=String.format(ApiUrls.TAGS_GET,tokenService.getToken());
		Map<String,Object>	result= HttpRequestUtil.sendPostJson(url,p);
		return result;
	}

	@Override
	public Tips tagsUpdate(int tagId, String tagName) {
		Tips tips=new Tips("更新成功");
		Map<String,Object> p=map("tag",map("name",tagName));
		String url=String.format(ApiUrls.TAGS_UPDATE,tokenService.getToken());
		Map<String,Object>	result= HttpRequestUtil.sendPostJson(url,p);
		if(!"0".equals(""+result.get("errcode"))){
			tips.setErrMsg((String) result.get("errmsg"));
		}
		return tips;
	}

	@Override
	public Tips tagsDelete(String tagId) {
		Tips tips=new Tips("删除成功");
		Map<String,Object> p=map("tag",map("id",tagId));
		String url=String.format(ApiUrls.TAGS_UPDATE,tokenService.getToken());
		Map<String,Object>	result= HttpRequestUtil.sendPostJson(url,p);
		if(!"0".equals(""+result.get("errcode"))){
			tips.setErrMsg((String) result.get("errmsg"));
		}
		return tips;
	}

	@Override
	public Map<String, Object> getTagUsers(int tagId, String nextOpenid) {
		Map<String,Object> p=map("tagid",tagId,"next_openid",nextOpenid);
		String url=String.format(ApiUrls.TAGS_USER_TAG_GET,tokenService.getToken());
		Map<String,Object>	result= HttpRequestUtil.sendPostJson(url,p);
		return result;
	}

	@Override
	public Tips membersBatchTagging(int tagId, List<String> openids) {
		Tips tips=new Tips("更新成功");
		Map<String,Object> p= map("tagid",tagId,"openid_list",openids);

		String url=String.format(ApiUrls.TAGS_BATCHTAGGING,tokenService.getToken());
		Map<String,Object>	result= HttpRequestUtil.sendPostJson(url,p);
		if(!"0".equals(""+result.get("errcode"))){
			tips.setErrMsg((String) result.get("errmsg"));
		}
		return tips;
	}

	@Override
	public Map<String, Object> membersBatchUntagging(int tagId, List<String> openids) {
		Tips tips=new Tips("更新成功");
		Map<String,Object> p= map("tagid",tagId,"openid_list",openids);

		String url=String.format(ApiUrls.TAGS_BATCHUNTAGGING,tokenService.getToken());
		Map<String,Object>	result= HttpRequestUtil.sendPostJson(url,p);
		if(!"0".equals(""+result.get("errcode"))){
			tips.setErrMsg((String) result.get("errmsg"));
		}
		return tips;
	}

	@Override
	public Map<String, Object> getMemberTags(String openid) {
		Map<String,Object> p=map("openid",openid);
		String url=String.format(ApiUrls.TAGS_GETIDLIST,tokenService.getToken());
		Map<String,Object>	result= HttpRequestUtil.sendPostJson(url,p);
		return result;
	}

}
