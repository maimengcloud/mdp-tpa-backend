package com.mdp.tpa.wechat.service;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.mdp.tpa.wechat.HttpRequestUtil;
import com.mdp.tpa.wechat.api.AccessTokenService;
import com.mdp.tpa.wechat.api.ApiUrls;
import com.mdp.tpa.wechat.api.PoiService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class PoiServiceImpl implements PoiService {

	private static String DatePattern = "yyyy-MM-dd";
	@Autowired
	AccessTokenService tokenService;
	
	@Override
	public Map<String, Object> addPoi(Map<String, Object> poiInfo) {
		String url=String.format(ApiUrls.MATERIAL_BATCHGET,tokenService.getToken());
		Map<String,Object>	result= HttpRequestUtil.sendPostJson(url,poiInfo);
		return result;
	}

	/**
	 * 注意此处使用了个人开发者的key。请修改为企业开发者的key
	 */
	@Override
	public Map<String, Object> translate( List<Map<String,Object>> poiList) {
		String url = "http://apis.map.qq.com/ws/coord/v1/translate";
		String key="5IXBZ-JPUWF-PZUJP-J7LTT-557BQ-2BFRO";
		String type="";
		if (poiList.size()>0) {
			type=(String) poiList.get(0).get("offsetType");
		}
		String lngLat=new String();
		for (int i=0;i<poiList.size();i++) {
			BigDecimal lat = (BigDecimal) poiList.get(i).get("latitude");
			BigDecimal lng = (BigDecimal) poiList.get(i).get("longitude");
			if (i==poiList.size()-1) {
				lngLat+=lat+","+lng;
			}else{
				lngLat+=lat+","+lng+";";
			}
		}
		url=url+"?locations="+lngLat+"&type="+type.trim()+"&key="+key;
		RestTemplate rest = new RestTemplate(); 
		ResponseEntity<HashMap> responseEntity = rest.getForEntity(url, HashMap.class);
		Map<String,Object> body = responseEntity.getBody();
		return body;
	}
	
	

	

}
