package com.mdp.tpa.wechat.service.cache.redis;

import java.util.concurrent.TimeUnit;

import com.mdp.tpa.wechat.api.cache.ApiTicketCache;
import com.mdp.tpa.wechat.entity.ApiTicket;
import org.springframework.stereotype.Service;

@Service
public class RedisApiTicketCacheService extends DefaultRedisCacheService<ApiTicket> implements ApiTicketCache {
	@Override
	public String getCacheKey() {
		// TODO Auto-generated method stub
		return "mdp_wxapi_ApiTicket";
	}
	public   void put(String key, ApiTicket o) {
		String okey=this.getCacheKey()+":"+key;
		cacheHKVService.put(okey, key, o);
		cacheHKVService.expire(okey, o.getExpiresIn(), TimeUnit.SECONDS);
	}
	@Override
	public ApiTicket get(String key) {
		String okey=this.getCacheKey()+":"+key;
		return  (ApiTicket) cacheHKVService.get(okey, key);
	}
}
