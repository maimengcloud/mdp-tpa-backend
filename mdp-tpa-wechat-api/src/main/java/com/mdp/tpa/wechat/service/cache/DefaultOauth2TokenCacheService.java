package com.mdp.tpa.wechat.service.cache;

import com.mdp.tpa.wechat.service.cache.redis.DefaultRedisCacheService;
import com.mdp.tpa.wechat.api.cache.Oauth2TokenCache;
import com.mdp.tpa.wechat.entity.Oauth2AccessToken;

public class DefaultOauth2TokenCacheService extends DefaultRedisCacheService<Oauth2AccessToken> implements Oauth2TokenCache {


    @Override
    public String getCacheKey() {
        return DefaultOauth2TokenCacheService.class.getName();
    }


}
