package com.mdp.tpa.wechat.api;

import java.util.Map;

/**
 * 微信关注、取消关注公众号的推送事件处理
 * 由微信主动推送到第三方系统
 */
public interface SubscribeEventService {
	
	/**
	 * 卡券api推送事件入口
	 * @param event
	 * @param msgType
	 * @param fromUser
	 * @param toUser
	 * @param createTime
	 * @param info
	 */
	public void doEvent( String event,String msgType,String fromUser,String toUser,String createTime,Map<String,Object> info);
	
	
	/**
	用户在关注与取消关注公众号时，微信会把这个事件推送到开发者填写的URL。方便开发者给用户下发欢迎消息或者做帐号的解绑。
	微信服务器在五秒内收不到响应会断掉连接，并且重新发起请求，总共重试三次。
	关于重试的消息排重，推荐使用FromUserName + CreateTime 排重。
	假如服务器无法保证在五秒内处理并回复，可以直接回复空串，微信服务器不会对此作任何处理，并且不会发起重试。 
	事件类型，subscribe(订阅)、unsubscribe(取消订阅)
	<xml>
		<ToUserName><![CDATA[toUser]]></ToUserName>
		<FromUserName><![CDATA[FromUser]]></FromUserName>
		<CreateTime>123456789</CreateTime>
		<MsgType><![CDATA[event]]></MsgType>
		<Event><![CDATA[subscribe]]></Event>
	</xml>
	**/
	public void subscribe( String event,String msgType,String fromUser,String toUser,String createTime,Map<String,Object> info);
	 
	/**
	用户在关注与取消关注公众号时，微信会把这个事件推送到开发者填写的URL。方便开发者给用户下发欢迎消息或者做帐号的解绑。
	微信服务器在五秒内收不到响应会断掉连接，并且重新发起请求，总共重试三次。
	关于重试的消息排重，推荐使用FromUserName + CreateTime 排重。
	假如服务器无法保证在五秒内处理并回复，可以直接回复空串，微信服务器不会对此作任何处理，并且不会发起重试。 
	事件类型，subscribe(订阅)、unsubscribe(取消订阅)
	<xml>
		<ToUserName><![CDATA[toUser]]></ToUserName>
		<FromUserName><![CDATA[FromUser]]></FromUserName>
		<CreateTime>123456789</CreateTime>
		<MsgType><![CDATA[event]]></MsgType>
		<Event><![CDATA[subscribe]]></Event>
	</xml>
	**/
	public void unsubscribe( String event,String msgType,String fromUser,String toUser,String createTime,Map<String,Object> info);
}
