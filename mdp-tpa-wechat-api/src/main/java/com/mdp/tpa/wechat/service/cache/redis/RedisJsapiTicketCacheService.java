package com.mdp.tpa.wechat.service.cache.redis;

import java.util.concurrent.TimeUnit;

import com.mdp.tpa.wechat.api.cache.JsapiTicketCache;
import com.mdp.tpa.wechat.entity.JsapiTicket;
import org.springframework.stereotype.Service;

@Service
public class RedisJsapiTicketCacheService extends DefaultRedisCacheService<JsapiTicket> implements JsapiTicketCache {
	@Override
	public String getCacheKey() {
		// TODO Auto-generated method stub
		return "mdp_wxapi_JsapiTicket";
	}
	public   void put(String key, JsapiTicket o) {
		String okey=this.getCacheKey()+":"+key;
		cacheHKVService.put(okey, key, o);
		cacheHKVService.expire(okey, o.getExpiresIn(), TimeUnit.SECONDS);
	}
	
	@Override
	public JsapiTicket get(String key) {
		String okey=this.getCacheKey()+":"+key;
		return  (JsapiTicket) cacheHKVService.get(okey, key);
	}

}
