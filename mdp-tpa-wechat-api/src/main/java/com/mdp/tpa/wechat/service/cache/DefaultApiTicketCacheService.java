package com.mdp.tpa.wechat.service.cache;

import com.mdp.tpa.wechat.entity.ApiTicket;
import com.mdp.core.service.DefaultCacheService;
import com.mdp.tpa.wechat.api.cache.ApiTicketCache;

public class DefaultApiTicketCacheService extends DefaultCacheService<ApiTicket> implements ApiTicketCache {

}
