package com.mdp.tpa.wechat.entity;

import java.io.Serializable;

public class ApiTicket implements Serializable{
	
	private static final long serialVersionUID = -6052097987186529033L;
	
	String ticket;
	Integer expiresIn;
	Long ctime;
	
	public String getTicket() {
		return ticket;
	}
	public void setTicket(String ticket) {
		this.ticket = ticket;
	}
	public Integer getExpiresIn() {
		return expiresIn;
	}
	public void setExpiresIn(Integer expiresIn) {
		this.expiresIn = expiresIn;
	}
	public Long getCtime() {
		return ctime;
	}
	public void setCtime(Long ctime) {
		this.ctime = ctime;
	}
	 
	
	
}
