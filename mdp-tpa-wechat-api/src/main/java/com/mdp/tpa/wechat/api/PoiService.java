package com.mdp.tpa.wechat.api;

import java.util.List;
import java.util.Map;
/**
 * 向微信拉取或者更新数据接口到第三方
 * 由第三方主动发起
 * @author Administrator
 *
 */
public interface PoiService {

	/**
	 * 创建微信门店<P/>
	 * 创建门店接口是为商户提供创建自己门店数据的接口，门店数据字段越完整，商户页面展示越丰富，越能够吸引更多用户，并提高曝光度。创建门店接口调用成功后会返回errcode 0、errmsg ok，会实时返回唯一的poiid。
	 * HTTP请求方式: POST<P/>
	 * URL: 	http://api.weixin.qq.com/cgi-bin/poi/addpoi?access_token=TOKEN
	 * @param poiInfo
	 * @return
	 */
	public Map<String,Object> addPoi( Map<String,Object> poiInfo);
	
	/**
	 * 经纬度转换为腾讯地图经纬度<P/>
	 * 实现从其它地图供应商坐标系或标准GPS坐标系，批量转换到腾讯地图坐标系。
	 * HTTP请求方式: GET<P/>
	 * URL: 	http://apis.map.qq.com/ws/coord/v1/translate
	 * 个人腾讯地图key:请注意更换为企业key:5IXBZ-JPUWF-PZUJP-J7LTT-557BQ-2BFRO
	 * @param poiInfo
	 * @return
	 */
	public Map<String,Object> translate( List<Map<String,Object>> poiList);
	
}
