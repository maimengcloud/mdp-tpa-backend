package com.mdp.tpa.wechat.util;

import java.io.IOException;
import java.util.Base64;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
 

public class WxaSecurityUtil {
	
	public static ObjectMapper o=new ObjectMapper();
	public static Log log=LogFactory.getLog(WxaSecurity.class);
	
    public static void main(String[] args) { 
        String  encryptedData = "CiyLU1Aw2KjvrjMdj8YKliAjtP4gsMZMQmRzooG2xrDcvSnxIMXFufNstNGTyaGS9uT5geRa0W4oTOb1WT7fJlAC+oNPdbB+3hVbJSRgv+4lGOETKUQz6OYStslQ142dNCuabNPGBzlooOmB231qMM85d2/fV6ChevvXvQP8Hkue1poOFtnEtpyxVLW1zAo6/1Xx1COxFvrc2d7UL/lmHInNlxuacJXwu0fjpXfz/YqYzBIBzD6WUfTIF9GRHpOn/Hz7saL8xz+W//FRAUid1OksQaQx4CMs8LOddcQhULW4ucetDf96JcR3g0gfRK4PC7E/r7Z6xNrXd2UIeorGj5Ef7b1pJAYB6Y5anaHqZ9J6nKEBvB4DnNLIVWSgARns/8wR2SiRS7MNACwTyrGvt9ts8p12PKFdlqYTopNHR1Vf7XjfhQlVsAJdNiKdYmYVoKlaRv85IfVunYzO0IKXsyl7JCUjCpoG20f0a04COwfneQAGGwd5oa+T8yO5hzuyDb/XcxxmK01EpqOyuxINew==";
        String iv="r7BXXKkLb8qrSNn05n0qiA==";
        String appid = "wx4f4bc4dec97d474b";
        String sessionKey = "tiihtNczf5v6AKRyjwEUhQ==";
        Map map =decrypt(sessionKey,encryptedData,iv);
        log.debug(map);
    }
 
    public static Map<String,Object>  decrypt(String sessionKey,String  encryptedData ,String iv){ 
        byte[] sessionKeyBy =Base64.getDecoder().decode(sessionKey);
        byte[] encryptedDataBy = Base64.getDecoder().decode(encryptedData);
        byte[] ivBy = Base64.getDecoder().decode(iv);
        byte[] dec = WxaSecurity.decryptOfDiyIV(encryptedDataBy, sessionKeyBy,ivBy);
        	try {
				Map map=o.readValue(dec, HashMap.class);
				return map; 
			} catch (JsonParseException e) {
				 log.error("",e);
			} catch (JsonMappingException e) {
				 log.error("",e);
			} catch (IOException e) {
				 log.error("",e);
			} 
          return null;
    }
    
    public static String signature(String rawData,String sessionKey){
    	return SHA1.getWxaSHA1(rawData, sessionKey);
    }
 
}