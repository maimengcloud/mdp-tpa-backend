package com.mdp.tpa.wechat.api;

import java.util.List;
import java.util.Map;

import com.mdp.core.entity.Tips;

/**
 * 微信用户标签管理
 * 开发者可以使用用户标签管理的相关接口，实现对公众号的标签进行创建、查询、修改、删除等操作，也可以对用户进行打标签、取消标签等操作。
 * @author Administrator
 *
 */
public interface UserTagsService {
	
	/**
	 *  创建标签 </p>
	 *	一个公众号，最多可以创建100个标签。</p>
	 * http请求方式：POST（请使用https协议） </p>
     * https://api.weixin.qq.com/cgi-bin/tags/create?access_token=ACCESS_TOKEN</p>
	 * @param tagName 标签名
	 * @return
		{</p>
		  "tag":{</p>
			  "id":134,//标签id</p>
			  "name":"广东"</p>
			}</p>
		}</p>
	 */
	public Map<String,Object> tagsCreate( String tagName);
	
	/**
	 *  获取公众号已创建的标签 </p> 
	 * http请求方式：GET（请使用https协议） </p>
     * https://api.weixin.qq.com/cgi-bin/tags/get?access_token=ACCESS_TOKEN</p>
	 * @return
		{</p> 
		  "tags":[{</p> 
		      "id":1,</p> 
		      "name":"每天一罐可乐星人",</p> 
		      "count":0 //此标签下粉丝数</p> 
			},{</p> 
			  "id":2,</p> 
			  "name":"星标组",</p> 
			  "count":0</p> 
			},{</p> 
			  "id":127,</p> 
			  "name":"广东",</p> 
			  "count":5</p> 
			}</p> 
		  ]</p> 
		}</p> 
	 */
	public Map<String,Object> tagsGet( );
	
	/**
	 *   编辑标签</p> 
	 * http请求方式：POST（请使用https协议） </p>
     * https://api.weixin.qq.com/cgi-bin/tags/update?access_token=ACCESS_TOKEN</p>
     * @param tagId 标签编号
	 * @param tagName 标签名
	 * @return 
		{</p>
		  "errcode":0,</p>
		  "errmsg":"ok"</p>
		}</p>
	 */
	public Tips tagsUpdate( int tagId,String tagName);
	
	/**
	 *  删除标签</p>
	 *	请注意，当某个标签下的粉丝超过10w时，后台不可直接删除标签。此时，开发者可以对该标签下的openid列表，先进行取消标签的操作，直到粉丝数不超过10w后，才可直接删除该标签。</p>
	 * http请求方式：POST（请使用https协议） </p>
     * https://api.weixin.qq.com/cgi-bin/tags/delete?access_token=ACCESS_TOKEN</p>
	 * @param tagId 标签编号
	 * @return 
		{</p>
		  "errcode":0,</p>
		  "errmsg":"ok"</p>
		}</p>
		
		错误码：</p>
		-1	系统繁忙</p>
		45058    不能修改0/1/2这三个系统默认保留的标签</p>
		45057     该标签下粉丝数超过10w，不允许直接删除</p>
	 */
	public Tips tagsDelete( String tagId);
	 
	/**
	 *  获取标签下粉丝列表 </p>
	 *	一个公众号，最多可以创建100个标签。</p>
	 * http请求方式：POST（请使用https协议） </p>
     * https://api.weixin.qq.com/cgi-bin/user/tag/get?access_token=ACCESS_TOKEN</p>
	 * @param tagId 标签编号
	 * @param nextOpenid 第一个拉取的OPENID，不填默认从头开始拉取
	 * @return
		{</p>
		  "count":2,//这次获取的粉丝数量</p>
		  "data":{//粉丝列表</p>
		  "openid":[</p>
		     "ocYxcuAEy30bX0NXmGn4ypqx3tI0",</p>
		     "ocYxcuBt0mRugKZ7tGAHPnUaOW7Y"</p>
		    ]</p>
		  },</p>
		  "next_openid":"ocYxcuBt0mRugKZ7tGAHPnUaOW7Y"//拉取列表最后一个用户的openid</p>
		}</p>
	 */
	public Map<String,Object> getTagUsers( int tagId,String nextOpenid);
	
	/**
	 *  批量为用户打标签 </p>
	 *	标签功能目前支持公众号为用户打上最多20个标签。</p>
	 * http请求方式：POST（请使用https协议） </p>
     * https://api.weixin.qq.com/cgi-bin/tags/members/batchtagging?access_token=ACCESS_TOKEN</p>
     * @param tagId 标签编号
	 * @param openids openid列表
	 * @return
		{</p>
		  "errcode":0,</p>
		  "errmsg":"ok"</p>
		}</p>
	 */
	public Tips membersBatchTagging( int tagId,List<String> openids);
	
	/**
	 *  批量为用户取消标签 </p>
	 *	标签功能目前支持公众号为用户打上最多20个标签。</p>
	 * http请求方式：POST（请使用https协议） </p>
     * https://api.weixin.qq.com/cgi-bin/tags/members/batchuntagging?access_token=ACCESS_TOKEN</p>
     * @param tagId 标签编号
	 * @param openids openid列表
	 * @return
		{</p>
		  "errcode":0,</p>
		  "errmsg":"ok"</p>
		}</p>
	 */
	public Map<String,Object> membersBatchUntagging( int tagId,List<String> openids);
	
	/**
	 *  获取用户身上的标签列表 </p> 
	 * http请求方式：POST（请使用https协议） </p>
     * https://api.weixin.qq.com/cgi-bin/tags/getidlist?access_token=ACCESS_TOKEN</p>
	 * @param tagName 标签名
	 * @return
		{</p>
		  "tagid_list":[//被置上的标签列表</p>
		    134,</p>
		    2</p>
		   ]</p>
		}</p>
	 */
	public Map<String,Object> getMemberTags( String openid);
}
