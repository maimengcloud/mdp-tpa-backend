package com.mdp.tpa.wechat.service.cache;

import com.mdp.tpa.wechat.service.cache.redis.DefaultRedisCacheService;
import com.mdp.tpa.wechat.entity.Oauth2AccessToken;
import com.mdp.tpa.wechat.api.cache.Oauth2CodeCache;

public class DefaultOauth2CodeCacheService extends DefaultRedisCacheService<Oauth2AccessToken> implements Oauth2CodeCache {

    @Override
    public String getCacheKey() {
        return DefaultOauth2CodeCacheService.class.getName();
    }
}
