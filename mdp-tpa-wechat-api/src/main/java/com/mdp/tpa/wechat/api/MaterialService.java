package com.mdp.tpa.wechat.api;

import java.util.Map;

import com.mdp.core.entity.Tips;

/**
 * 素材管理接口
 * @author Administrator
 *
 */
public interface MaterialService {
	
	/**
	 * 上传图文消息内的图片获取URL </p>
	 * http请求方式: POST，https协议</p>
	 * https://api.weixin.qq.com/cgi-bin/media/uploadimg?access_token=ACCESS_TOKEN</p>
	 * @param filePathAndName 要上传的文件路径及文件名
	 * @param media   必输 form-data中媒体文件标识，有filename、filelength、content-type等信息
	 * @return
	 * {
    	 "url":  "http://mmbiz.qpic.cn/mmbiz/gLO17UPS6FS2xsypf378iaNhWacZ1G1UplZYWEYfwvuU6Ont96b1roYs CNFwaRrSaKTPCUdBK9DgEHicsKwWCBRQ/0"
		}
	 */
	public String uploadimg( String filePathAndName,String media);
	
	
	/**
	 * 上传临时图文消息素材
	 * http请求方式: POST，https协议
	 * https://api.weixin.qq.com/cgi-bin/media/uploadnews?access_token=ACCESS_TOKEN
	 * @param title 标题
	 * @param thumbMediaId 图文消息的封面图片素材id（必须是永久mediaID） 必输
	 * @param author 作者 非必输
	 * @param digest 非必输  图文消息的摘要，仅有单图文消息才有摘要，多图文此处为空。如果本字段为没有填写，则默认抓取正文前64个字。
	 * @param showCoverPic  必输 是否显示封面，0为false，即不显示，1为true，即显示
	 * @param content  必输 图文消息的具体内容，支持HTML标签，必须少于2万字符，小于1M，且此处会去除JS,涉及图片url必须来源"上传图文消息内的图片获取URL"接口获取。外部图片url将被过滤。
	 * @param contentSourceUrl   必输 图文消息的原文地址，即点击“阅读原文”后的URL
	 * @return
	 * {
		  "media_id":MEDIA_ID
		}
	 */
	public String uploadNews( String title,String thumbMediaId,String author,String digest,String showCoverPic,String content,String contentSourceUrl);
	
	

	/**
	 * 新增永久图文素材
	 * http请求方式: POST，https协议
	 * https://api.weixin.qq.com/cgi-bin/material/add_news?access_token=ACCESS_TOKEN
	 * @param title 标题
	 * @param thumbMediaId 图文消息的封面图片素材id（必须是永久mediaID） 必输
	 * @param author 作者 非必输
	 * @param digest 非必输  图文消息的摘要，仅有单图文消息才有摘要，多图文此处为空。如果本字段为没有填写，则默认抓取正文前64个字。
	 * @param showCoverPic  必输 是否显示封面，0为false，即不显示，1为true，即显示
	 * @param content  必输 图文消息的具体内容，支持HTML标签，必须少于2万字符，小于1M，且此处会去除JS,涉及图片url必须来源"上传图文消息内的图片获取URL"接口获取。外部图片url将被过滤。
	 * @param contentSourceUrl   必输 图文消息的原文地址，即点击“阅读原文”后的URL
	 * @return
	 * {
		  "media_id":MEDIA_ID
		}
	 */
	public String addNews( String title,String thumbMediaId,String author,String digest,String showCoverPic,String content,String contentSourceUrl);
	

	
	/**
	 *	修改永久图文素材</p>
	 *	开发者可以通过本接口对永久图文素材进行修改。</p>
	 *	请注意：</p>
	 *	1、也可以在公众平台官网素材管理模块中保存的图文消息（永久图文素材）</p>
	 *	2、调用该接口需https协议</p>
	 * http请求方式: POST，https协议</p>
	 * https://api.weixin.qq.com/cgi-bin/material/add_news?access_token=ACCESS_TOKEN</p>
	 * @param mediaId 要修改的图文消息的id
	 * @param index 要更新的文章在图文消息中的位置（多图文消息时，此字段才有意义），第一篇为0
	 * @param title 标题
	 * @param thumbMediaId 图文消息的封面图片素材id（必须是永久mediaID） 必输
	 * @param author 作者 非必输
	 * @param digest 非必输  图文消息的摘要，仅有单图文消息才有摘要，多图文此处为空。如果本字段为没有填写，则默认抓取正文前64个字。
	 * @param showCoverPic  必输 是否显示封面，0为false，即不显示，1为true，即显示
	 * @param content  必输 图文消息的具体内容，支持HTML标签，必须少于2万字符，小于1M，且此处会去除JS,涉及图片url必须来源"上传图文消息内的图片获取URL"接口获取。外部图片url将被过滤。
	 * @param contentSourceUrl   必输 图文消息的原文地址，即点击“阅读原文”后的URL
	 * @return
	 * {</p>
		 "errcode": 0,</p>
		 "errmsg": "ok"</p>
		}</p>
	 */
	public Tips updateNews( int mediaId,int index,String title,String thumbMediaId,String author,String digest,String showCoverPic,String content,String contentSourceUrl);

	/**
	 * 删除永久素材
	 * http请求方式: POST
	 * https://api.weixin.qq.com/cgi-bin/material/del_material?access_token=ACCESS_TOKEN 
	 * @param mediaId 素材编号
	 * @return
	 * {</p>
		 "errcode": 0,</p>
		 "errmsg": "ok"</p>
	    }</p>
	 */
	public Tips delMaterial( int mediaId);
	
	/**
	 * 在新增了永久素材后，开发者可以分类型获取永久素材的列表。</p>
	 * 请注意：</p>
	 * 	1、获取永久素材的列表，也包含公众号在公众平台官网素材管理模块中新建的图文消息、语音、视频等素材</p>
	 * 	2、临时素材无法通过本接口获取</p>
	 * 	3、调用该接口需https协议</p>
	 * http请求方式: POST</p>
	 * https://api.weixin.qq.com/cgi-bin/material/batchget_material?access_token=ACCESS_TOKEN</p>
	 * @param type 素材的类型，图片（image）、视频（video）、语音 （voice）、图文（news）
	 * @param offset 从全部素材的该偏移位置开始返回，0表示从第一个素材 返回
	 * @param COUNT 返回素材的数量，取值在1到20之间
	 * @return
	 * 永久图文消息素材列表的响应如下：</p>
	 * {</p>
		  "total_count": TOTAL_COUNT,</p>
		  "item_count": ITEM_COUNT,</p>
		  "item": [{</p>
		      "media_id": MEDIA_ID,</p>
		      "content": {</p>
		          "news_item": [{</p>
		              "title": TITLE,</p>
		              "thumb_media_id": THUMB_MEDIA_ID,</p>
		              "show_cover_pic": SHOW_COVER_PIC(0 / 1),</p>
		              "author": AUTHOR,</p>
		              "digest": DIGEST,</p>
		              "content": CONTENT,</p>
		              "url": URL,</p>
		              "content_source_url": CONTETN_SOURCE_URL</p>
		          },</p>
		          //多图文消息会在此处有多篇文章</p>
		          ]</p>
		       },</p>
		       "update_time": UPDATE_TIME</p>
		   },</p>
		   //可能有多个图文消息item结构</p>
		 ]</p>
		}</p>
		
		其他类型（图片、语音、视频）的返回如下：</p>
		{</p>
		  "total_count": TOTAL_COUNT,</p>
		  "item_count": ITEM_COUNT,</p>
		  "item": [{</p>
		      "media_id": MEDIA_ID,</p>
		      "name": NAME,</p>
		      "update_time": UPDATE_TIME,</p>
		      "url":URL</p>
		  },</p>
		  //可能会有多个素材</p>
		  ]</p>
		}</p>
	 */
	public Map<String,Object> batchgetMaterial( String type,int offset,int count);
	
	
	/**
	 * 开发者可以根据本接口来获取永久素材的列表，需要时也可保存到本地。
	 * 请注意：
	 *	1.永久素材的总数，也会计算公众平台官网素材管理中的素材
	 *	2.图片和图文消息素材（包括单图文和多图文）的总数上限为5000，其他素材的总数上限为1000
	 *	3.调用该接口需https协议
	 *  http请求方式: GET
     *   https://api.weixin.qq.com/cgi-bin/material/get_materialcount?access_token=ACCESS_TOKEN
     *   
     *   {
	 *	  "voice_count":COUNT,
	 *	  "video_count":COUNT,
	 *	  "image_count":COUNT,
	 *	  "news_count":COUNT
	 *	}
	 * @return
	 */
	public Map<String,Object> getMaterialcount( );
	
}
