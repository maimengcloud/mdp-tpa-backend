package com.mdp.tpa.wechat.service.cache;

import com.mdp.tpa.wechat.entity.JsapiTicket;
import com.mdp.core.service.DefaultCacheService;
import com.mdp.tpa.wechat.api.cache.JsapiTicketCache;

public class DefaultJsapiTicketCacheService extends DefaultCacheService<JsapiTicket> implements JsapiTicketCache {

}
