package com.mdp.tpa.wechat.api.cache;

import com.mdp.core.api.CacheService;
import com.mdp.tpa.wechat.entity.JsapiTicket;

public interface JsapiTicketCache extends CacheService<JsapiTicket> {

}
