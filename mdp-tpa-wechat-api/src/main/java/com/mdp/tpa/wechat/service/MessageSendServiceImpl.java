package com.mdp.tpa.wechat.service;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.mdp.tpa.wechat.HttpRequestUtil;
import com.mdp.tpa.wechat.api.AccessTokenService;
import com.mdp.tpa.wechat.api.ApiUrls;
import com.mdp.tpa.wechat.api.MessageSendService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class MessageSendServiceImpl implements MessageSendService {


	@Autowired
	AccessTokenService tokenService;

	@Override
	public Map<String, Object> sendMpNewsByTag(String  mediaId, int tagId) {
		// TODO Auto-generated method stub
		String url=String.format(ApiUrls.MSG_SEND_ALL, tokenService.getToken());
		/**
		 * {
			   "filter":{
			      "is_to_all":false,
			      "tag_id":tag_id
			   },
			   "mpnews":{
			      "media_id":"media_id"
			   },
			    "msgtype":"mpnews",
			    "send_ignore_reprint":0
			}
		 */
		
		
		Map<String,Object> params=new HashMap<>();
		params.put("msgtype", "mpnews");
		params.put("send_ignore_reprint", 0);
		Map<String,Object> filter=new HashMap<>();
		filter.put("is_to_all",tagId==0?true:false);
		if(tagId!=0){
			filter.put("tag_id", tagId);
		}
		params.put("filter", filter);
		Map<String,Object> mpnews=new HashMap<>();
		mpnews.put("media_id", mediaId);
		params.put("mpnews", mpnews);
		Map<String,Object> result= HttpRequestUtil.sendPostJson(url, params);
		return result;
	}

	@Override
	public Map<String, Object> sendTextByTag(String  content, int tagId) {
		/**
		 * {
			   "filter":{
			      "is_to_all":false,
			      "tag_id":[[${tagId}]]
			   },
			   "text":{
			      "content":"[[${content}]]"
			   },
			    "msgtype":"text",
			    "send_ignore_reprint":0
			}
		 */
		
		String url=String.format(ApiUrls.MSG_SEND_ALL, tokenService.getToken());

		Map<String,Object> params=new HashMap<>();
		params.put("msgtype", "text");
		params.put("send_ignore_reprint", 0);
		Map<String,Object> filter=new HashMap<>();
		filter.put("is_to_all",tagId==0?true:false);
		if(tagId!=0){
			filter.put("tag_id", tagId);
		} 
		params.put("filter", filter);
		Map<String,Object> text=new HashMap<>();
		text.put("content", content);
		params.put("text", text);
		Map<String,Object> result=HttpRequestUtil.sendPostJson(url, params);
		return result;
 	}

	@Override
	public Map<String, Object> sendVoiceByTag(String  mediaId, int tagId) {
		String url=String.format(ApiUrls.MSG_SEND_ALL, tokenService.getToken());
		/**
		 * {
			   "filter":{
			      "is_to_all":false,
			      "tag_id":tag_id
			   },
			   "voice":{
			      "media_id":"media_id"
			   },
			    "msgtype":"voice",
			    "send_ignore_reprint":0
			}
		 */
		
		
		Map<String,Object> params=new HashMap<>();
		params.put("msgtype", "voice");
		params.put("send_ignore_reprint", 0);
		Map<String,Object> filter=new HashMap<>();
		filter.put("is_to_all",tagId==0?true:false);
		if(tagId!=0){
			filter.put("tag_id", tagId);
		} 
		params.put("filter", filter);
		Map<String,Object> voice=new HashMap<>();
		voice.put("media_id", mediaId);
		params.put("voice", voice);
		Map<String,Object> result=HttpRequestUtil.sendPostJson(url, params);
		return result;
	}

	@Override
	public Map<String, Object> sendImageByTag(List<String>  mediaIds,String recommend, int tagId) {
		String url=String.format(ApiUrls.MSG_SEND_ALL, tokenService.getToken());
		/**
		 * {
			   "filter":{
			      "is_to_all":false,
			      "tag_id":tag_id
			   },
			   "images":{
			      "media_ids": [
			         "aaa",
			         "bbb",
			         "ccc"
			      ],
			      "recommend": "xxx",
			      "need_open_comment": 1,
			      "only_fans_can_comment": 0
			   },
			    "msgtype":"voice",
			    "send_ignore_reprint":0
			}
		 */
		
		
		Map<String,Object> params=new HashMap<>();
		params.put("msgtype", "images");
		params.put("send_ignore_reprint", 0);
		Map<String,Object> filter=new HashMap<>();
		filter.put("is_to_all",tagId==0?true:false);
		if(tagId!=0){
			filter.put("tag_id", tagId);
		}
		params.put("filter", filter);
		Map<String,Object> images=new HashMap<>();
		images.put("media_ids", mediaIds);
		images.put("recommend", recommend);
		images.put("need_open_comment", 1);
		images.put("only_fans_can_comment", 0);
		params.put("images", images);
		Map<String,Object> result=HttpRequestUtil.sendPostJson(url, params);
		return result;
	}

	@Override
	public Map<String, Object> sendMpVideoByTag(String  mediaId, int tagId) {
		String url=String.format(ApiUrls.MSG_SEND_ALL, tokenService.getToken());
		/**
		 * {
			   "filter":{
			      "is_to_all":false,
			      "tag_id":tag_id
			   },
			   "video":{
			      "media_id":"media_id"
			   },
			    "msgtype":"video",
			    "send_ignore_reprint":0
			}
		 */
		
		
		Map<String,Object> params=new HashMap<>();
		params.put("msgtype", "video");
		params.put("send_ignore_reprint", 0);
		Map<String,Object> filter=new HashMap<>();
		filter.put("is_to_all",tagId==0?true:false);
		if(tagId!=0){
			filter.put("tag_id", tagId);
		}
		params.put("filter", filter);
		Map<String,Object> video=new HashMap<>();
		video.put("media_id", mediaId);
		params.put("video", video);
		Map<String,Object> result=HttpRequestUtil.sendPostJson(url, params);
		return result;
	}

	@Override
	public Map<String, Object> sendWxCardByTag(String  cardId, int tagId) {
		String url=String.format(ApiUrls.MSG_SEND_ALL, tokenService.getToken());
		/**
		 * {
			   "filter":{
			      "is_to_all":false,
			      "tag_id":tag_id
			   },
			   "wxcard":{
			      "card_id":"card_id"
			   },
			    "msgtype":"wxcard",
			    "send_ignore_reprint":0
			}
		 */
		
		
		Map<String,Object> params=new HashMap<>();
		params.put("msgtype", "wxcard");
		params.put("send_ignore_reprint", 0);
		Map<String,Object> filter=new HashMap<>();
		filter.put("is_to_all",tagId==0?true:false);
		if(tagId!=0){
			filter.put("tag_id", tagId);
		}
		params.put("filter", filter);
		Map<String,Object> wxcard=new HashMap<>();
		wxcard.put("card_id", cardId);
		params.put("wxcard", wxcard);
		Map<String,Object> result=HttpRequestUtil.sendPostJson(url, params);
		return result;
	}

	@Override
	public Map<String, Object> sendMpNewsByOpenids(String  mediaId, List<String> openids) { 
		/**
		 * {
			   "touser":[
			    "OPENID1",
			    "OPENID2"
			   ],
			   "mpnews":{
			      "media_id":"123dsdajkasd231jhksad"
			   },
			    "msgtype":"mpnews"，
			    "send_ignore_reprint":0
			}
		 */
		
 		String url=String.format(ApiUrls.MSG_SEND, tokenService.getToken()); 
		Map<String,Object> params=new HashMap<>();
		params.put("msgtype", "mpnews");
		params.put("send_ignore_reprint", 0); 
		params.put("touser", openids.size()>1?openids:openids.get(0)); 
		Map<String,Object> mpnews=new HashMap<>();
		mpnews.put("media_id", mediaId);
		params.put("mpnews", mpnews);
		Map<String,Object> result=HttpRequestUtil.sendPostJson(url, params);
		return result;
	}

	@Override
	public Map<String, Object> sendTextByOpenids(String  content, List<String> openids) {
		/**
		 * {
			   "touser":[
			    "OPENID1",
			    "OPENID2"
			   ],
			   "text":{
			      "content":"123dsdajkasd231jhksad"
			   },
			    "msgtype":"text"，
			    "send_ignore_reprint":0
			}
		 */
		
 		String url=String.format(ApiUrls.MSG_SEND, tokenService.getToken()); 
		Map<String,Object> params=new HashMap<>();
		params.put("msgtype", "text");
		params.put("send_ignore_reprint", 0); 
		params.put("touser", openids.size()>1?openids:openids.get(0));  
		Map<String,Object> text=new HashMap<>();
		text.put("content", content);
		params.put("text", text);
		Map<String,Object> result=HttpRequestUtil.sendPostJson(url, params);
		return result;
	}

	@Override
	public Map<String, Object> sendVoiceByOpenids(String  mediaId, List<String> openids) {
		/**
		 * {
			   "touser":[
			    "OPENID1",
			    "OPENID2"
			   ],
			   "voice":{
			      "media_id":"123dsdajkasd231jhksad"
			   },
			    "msgtype":"voice"，
			    "send_ignore_reprint":0
			}
		 */
		
 		String url=String.format(ApiUrls.MSG_SEND, tokenService.getToken()); 
		Map<String,Object> params=new HashMap<>();
		params.put("msgtype", "voice");
		params.put("send_ignore_reprint", 0); 
		params.put("touser", openids.size()>1?openids:openids.get(0)); 
		Map<String,Object> voice=new HashMap<>();
		voice.put("media_id", mediaId);
		params.put("voice", voice);
		Map<String,Object> result=HttpRequestUtil.sendPostJson(url, params);
		return result;
	}

	@Override
	public Map<String, Object> sendImageByOpenids(  List<String> mediaIds,String recommend, List<String> openids) {
		/**
		 * {
			   "touser":[
			    "OPENID1",
			    "OPENID2"
			   ],
			   "images": {
			      "media_ids": [
			         "aaa",
			         "bbb",
			         "ccc"
			      ],
			      "recommend": "xxx",
			      "need_open_comment": 1,
			      "only_fans_can_comment": 0
			   },
			    "msgtype":"voice"，
			    "send_ignore_reprint":0
			}
		 */
		
 		String url=String.format(ApiUrls.MSG_SEND, tokenService.getToken()); 
		Map<String,Object> params=new HashMap<>();
		params.put("msgtype", "voice");
		params.put("send_ignore_reprint", 0); 
		params.put("touser", openids.size()>1?openids:openids.get(0)); 
		Map<String,Object> images=new HashMap<>();
		images.put("media_ids", mediaIds);
		images.put("recommend", recommend); 
		images.put("need_open_comment", 1); 
		images.put("only_fans_can_comment", 0);
		params.put("images", images);
		Map<String,Object> result=HttpRequestUtil.sendPostJson(url, params);
		return result;
	}

	@Override
	public Map<String, Object> sendMpVideoByOpenids(String  mediaId,String title,String description, List<String> openids) {
		/**
		 * {
			   "touser":[
			    "OPENID1",
			    "OPENID2"
			   ],
			   "mpvideo":{
			      "media_id":"123dsdajkasd231jhksad"
			      "title":"TITLE",
			      "description":"DESCRIPTION"
			   },
			    "msgtype":"mpvideo"，
			    "send_ignore_reprint":0
			}
		 */
		
 		String url=String.format(ApiUrls.MSG_SEND, tokenService.getToken()); 
		Map<String,Object> params=new HashMap<>();
		params.put("msgtype", "mpvideo");
		params.put("send_ignore_reprint", 0); 
		params.put("touser", openids.size()>1?openids:openids.get(0)); 
		Map<String,Object> mpvideo=new HashMap<>();
		mpvideo.put("media_id", mediaId); 
		mpvideo.put("title", title); 
		mpvideo.put("description", description);
		params.put("mpvideo", mpvideo);
		Map<String,Object> result=HttpRequestUtil.sendPostJson(url, params);
		return result;
	}

	@Override
	public Map<String, Object> sendWxCardByOpenids(String  cardId, List<String> openids) {
		/**
		 * {
			   "touser":[
			    "OPENID1",
			    "OPENID2"
			   ],
			   "wxcard":{
			      "card_id":"123dsdajkasd231jhksad"
			   },
			    "msgtype":"wxcard"，
			    "send_ignore_reprint":0
			}
		 */
		
 		String url=String.format(ApiUrls.MSG_SEND, tokenService.getToken()); 
		Map<String,Object> params=new HashMap<>();
		params.put("msgtype", "wxcard");
		params.put("send_ignore_reprint", 0); 
		params.put("touser", openids.size()>1?openids:openids.get(0)); 
		Map<String,Object> wxcard=new HashMap<>();
		wxcard.put("card_id", cardId);
		params.put("wxcard", wxcard);
		Map<String,Object> result=HttpRequestUtil.sendPostJson(url, params);
		return result;
	}

	@Override
	public Map<String, Object> sendMpNewsToAll( String mediaId) {
		// TODO Auto-generated method stub
		return this.sendMpNewsByTag( mediaId, 0);
	}

	@Override
	public Map<String, Object> sendTextToAll( String content) {
		// TODO Auto-generated method stub
		return this.sendTextByTag( content, 0);
	}

	@Override
	public Map<String, Object> sendVoicToAll( String mediaId) {
		// TODO Auto-generated method stub
		return this.sendVoiceByTag( mediaId, 0);
	}

	@Override
	public Map<String, Object> sendMpVideoToAll( String mediaId) {
		// TODO Auto-generated method stub
		return sendMpVideoByTag( mediaId, 0);
	}

	@Override
	public Map<String, Object> sendWxCardToAll( String cardId) {
		// TODO Auto-generated method stub
		return sendWxCardByTag(cardId, 0);
	}

	@Override
	public Map<String, Object> sendMpNewsByOpenid( String mediaId, String openid) {
		// TODO Auto-generated method stub
		List<String> openids=new ArrayList<>();
		openids.add(openid);
		return sendMpNewsByOpenids( mediaId, openids);
	}

	@Override
	public Map<String, Object> sendTextByOpenid( String content, String openid) {
		List<String> openids=new ArrayList<>();
		openids.add(openid);
		return sendTextByOpenids( content, openids);
	}

	@Override
	public Map<String, Object> sendVoiceByOpenid( String mediaId, String openid) {
		List<String> openids=new ArrayList<>();
		openids.add(openid);
		return sendVoiceByOpenids(  mediaId, openids);
	}

	@Override
	public Map<String, Object> sendMpVideoByOpenid( String mediaId, String title, String description,
			String openid) {
		List<String> openids=new ArrayList<>();
		openids.add(openid);
		return sendMpVideoByOpenids(mediaId, title,description,openids);
	}

	@Override
	public Map<String, Object> sendWxCardByOpenid( String cardId, String openid) {
		List<String> openids=new ArrayList<>();
		openids.add(openid);
		return sendWxCardByOpenids( cardId, openids);
	}

	@Override
	public Map<String, Object> sendImageToAll( List<String> mediaIds, String recommend) {
		// TODO Auto-generated method stub
		return sendImageByTag( mediaIds, recommend, 0);
	}

	@Override
	public Map<String, Object> sendImageByOpenid( List<String> mediaIds, String recommend,
			String openid) {
		List<String> openids=new ArrayList<>();
		openids.add(openid);
		return sendImageByOpenids( mediaIds,recommend, openids);
	}

	@Override
	public Map<String, Object> sendTemplateMessage( String templateId, String openid, String url, Map<String, Object> data) { 
		 
 		String url2=String.format(ApiUrls.MSG_SEND_TEMPLATE, tokenService.getToken()); 
		Map<String,Object> params=new HashMap<>(); 
		params.put("touser", openid);
		params.put("template_id", templateId); 
		params.put("url", url);
		params.put("data", data);
		Map<String,Object> result=HttpRequestUtil.sendPostJson(url2, params);
		return result;
	}

	@Override
	public Map<String, Object> getAllTemplate( ) {
 		String url=String.format(ApiUrls.MSG_TEMPLATE_GET_ALL, tokenService.getToken());  
		Map<String,Object> result=HttpRequestUtil.sendGetJson(url, null);
		return result;
	}
 

}
