package com.mdp.tpa.wechat.config;

import lombok.Data;

@Data
public class AppProperties {
    /**
     * 应用appid
     */
    String appid;

    /**
     *   加密串
     */
    String appSecret;

    /**
     * APP_TOKEN
     */
    String appToken;

    /**
     *   秘钥EncodingAESKey(消息加密密钥)
     */
    String encKey;



    /**
     * 是否允许自动加载accessToken，jsticket,apiticket等,默认为true
     */
    boolean enabledLoadAccessToken=true;
}
