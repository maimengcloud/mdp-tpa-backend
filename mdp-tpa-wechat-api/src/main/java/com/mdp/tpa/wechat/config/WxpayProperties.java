package com.mdp.tpa.wechat.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

import java.util.Properties;

@Data
@ConfigurationProperties(prefix = "mdp.apps.wxpay2")
public class WxpayProperties extends AppProperties{
	/**
	 * 支付商户
	 */
	String mchId;

	/**
	 * 支付密钥
	 */
	String payKey;

	/**
	 * 支付通知地址
	 */
	String notifyUrl;
}