package com.mdp.tpa.wechat.service.cache.redis;

import com.mdp.tpa.wechat.api.cache.Oauth2CodeCache;
import com.mdp.tpa.wechat.entity.Oauth2AccessToken;
import org.springframework.stereotype.Service;

@Service
public class RedisOauth2CodeCacheService extends DefaultRedisCacheService<Oauth2AccessToken> implements Oauth2CodeCache {
	@Override
	public String getCacheKey() {
		// TODO Auto-generated method stub
		return "mdp_wxapi_Oauth2Code";
	}
}
