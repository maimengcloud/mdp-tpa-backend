package com.mdp.tpa.wechat.service;

import java.util.Map;

import javax.annotation.PostConstruct;

import com.mdp.tpa.wechat.HttpRequestUtil;
import com.mdp.tpa.wechat.api.AccessTokenService;
import com.mdp.tpa.wechat.api.ApiUrls;
import com.mdp.tpa.wechat.api.MenuService;
import com.mdp.tpa.wechat.util.WxResultParseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonParser.Feature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mdp.core.entity.Tips;

@Service
public class MenuServiceImpl implements MenuService {

	Logger log=LoggerFactory.getLogger(MenuServiceImpl.class);
	
	ObjectMapper objectMapper=new ObjectMapper();
	@Autowired
	AccessTokenService tokenService;
 
	@PostConstruct
	public void initObjectMapper(){
		objectMapper.setSerializationInclusion(com.fasterxml.jackson.annotation.JsonInclude.Include.ALWAYS);
		objectMapper.configure(Feature.ALLOW_UNQUOTED_FIELD_NAMES, true);
		objectMapper.configure(Feature.ALLOW_UNQUOTED_CONTROL_CHARS, true); 
		
	}  
	@Override
	public Tips deleteMenu( ) {
 		Tips tips=new Tips("创建成功");
		try {
 			String url=String.format(ApiUrls.MENU_DELETE, tokenService.getToken());
			Map<String,Object> data= HttpRequestUtil.sendPostJson(url,null);
			tips= WxResultParseUtil.mapToTips(data);
		} catch (Exception e) {
			log.error("",e); 
			tips.setErrMsg(e.getMessage()); 
			return tips;
		}
 		return tips;
	} 

	
	@Override
	public Tips createMenu(  String menusJson) {

		Tips tips=new Tips("创建成功");
		try {
			String url=String.format(ApiUrls.MENU_CREATE, tokenService.getToken()); 
			Map<String,Object> data=HttpRequestUtil.sendPostJson(url, menusJson);
			tips=WxResultParseUtil.mapToTips(data);
		} catch (Exception e) {
			log.error("",e); 
			tips.setErrMsg(e.getMessage()); 
			return tips;
		}
 		return tips;
	}
	

}
