package com.mdp.tpa.wechat.api;

import java.util.List;
import java.util.Map;

import com.alibaba.fastjson.JSONObject;
import com.mdp.core.entity.Tips;

public interface MenuService {
	/** 
	 * 删除菜单</p>
 	 * http请求方式：GET https://api.weixin.qq.com/cgi-bin/menu/delete?access_token=ACCESS_TOKEN</p> 
	 * @return
	 * 正确的Json返回结果:</p>
		{"errcode":0,"errmsg":"ok"}</p>
	 */
	public Tips deleteMenu( );
	
	/** 
	 * 创建菜单</p>
	 * 获取二维码ticket后，开发者可用ticket换取二维码图片。请注意，本接口无须登录态即可调用。</p>
	 * http请求方式：POST（请使用https协议）</p>
	 * URL:  https://api.weixin.qq.com/cgi-bin/menu/create?access_token=ACCESS_TOKEN</p> 
	 * @return
	 * 正确的Json返回结果:</p>
		{"errcode":0,"errmsg":"ok"}</p>
	 */
	public Tips createMenu(  String menusJson);
	
	 
	
}
