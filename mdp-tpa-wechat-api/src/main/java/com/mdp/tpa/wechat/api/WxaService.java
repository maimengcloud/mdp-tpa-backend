package com.mdp.tpa.wechat.api;

import java.util.Map;

public interface WxaService {
	/**
	 * 微信小程序登录通过客户端上送的code获取openid
	 * @param appid
	 * @param appSecret
	 * @param code
	 * @return {openid=oBi4O0dwNZOYJ5HP4REmTnhL05D0, session_key=PtSra+OdMH0/67OtuFK0jg==, expires_in=7200}
	 */
	public Map<String,Object> jscode2session(String appid,String appSecret,String code);

	/**
	 * 通过该接口生成的小程序码，永久有效，数量暂无限制。用户扫描该码进入小程序后，开发者需在对应页面获取的码中 scene 字段的值，再做处理逻辑。使用如下代码可以获取到二维码中的 scene 字段的值。调试阶段可以使用开发工具的条件编译自定义参数 scene=xxxx 进行模拟，开发工具模拟时的 scene 的参数值需要进行 urlencode</p>
	 * 创建微信小程序码图片链接 （非二维码）</p>
	 * https://api.weixin.qq.com/wxa/getwxacodeunlimit?access_token=ACCESS_TOKEN
	 * @param scene 最大32个可见字符，只支持数字，大小写英文以及部分特殊字符：!#$&'()*+,/:;=?@-._~，其它字符请自行编码为合法字符（因不支持%，中文无法使用 urlencode 处理，请使用其他编码方式）
	 * @param page 必须是已经发布的小程序页面，例如 "pages/index/index" ,根路径前不要填加'/',不能携带参数（参数请放在scene字段里），如果不填写这个字段，默认跳主页面
	 * @param width 二维码的宽度 默认430 
	 * @return
	 * 
	 */
	 public Map<String,Object> getWxacodeUnlimitAutoColor( String scene,String page,int width);
	 
		/**
		 * 通过该接口生成的小程序码，永久有效，数量暂无限制。用户扫描该码进入小程序后，开发者需在对应页面获取的码中 scene 字段的值，再做处理逻辑。使用如下代码可以获取到二维码中的 scene 字段的值。调试阶段可以使用开发工具的条件编译自定义参数 scene=xxxx 进行模拟，开发工具模拟时的 scene 的参数值需要进行 urlencode</p>
		 * 创建微信小程序码图片链接 （非二维码）</p>
		 * https://api.weixin.qq.com/wxa/getwxacodeunlimit?access_token=ACCESS_TOKEN
		 * @param scene 最大32个可见字符，只支持数字，大小写英文以及部分特殊字符：!#$&'()*+,/:;=?@-._~，其它字符请自行编码为合法字符（因不支持%，中文无法使用 urlencode 处理，请使用其他编码方式）
		 * @param page 必须是已经发布的小程序页面，例如 "pages/index/index" ,根路径前不要填加'/',不能携带参数（参数请放在scene字段里），如果不填写这个字段，默认跳主页面
		 * @param width 二维码的宽度 默认430 
		 * @param r 颜色rgb中的r
		 * @param g 颜色rgb中的g
		 * @param b 颜色rgb中的b
		 * @return
		 * 
		 */
		 public Map<String,Object> getWxacodeUnlimitUnAutoColor( String scene,String page,int width,int r,int g,int b);
}
