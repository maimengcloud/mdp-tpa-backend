package com.mdp.tpa.wechat.config;

import com.mdp.tpa.wechat.wxpaysdk.MyWxPayConfig;
import com.mdp.tpa.wechat.wxpaysdk.WXPayConfig;
import lombok.Data;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Data
@Configuration
@EnableConfigurationProperties({WxpubProperties.class, WxpayProperties.class, WxminiappProperties.class,WxopenProperties.class})
public class WxapiAutoConfiguration {

	Logger logger= LoggerFactory.getLogger(WxapiAutoConfiguration.class);

	@Autowired
	WxpubProperties wxpubProperties;

	@Autowired
	WxopenProperties wxopenProperties;

	@Autowired
	WxpayProperties wxpayProperties;

	@Autowired
	WxminiappProperties wxminiappProperties;

	@Bean
	public WXPayConfig initWxPayConfig(){

		try {
			WXPayConfig config=new MyWxPayConfig(wxpayProperties.getAppid(), wxpayProperties.getMchId() , wxpayProperties.getPayKey());
			return config;
		} catch (Exception e) {
			logger.error("初始化微信支付配置文件出错，微信支付功能将无法使用",e);
			return null;
		}


	}
	
}
