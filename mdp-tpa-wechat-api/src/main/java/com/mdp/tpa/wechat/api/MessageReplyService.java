package com.mdp.tpa.wechat.api;

import java.util.List;
import java.util.Map;

public interface MessageReplyService {
	
	/**
	 * 获取回复用户在文本消息体
	 * @param toUserName 接收方帐号（收到的OpenID）
	 * @param fromUserName 开发者微信号
	 * @param createTime 消息创建时间 （整型）
	 * @param content 回复的消息内容（换行：在content中能够换行，微信客户端就支持换行显示） msgType=text
	 * @return
	 * 
		<xml>
		<ToUserName><![CDATA[toUser]]></ToUserName>
		<FromUserName><![CDATA[fromUser]]></FromUserName>
		<CreateTime>12345678</CreateTime>
		<MsgType><![CDATA[text]]></MsgType>
		<Content><![CDATA[你好]]></Content>
		</xml>
	 */
	public String text(  String toUserName,String fromUserName,Integer createTime,String content);
	 
	/**
	 * 获取回复用户在文本消息体
	 * @param toUserName 接收方帐号（收到的OpenID）
	 * @param fromUserName 开发者微信号
	 * @param createTime 消息创建时间 （整型）
	 * @param mediaId 回复的素材编号， msgType=image
	 * @return
	 *  
		<xml>
			<ToUserName><![CDATA[toUser]]></ToUserName>
			<FromUserName><![CDATA[fromUser]]></FromUserName>
			<CreateTime>12345678</CreateTime>
			<MsgType><![CDATA[image]]></MsgType>
			<Image>
				<MediaId><![CDATA[media_id]]></MediaId>
			</Image>
		</xml>
	 */
	public String image(  String toUserName,String fromUserName,Integer createTime,String mediaId);
	
	/**
	 * 获取回复用户 声音消息体
	 * @param toUserName 接收方帐号（收到的OpenID）
	 * @param fromUserName 开发者微信号
	 * @param createTime 消息创建时间 （整型）
	 * @param mediaId 回复的素材编号， msgType=voice
	 * @return
	 * 

		<xml>
			<ToUserName><![CDATA[toUser]]></ToUserName>
			<FromUserName><![CDATA[fromUser]]></FromUserName>
			<CreateTime>12345678</CreateTime>
			<MsgType><![CDATA[voice]]></MsgType>
			<Voice>
				<MediaId><![CDATA[media_id]]></MediaId>
			</Voice>
		</xm>
	 */
	public String voice(  String toUserName,String fromUserName,Integer createTime,String mediaId);
	
	/**
	 * 获取回复用户 视频消息体
	 * @param toUserName 接收方帐号（收到的OpenID）
	 * @param fromUserName 开发者微信号
	 * @param createTime 消息创建时间 （整型）
	 * @param mediaId 回复的素材编号， msgType=video
	 * @param title 标题
	 * @param description 描述
	 * @return
	 * 
		
		<xml>
			<ToUserName><![CDATA[toUser]]></ToUserName>
			<FromUserName><![CDATA[fromUser]]></FromUserName>
			<CreateTime>12345678</CreateTime>
			<MsgType><![CDATA[video]]></MsgType>
			<Video>
				<MediaId><![CDATA[media_id]]></MediaId>
				<Title><![CDATA[title]]></Title>
				<Description><![CDATA[description]]></Description>
			</Video> 
		</xml>
	 */
	public String video(  String toUserName,String fromUserName,Integer createTime,String mediaId,String title,String description);
	
	
	/**
	 * 获取回复用户 音乐消息体
	 * @param toUserName 接收方帐号（收到的OpenID）
	 * @param fromUserName 开发者微信号
	 * @param createTime 消息创建时间 （整型）
	 * @param mediaId 回复的素材编号， msgType=music
	 * @param title
	 * @param description
	 * @param musicUrl
	 * @param hqMusicUrl
	 * @param thumbMediaId
	 * @return
	 * 
		<xml>
		<ToUserName><![CDATA[toUser]]></ToUserName>
		<FromUserName><![CDATA[fromUser]]></FromUserName>
		<CreateTime>12345678</CreateTime>
		<MsgType><![CDATA[music]]></MsgType>
		<Music>
			<Title><![CDATA[TITLE]]></Title>
			<Description><![CDATA[DESCRIPTION]]></Description>
			<MusicUrl><![CDATA[MUSIC_Url]]></MusicUrl>
			<HQMusicUrl><![CDATA[HQ_MUSIC_Url]]></HQMusicUrl>
			<ThumbMediaId><![CDATA[media_id]]></ThumbMediaId>
		</Music>
		</xml>
	 */
	public String music(  String toUserName,String fromUserName,Integer createTime,String mediaId,String title,String description,String musicUrl,String hqMusicUrl,String thumbMediaId);
	
	
	/**
	 * 获取回复用户 声音消息体
	 * @param toUserName 接收方帐号（收到的OpenID）
	 * @param fromUserName 开发者微信号
	 * @param createTime 消息创建时间 （整型） msgType=news
	 * @return
	 *  
	<xml>
		<ToUserName><![CDATA[toUser]]></ToUserName>
		<FromUserName><![CDATA[fromUser]]></FromUserName>
		<CreateTime>12345678</CreateTime>
		<MsgType><![CDATA[news]]></MsgType>
		<ArticleCount>2</ArticleCount>
		<Articles>
			<item>
				<Title><![CDATA[title1]]></Title> 
				<Description><![CDATA[description1]]></Description>
				<PicUrl><![CDATA[picurl]]></PicUrl>
				<Url><![CDATA[url]]></Url>
			</item>
			<item>
				<Title><![CDATA[title]]></Title>
				<Description><![CDATA[description]]></Description>
				<PicUrl><![CDATA[picurl]]></PicUrl>
				<Url><![CDATA[url]]></Url>
			</item>
		</Articles>
	</xml>
	 */
	public String news(  String toUserName,String fromUserName,Integer createTime,List<Map<String,Object>> items);
	
 
}
