package com.mdp.tpa.wechat.service;

import static com.mdp.core.utils.BaseUtils.map;

import java.util.Date;
import java.util.Map;

import com.mdp.core.api.CacheHKVService;
import com.mdp.tpa.wechat.HttpRequestUtil;
import com.mdp.tpa.wechat.api.AccessTokenService;
import com.mdp.tpa.wechat.api.ApiUrls;
import com.mdp.tpa.wechat.api.WxaService;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mdp.core.service.DefaultCacheService;

@Service
public class WxaServiceImpl implements WxaService {

	@Autowired
	CacheHKVService cacheHKVService;

	Log log=LogFactory.getLog(AccessTokenServiceImpl.class);

	@Autowired
	AccessTokenService tokenService;
 
	
	@Override
	public Map<String, Object> jscode2session(String appid,String appSecret,String code) {
		// TODO Auto-generated method stub
			String url=String.format(ApiUrls.WXA_JSCODE2SESSION, appid,appSecret,code);
			Map<String,Object> result= HttpRequestUtil.sendGetJson(url,null);
			if(result.containsKey("openid")){
				result.put("createDate", new Date());
				String sessionKeyNew=(String) result.get("session_key");
				cacheHKVService.put(sessionKeyNew,result);
				return result;
			}else{
				return result;
			}
			
	}

	@Override
	public Map<String, Object> getWxacodeUnlimitAutoColor( String scene, String page, int width) {
 		String url=String.format(ApiUrls.WXA_GETWXACODEUNLIMIT, tokenService.getToken());
		Map<String,Object> params=map("scene",scene,"page",page,"width",width); 
		Map<String,Object> result=HttpRequestUtil.sendGetJson(url,params); 	 
		return result;
	}

	@Override
	public Map<String, Object> getWxacodeUnlimitUnAutoColor(String scene, String page, int width, int r, int g,
			int b) {
 		String url=String.format(ApiUrls.WXA_GETWXACODEUNLIMIT, tokenService.getToken());
		Map<String,Object> params=map("scene",scene,"page",page,"width",width,"r",r,"g",g,"b",b,"tokenService.getToken()",tokenService.getToken()); 
		Map<String,Object> result=HttpRequestUtil.sendGetJson(url,params); 	 
		return result; 
	}  
	 

}
