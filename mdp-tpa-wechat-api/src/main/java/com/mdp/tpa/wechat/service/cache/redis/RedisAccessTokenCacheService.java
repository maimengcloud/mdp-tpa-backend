package com.mdp.tpa.wechat.service.cache.redis;

import java.util.concurrent.TimeUnit;

import com.mdp.tpa.wechat.api.cache.AccessTokenCache;
import com.mdp.tpa.wechat.entity.AccessToken;
import org.springframework.stereotype.Service;

@Service
public class RedisAccessTokenCacheService extends DefaultRedisCacheService<AccessToken> implements AccessTokenCache {

	@Override
	public String getCacheKey() {
		// TODO Auto-generated method stub
		return "mdp_wxapi_AccessToken";
	}
	
	 
	public   void put(String key, AccessToken o) {
		String okey=this.getCacheKey()+":"+key;
		cacheHKVService.put(okey, key, o);
		cacheHKVService.expire(okey, o.getExpiresIn(), TimeUnit.SECONDS);
	}

	@Override
	public AccessToken get(String key) {
		String okey=this.getCacheKey()+":"+key;
		return  (AccessToken) cacheHKVService.get(okey, key);
	}

	@Override
	public boolean containsValue(AccessToken value) {
		return false;
	}


}
