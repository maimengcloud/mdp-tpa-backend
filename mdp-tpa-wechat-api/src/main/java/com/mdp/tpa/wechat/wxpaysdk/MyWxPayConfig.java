package com.mdp.tpa.wechat.wxpaysdk;

import lombok.Data;

import java.io.ByteArrayInputStream;
import java.io.InputStream;

/**
 * @author Jackie
 * @Title:
 * @Package
 * @Description: 微信需要实现自己应用的配置参数
 * @date 2018/3/2817:29
 */
@Data
public class MyWxPayConfig implements WXPayConfig {

	private byte[] certData;// 证书
	private String appId;
	private String mchId;
	private String key;
	private String notifyUrl;

	
	public MyWxPayConfig(String appId,String mchId,String key) throws Exception {// 获取证书
		// String certPath = "/path/to/apiclient_cert.p12";//证书路径
		// String certPath =
		// this.getClass().getClassLoader().getResource("apiclient_cert.p12").getPath();
		// File file = new File(certPath);
		// InputStream certStream = new FileInputStream(file);
		this.appId=appId; 
		this.mchId=mchId;
		this.key=key;
		InputStream certStream = this.getClass().getResourceAsStream("/certs/"+mchId+"/apiclient_cert.p12");
		this.certData = new byte[(int) certStream.available()];
		certStream.read(this.certData);
		certStream.close();
	}

	@Override
	public String getAppId() {
		return appId;
		//return "wxdc4792ea8b97cd10";
	}
 

	@Override
	public String getKey() {
		return key;
		//return "ffffffffgfhfgjhdfgsdfgsghdshgdfs";
	}

	@Override
	public InputStream getCertStream() {
		ByteArrayInputStream certBis;
		certBis = new ByteArrayInputStream(this.certData);
		return certBis;
	}

	@Override
	public int getHttpConnectTimeoutMs() {
		return 8000;
	}

	@Override
	public int getHttpReadTimeoutMs() {
		return 10000;
	}

	public byte[] getCertData() {
		return certData;
	}

	public void setCertData(byte[] certData) {
		this.certData = certData;
	}
 

	public void setAppId(String appId) {
		this.appId = appId;
	}

	public String getMchId() {
		return mchId;
	}

	public void setMchId(String mchId) {
		this.mchId = mchId;
	}

	public void setKey(String key) {
		this.key = key;
	}
  
	
}
