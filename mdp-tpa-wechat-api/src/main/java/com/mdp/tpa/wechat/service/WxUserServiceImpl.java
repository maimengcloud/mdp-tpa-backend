package com.mdp.tpa.wechat.service;

import java.util.List;
import java.util.Map;

import com.mdp.tpa.wechat.HttpRequestUtil;
import com.mdp.tpa.wechat.api.AccessTokenService;
import com.mdp.tpa.wechat.api.ApiUrls;
import com.mdp.tpa.wechat.api.WxUserService;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mdp.core.err.BizException;

@Service
public class WxUserServiceImpl implements WxUserService {

	Log log=LogFactory.getLog(WxUserServiceImpl.class);

	@Autowired
	AccessTokenService tokenService;
	
	@Override
	public Map<String, Object> getSnsUserinfo( String openid) {
 		Map<String,Object> map= HttpRequestUtil.sendGetJson(String.format(ApiUrls.SNS_USER_INFO, tokenService.getToken(),openid), null);
		if(map.containsKey("errcode")){
			throw new BizException(map.get("errcode")+"", (String)map.get("errmsg"));
		}
 		return map;
	}

	@Override
	public Map<String, Object> getUserinfo(String openid) { 
 		Map<String,Object> map= HttpRequestUtil.sendGetJson(String.format(ApiUrls.USER_INFO, tokenService.getToken(),openid), null);
		if(map.containsKey("errcode")){
			throw new BizException(map.get("errcode")+"", (String)map.get("errmsg"));
		}
 		return map;
	}

	@Override
	public Map<String, Object> batchgetUserInfo(List<String> openids) { 
 	 
 		return null;
	}

	@Override
	public Map<String, Object> getUserOpenidList(String nextOpenid) { 
 		Map<String,Object> map=  HttpRequestUtil.sendGetJson(String.format(ApiUrls.USER_GET, tokenService.getToken(),nextOpenid), null);
		if(map.containsKey("errcode")){
			throw new BizException(map.get("errcode")+"", (String)map.get("errmsg"));
		}
 		return map;
	}

}
