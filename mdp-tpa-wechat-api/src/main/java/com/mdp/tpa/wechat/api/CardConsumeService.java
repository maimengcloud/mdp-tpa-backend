package com.mdp.tpa.wechat.api;

import java.util.Map;

import com.mdp.core.entity.Tips;
/**
 * 微信卡券核销相关接口
 * 由第三方主动发起
 * @author Administrator
 *
 */
public interface CardConsumeService {
	/**
	 * 1.1
	 * 查询Code接口<P/>
	 * 查询code接口可以查询当前code是否可以被核销并检查code状态。当前可以被定位的状态为正常、已核销、转赠中、已删除、已失效和无效code。<P/>
	 * HTTP请求方式: POST<P/>
	 * URL:https://api.weixin.qq.com/card/code/get?access_token=TOKEN<P/>
	 * 卡券正常状态返回：<P/>
	 * {<P/>
		  "errcode": 0,<P/>
		  "errmsg": "ok",<P/>
		  "card": {<P/>
		    "card_id": "pbLatjk4T4Hx-QFQGL4zGQy27_Qg",<P/>
		    "begin_time": 1457452800,<P/>
		    "end_time": 1463155199<P/>
		  },<P/>
		  "openid": "obLatjm43RA5C6QfMO5szKYnT3dM",<P/>
		  "can_consume": true,<P/>
		  "outer_str": "12b",<P/>
		  "user_card_status": "NORMAL"<P/>
		} <P/>
	    卡券异常状态返回：<P/>
	    {<P/>
		 "errcode": 40127,<P/>
		 "errmsg": "invalid user-card status! Hint: the card was given to user, but may be deleted or set unavailable ! hint: [iHBD40040ent3]"<P/>
		}<P/>
	@param cardId 卡券ID代表一类卡券。自定义code卡券必填。
	@param code 单张卡券的唯一标准。
	@param checkConsume 是否校验code核销状态，填入true和false时的code异常状态返回数据不同。
	 */
	public Map<String,Object> codeGet( String cardId,String code,boolean checkConsume);
	

	/**
	 * 设置卡券失效接口<P/>
	 * 为满足改票、退款等异常情况，可调用卡券失效接口将用户的卡券设置为失效状态。 <P/>
	 * 
	 * HTTP请求方式: POST <P/>
	 * URL:https://api.weixin.qq.com/card/code/unavailable?access_token=TOKEN<P/>
	 * 1.设置卡券失效的操作不可逆，即无法将设置为失效的卡券调回有效状态，商家须慎重调用该接口。<P/>
	 * 2.商户调用失效接口前须与顾客事先告知并取得同意，否则因此带来的顾客投诉，微信将会按照《微信运营处罚规则》进行处罚。<P/>
	 * 返回：<P/>
		{<P/>
		"errcode":0,<P/>
		"errmsg":"ok",<P/>
		}<P/>
     * @param cardId 
     * @param code
     * @param reason
	 */
	public Tips codeUnavailable( String cardId,String code,String reason);
	
	/**
	1.2 核销Code接口
	1.仅支持核销有效状态的卡券，若卡券处于异常状态，均不可核销。（异常状态包括：卡券删除、未生效、过期、转赠中、转赠退回、失效）
	我们强烈建议开发者在调用核销code接口之前调用查询code接口，并在核销之前对非法状态的code(如转赠中、已删除、已核销等)做出处理。
	消耗code接口是核销卡券的唯一接口,开发者可以调用当前接口将用户的优惠券进行核销，该过程不可逆。
	HTTP请求方式: POST
	URL:https://api.weixin.qq.com/card/code/consume?access_token=TOKEN
		非自定义Code卡券的请求
		{
		  "code": "12312313"
		}
		或自定义Code卡券的请求
		{
		  "code": "12312313",
		  "card_id": "pFS7Fjg8kV1IdDz01r4SQwMkuCKc"
		}
		
		返回：
		 {
			"errcode":0,
			"errmsg":"ok",
			"card":{"card_id":"pFS7Fjg8kV1IdDz01r4SQwMkuCKc"},
			"openid":"oFS7Fjl0WsZ9AMZqrI80nbIq8xrA"
			}
		*/
	public Map<String,Object> codeConsume( String code);
}
