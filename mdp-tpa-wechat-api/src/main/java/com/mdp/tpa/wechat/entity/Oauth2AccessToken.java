package com.mdp.tpa.wechat.entity;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;
@Data
public class Oauth2AccessToken implements Serializable {


	private static final long serialVersionUID = -3667448281506642177L;
	
	/**
	 * 网页授权接口调用凭证,注意：此access_token与基础支持的access_token不同
	 */
	String accessToken;
	/**
	 * access_token接口调用凭证超时时间，单位（秒）7200秒=120分=2小时
	 */
	Integer expiresIn;
	
	/**
	 * 用户刷新access_token
	 */
	String refreshToken;
	
	/**
	 * 用户唯一标识，请注意，在未关注公众号时，用户访问公众号的网页，也会产生一个用户和公众号唯一的OpenID
	 */
	String openid;
	
	/**
	 * 用户授权的作用域，使用逗号（,）分隔
	 */ 
	String scope;
	
	/**
	 * 
	 * 第一次创建日期
	 */
	Date createDate;
	
	/**
	 * 刷新时间
	 */
	Date refreshDate;
	
	String unionid;
	
	/**
	 * refreshToken过期时间 30天
	 */
	Integer refreshTokenOutTimes;

	
	
}
