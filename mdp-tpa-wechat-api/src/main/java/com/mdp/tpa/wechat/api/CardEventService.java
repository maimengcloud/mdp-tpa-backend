package com.mdp.tpa.wechat.api;

import java.util.Map;

/**
 * 微信卡券相关的推送事件处理
 * 由微信主动推送到第三方系统
 */
public interface CardEventService {
	
	/**
	 * 卡券api推送事件入口
	 * @param event
	 * @param msgType
	 * @param fromUser
	 * @param toUser
	 * @param createTime
	 * @param info
	 */
	public void doEvent( String event,String msgType,String fromUser,String toUser,String createTime,Map<String,Object> info);
	
	
	/**
	2.1 审核事件推送
	生成的卡券通过审核时，微信会把这个事件推送到开发者填写的URL。
	<xml> 
	  <ToUserName><![CDATA[toUser]]></ToUserName>  
	  <FromUserName><![CDATA[FromUser]]></FromUserName>  
	  <CreateTime>123456789</CreateTime>  
	  <MsgType><![CDATA[event]]></MsgType>  
	  <Event><![CDATA[card_pass_check]]></Event> //不通过为card_not_pass_check 
	  <CardId><![CDATA[cardid]]></CardId>  
	  <RefuseReason><![CDATA[非法代制]]></RefuseReason> 
	</xml>
	**/
	public void cardPassCheck( String event,String msgType,String fromUser,String toUser,String createTime,Map<String,Object> info);
	/**
	2.2 领取事件推送
	用户在领取卡券时，微信会把这个事件推送到开发者填写的URL。 推送XML数据包示例：
	<xml> 
	  <ToUserName> <![CDATA[gh_fc0a06a20993]]> </ToUserName>  
	  <FromUserName> <![CDATA[oZI8Fj040-be6rlDohc6gkoPOQTQ]]> </FromUserName>  
	  <CreateTime>1472551036</CreateTime>  
	  <MsgType> <![CDATA[event]]> </MsgType>  
	  <Event> <![CDATA[user_get_card]]> </Event>  
	  <CardId> <![CDATA[pZI8Fjwsy5fVPRBeD78J4RmqVvBc]]> </CardId>  
	  <IsGiveByFriend>0</IsGiveByFriend>  
	  <UserCardCode> <![CDATA[226009850808]]> </UserCardCode>  
	  <FriendUserName> <![CDATA[]]> </FriendUserName>  
	  <OuterId>0</OuterId>  
	  <OldUserCardCode> <![CDATA[]]> </OldUserCardCode>  
	  <OuterStr> <![CDATA[12b]]> </OuterStr>  
	  <IsRestoreMemberCard>0</IsRestoreMemberCard>  
	  <IsRecommendByFriend>0</IsRecommendByFriend> 
	</xml>
	**/
	public void userGetCard( String event,String msgType,String fromUser,String toUser,String createTime,Map<String,Object> info);
	/**
	2.3转赠事件推送
	用户在转赠卡券时，微信会把这个事件推送到开发者填写的URL。 推送XML数据包示例：

	<xml>
	  <ToUserName><![CDATA[gh_3fcea188bf78]]></ToUserName>  
	  <FromUserName><![CDATA[obLatjjwDolFjRRd3doGIdwNqRXw]]></FromUserName>  
	  <CreateTime>1474181868</CreateTime>  
	  <MsgType><![CDATA[event]]></MsgType>  
	  <Event><![CDATA[user_gifting_card]]></Event>  
	  <CardId><![CDATA[pbLatjhU-3pik3d4PsbVzvBxZvJc]]></CardId>  
	  <UserCardCode><![CDATA[297466945104]]></UserCardCode>  
	  <IsReturnBack>0</IsReturnBack>  
	  <FriendUserName><![CDATA[obLatjlNerkb62HtSdQUx66C4NTU]]></FriendUserName>  
	  <IsChatRoom>0</IsChatRoom> 
	</xml>
	**/
	public void userGiftingCard( String event,String msgType,String fromUser,String toUser,String createTime,Map<String,Object> info);
	
	/**
	2.4 删除事件推送
	用户在删除卡券时，微信会把这个事件推送到开发者填写的URL。 推送XML数据包示例：
	<xml> <ToUserName><![CDATA[toUser]]></ToUserName> 
	<FromUserName><![CDATA[FromUser]]></FromUserName> 
	<CreateTime>123456789</CreateTime> 
	<MsgType><![CDATA[event]]></MsgType> 
	<Event><![CDATA[user_del_card]]></Event> 
	<CardId><![CDATA[cardid]]></CardId> 
	<UserCardCode><![CDATA[12312312]]></UserCardCode>
	</xml>
	**/
	public void userDelCard( String event,String msgType,String fromUser,String toUser,String createTime,Map<String,Object> info);
	
	/**
	2.5 核销事件推送</p>
	卡券被核销时，微信会把这个事件推送到开发者填写的URL。 推送XML数据包示例：</p>
	<xml> </p>
	  <ToUserName> <![CDATA[gh_fc0a06a20993]]> </ToUserName>  </p>
	  <FromUserName> <![CDATA[oZI8Fj040-be6rlDohc6gkoPOQTQ]]> </FromUserName>  </p>
	  <CreateTime>1472549042</CreateTime>  </p>
	  <MsgType> <![CDATA[event]]> </MsgType>  </p>
	  <Event> <![CDATA[user_consume_card]]> </Event>  </p>
	  <CardId> <![CDATA[pZI8Fj8y-E8hpvho2d1ZvpGwQBvA]]> </CardId>  </p>
	  <UserCardCode> <![CDATA[452998530302]]> </UserCardCode>  </p>
	  <ConsumeSource> <![CDATA[FROM_API]]> </ConsumeSource>  </p>
	  <LocationName> <![CDATA[]]> </LocationName>  </p>
	  <StaffOpenId> <![CDATA[oZ********nJ3bPJu_Rtjkw4c]]> </StaffOpenId>  </p>
	  <VerifyCode> <![CDATA[]]> </VerifyCode>  </p>
	  <RemarkAmount> <![CDATA[]]> </RemarkAmount>  </p>
	  <OuterStr> <![CDATA[xxxxx]]> </OuterStr> </p>
	</xml> </p>
	*/
	public void userConsumeCard( String event,String msgType,String fromUser,String toUser,String createTime,Map<String,Object> info);
	
	/**
	2.6 买单事件推送
	微信买单完成时，微信会把这个事件推送到开发者填写的URL。 推送XML数据包示例：
	<xml> 
	  <ToUserName><![CDATA[gh_e2243xxxxxxx]]></ToUserName>  
	  <FromUserName><![CDATA[oo2VNuOUuZGMxxxxxxxx]]></FromUserName>  
	  <CreateTime>1442390947</CreateTime>  
	  <MsgType><![CDATA[event]]></MsgType>  
	  <Event><![CDATA[user_pay_from_pay_cell]]></Event>  
	  <CardId><![CDATA[po2VNuCuRo-8sxxxxxxxxxxx]]></CardId>  
	  <UserCardCode><![CDATA[38050000000]]></UserCardCode>  
	  <TransId><![CDATA[10022403432015000000000]]></TransId>  
	  <LocationId>291710000</LocationId>  
	  <Fee><![CDATA[10000]]></Fee>  
	  <OriginalFee><![CDATA[10000]]> </OriginalFee> 
	</xml>
	**/
	public void userPayFromPayCell( String event,String msgType,String fromUser,String toUser,String createTime,Map<String,Object> info);
	
	/**
	2.7 进入会员卡事件推送
	用户在进入会员卡时，微信会把这个事件推送到开发者填写的URL。
	需要开发者在创建会员卡时填入need_push_on_view	字段并设置为true。开发者须综合考虑领卡人数和服务器压力，决定是否接收该事件。
	推送XML数据包示例：
	<xml> 
	  <ToUserName> <![CDATA[gh_fcxxxx6a20993]]> </ToUserName>  
	  <FromUserName> <![CDATA[oZI8Fj040-xxxxx6gkoPOQTQ]]> </FromUserName>  
	  <CreateTime>1467811138</CreateTime>  
	  <MsgType> <![CDATA[event]]> </MsgType>  
	  <Event> <![CDATA[user_view_card]]> </Event>  
	  <CardId> <![CDATA[pZI8Fj2ezBbxxxxxT2UbiiWLb7Bg]]> </CardId>  
	  <UserCardCode> <![CDATA[4xxxxxxxx8558]]> </UserCardCode>  
	  <OuterStr> <![CDATA[12b]]> </OuterStr> 
	</xml>
	**/
	public void userViewCard( String event,String msgType,String fromUser,String toUser,String createTime,Map<String,Object> info);
	
	/**
	2.8 从卡券进入公众号会话事件推送
	用户在卡券里点击查看公众号进入会话时（需要用户已经关注公众号），微信会把这个事件推送到开发者填写的URL。开发者可识别从卡券进入公众号的用户身份。 推送XML数据包示例：
	<xml> 
	  <ToUserName><![CDATA[toUser]]></ToUserName>  
	  <FromUserName><![CDATA[FromUser]]></FromUserName>  
	  <CreateTime>123456789</CreateTime>  
	  <MsgType><![CDATA[event]]></MsgType>  
	  <Event><![CDATA[user_enter_session_from_card]]></Event>  
	  <CardId><![CDATA[cardid]]></CardId>  
	  <UserCardCode><![CDATA[12312312]]></UserCardCode> 
	</xml>
	**/
	public void userEnterSessionFromCard( String event,String msgType,String fromUser,String toUser,String createTime,Map<String,Object> info);
	
	/**
	2.9 会员卡内容更新事件
	当用户的会员卡积分余额发生变动时，微信会推送事件告知开发者。 推送XML数据包示例：
	<xml>
	  <ToUserName><![CDATA[gh_9e1765b5568e]]></ToUserName>  
	  <FromUserName><![CDATA[ojZ8YtyVyr30HheH3CM73y7h4jJE]]></FromUserName>  
	  <CreateTime>1445507140</CreateTime>  
	  <MsgType><![CDATA[event]]></MsgType>  
	  <Event><![CDATA[update_member_card]]></Event>  
	  <CardId><![CDATA[pjZ8Ytx-nwvpCRyQneH3Ncmh6N94]]></CardId>  
	  <UserCardCode><![CDATA[485027611252]]></UserCardCode>  
	  <ModifyBonus>3</ModifyBonus>  
	  <ModifyBalance>0</ModifyBalance> 
	</xml>
	**/ 
	public void updateMemberCard( String event,String msgType,String fromUser,String toUser,String createTime,Map<String,Object> info);
	
	/**
	2.10 库存报警事件
	当某个card_id的初始库存数大于200且当前库存小于等于100时，用户尝试领券会触发发送事件给商户，事件每隔12h发送一次。
	<xml> 
	  <ToUserName><![CDATA[gh_2d62d*****0]]></ToUserName>  
	  <FromUserName><![CDATA[oa3LFuBvWb7*********]]></FromUserName>  
	  <CreateTime>1443838506</CreateTime>  
	  <MsgType><![CDATA[event]]></MsgType>  
	  <Event><![CDATA[card_sku_remind]]></Event>  
	  <CardId><![CDATA[pa3LFuAh2P65**********]]></CardId>  
	  <Detail><![CDATA[the card's quantity is equal to 0]]></Detail> 
	</xml>
	**/
	public void cardSkuRemind( String event,String msgType,String fromUser,String toUser,String createTime,Map<String,Object> info);
	
	/**
	2.11 券点流水详情事件
	当商户朋友的券券点发生变动时，微信服务器会推送消息给商户服务器。
	<xml> 
	  <ToUserName><![CDATA[gh_7223c83d4be5]]></ToUserName>  
	  <FromUserName><![CDATA[ob5E7s-HoN9tslQY3-0I4qmgluHk]]></FromUserName>  
	  <CreateTime>1453295737</CreateTime>  
	  <MsgType><![CDATA[event]]></MsgType>  
	  <Event><![CDATA[card_pay_order]]></Event>  
	  <OrderId><![CDATA[404091456]]></OrderId>  
	  <Status><![CDATA[ORDER_STATUS_FINANCE_SUCC]]></Status>  
	  <CreateOrderTime>1453295737</CreateOrderTime>  
	  <PayFinishTime>0</PayFinishTime>  
	  <Desc><![CDATA[]]></Desc>  
	  <FreeCoinCount><![CDATA[200]]></FreeCoinCount>  
	  <PayCoinCount><![CDATA[0]]></PayCoinCount>  
	  <RefundFreeCoinCount><![CDATA[0]]></RefundFreeCoinCount>  
	  <RefundPayCoinCount><![CDATA[0]]></RefundPayCoinCount>  
	  <OrderType><![CDATA[ORDER_TYPE_SYS_ADD]]></OrderType>  
	  <Memo><![CDATA[开通账户奖励]]></Memo>  
	  <ReceiptInfo><![CDATA[]]></ReceiptInfo> 
	</xml>  
	**/
	public void   cardPayOrder( String event,String msgType,String fromUser,String toUser,String createTime,Map<String,Object> info);
	
	/**
	2.12 会员卡激活事件推送
	当用户通过一键激活的方式提交信息并点击激活或者用户修改会员卡信息后，商户会收到用户激活的事件推送

	<xml> 
	  <ToUserName> <![CDATA[gh_3fcea188bf78]]></ToUserName>  
	  <FromUserName><![CDATA[obLatjlaNQKb8FqOvt1M1x1lIBFE]]></FromUserName>  
	  <CreateTime>1432668700</CreateTime>  
	  <MsgType><![CDATA[event]]></MsgType>  
	  <Event><![CDATA[submit_membercard_user_info]]></Event>  
	  <CardId><![CDATA[pbLatjtZ7v1BG_ZnTjbW85GYc_E8]]></CardId>  
	  <UserCardCode><![CDATA[018255396048]]></UserCardCode> 
	</xml>
	**/
	public void submitMembercardUserInfo( String event,String msgType,String fromUser,String toUser,String createTime,Map<String,Object> info);
	 
}
