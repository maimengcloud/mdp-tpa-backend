package com.mdp.tpa.wechat.api;

import java.util.Map;

import com.mdp.core.entity.Tips;
import com.mdp.tpa.wechat.wxpaysdk.WXPayConfig;

public interface PayService {
	 
	
	/**
	 * 商户在小程序中先调用该接口在微信支付服务后台生成预支付交易单，返回正确的预支付交易后调起支付。</p>
	 * URL地址：https://api.mch.weixin.qq.com/pay/unifiedorder</p>
	 *   微信分配的小程序ID 
	 * @param mchId 微信支付分配的商户号
	 * @param outTradeNo  商户系统内部的订单号,32个字符内、可包含字母, 其他说明见商户订单号
	 * @param body 商品简单描述，该字段须严格按照规范传递，具体请见参数规定
	 * @param attach 附加数据，在查询API和支付通知中原样返回，该字段主要用于商户携带订单的自定义数据
	 * @param spbillCreateIp APP和网页支付提交用户端ip，Native支付填调用微信支付API的机器IP。
	 * @param totalFee 订单总金额，单位为分，详见支付金额
	 * @param openid trade_type=JSAPI，此参数必传，用户在商户appid下的唯一标识。openid如何获取，可参考【获取openid】。
	 * @param tradeType
	 * @param sign 签名，详见签名生成算法
	 * @param nonceStr 机字符串，不长于32位。推荐随机数生成算法
	 * @param notifyUrl
	 * @param goodsTag 
	 * @param signType 签名类型，目前支持HMAC-SHA256和MD5，默认为MD5
	 * @param feeType 符合ISO 4217标准的三位字母代码，默认人民币：CNY，其他值列表详见货币类型
	 * @return
	 * <p> "举例</p>
	 * <xml></p>
		   <return_code><![CDATA[SUCCESS]]></return_code></p>
		   <return_msg><![CDATA[OK]]></return_msg></p>
		   <appid><![CDATA[wx2421b1c4370ec43b]]></appid></p>
		   <mch_id><![CDATA[10000100]]></mch_id></p>
		   <nonce_str><![CDATA[IITRi8Iabbblz1Jc]]></nonce_str></p>
		   <sign><![CDATA[7921E432F65EB8ED0CE9755F0E86D72F]]></sign></p>
		   <result_code><![CDATA[SUCCESS]]></result_code></p>
		   <prepay_id><![CDATA[wx201411101639507cbf6ffd8b0779950874]]></prepay_id></p>
		   <trade_type><![CDATA[JSAPI]]></trade_type></p>
		</xml></p>"
	 */
	public Map<String,Object> unifiedOrder(WXPayConfig config, String outTradeNo, String body, String attach, String spbillCreateIp, Integer totalFee, String goodsTag, String openid);
	
	/**
	 * 该接口提供所有微信支付订单的查询，商户可以通过查询订单接口主动查询订单状态，完成下一步的业务逻辑。</p>
		需要调用查询接口的情况：</p>
		◆ 当商户后台、网络、服务器等出现异常，商户系统最终未接收到支付通知；</p>
		◆ 调用支付接口后，返回系统错误或未知交易状态情况；</p>
		◆ 调用被扫支付API，返回USERPAYING的状态；</p>
		◆ 调用关单或撤销接口API之前，需确认支付状态；</p>
		https://api.mch.weixin.qq.com/pay/orderquery</p>
	 *   微信分配的小程序ID 或者公众号id
	 * @param mchId 微信支付分配的商户号
	 * @param transactionId 微信的订单号，优先使用
	 * @param outTradeNo 商户系统内部的订单号，当没提供transaction_id时需要传这个。
	 * @param nonceStr 随机字符串，不长于32位。推荐随机数生成算法
	 * @param sign 签名，详见签名生成算法
	 * @param signType 签名类型，目前支持HMAC-SHA256和MD5，默认为MD5
	 * @return 返回举例
	 * <xml></p>
		   <return_code><![CDATA[SUCCESS]]></return_code></p>
		   <return_msg><![CDATA[OK]]></return_msg></p>
		   <appid><![CDATA[wx2421b1c4370ec43b]]></appid></p>
		   <mch_id><![CDATA[10000100]]></mch_id></p>
		   <device_info><![CDATA[1000]]></device_info></p>
		   <nonce_str><![CDATA[TN55wO9Pba5yENl8]]></nonce_str></p>
		   <sign><![CDATA[BDF0099C15FF7BC6B1585FBB110AB635]]></sign></p>
		   <result_code><![CDATA[SUCCESS]]></result_code></p>
		   <openid><![CDATA[oUpF8uN95-Ptaags6E_roPHg7AG0]]></openid></p>
		   <is_subscribe><![CDATA[Y]]></is_subscribe></p>
		   <trade_type><![CDATA[MICROPAY]]></trade_type></p>
		   <bank_type><![CDATA[CCB_DEBIT]]></bank_type></p>
		   <total_fee>1</total_fee></p>
		   <fee_type><![CDATA[CNY]]></fee_type></p>
		   <transaction_id><![CDATA[1008450740201411110005820873]]></transaction_id></p>
		   <out_trade_no><![CDATA[1415757673]]></out_trade_no></p>
		   <attach><![CDATA[订单额外描述]]></attach></p>
		   <time_end><![CDATA[20141111170043]]></time_end></p>
		   <trade_state><![CDATA[SUCCESS]]></trade_state></p>
		</xml></p>
		
		错误返回：
		  
	 */
	public Map<String,Object> orderQuery(WXPayConfig config,String outTradeNo);

	/**
	 * 当交易发生之后一段时间内，由于买家或者卖家的原因需要退款时，卖家可以通过退款接口将支付款退还给买家，微信支付将在收到退款请求并且验证成功之后，按照退款规则将支付款按原路退到买家帐号上。</p>
		注意：</p>
		1、交易时间超过一年的订单无法提交退款；</p>
		2、微信支付退款支持单笔交易分多次退款，多次退款需要提交原支付订单的商户订单号和设置不同的退款单号。一笔退款失败后重新提交，要采用原来的退款单号。总退款金额不能超过用户实际支付金额。</p>
		接口链接：https://api.mch.weixin.qq.com/secapi/pay/refund</p>
	 *   微信分配的小程序ID
	 * @param mchId 微信支付分配的商户号
	 * @param transactionId 微信生成的订单号，在支付通知中有返回
	 * @param outTradeNo 商户侧传给微信的订单号
	 * @param outRefundNo 商户系统内部的退款单号，商户系统内部唯一，同一退款单号多次请求只退一笔
	 * @param totalFee 订单总金额，单位为分，只能为整数，详见支付金额
	 * @param refundFee 退款总金额，订单总金额，单位为分，只能为整数，详见支付金额
	 * @param refundFeeType 货币类型，符合ISO 4217标准的三位字母代码，默认人民币：CNY，其他值列表详见货币类型
	 * @param nonceStr 随机字符串，不长于32位。推荐随机数生成算法
	 * @param sign 签名，详见签名生成算法
	 * @param signType 签名类型，目前支持HMAC-SHA256和MD5，默认为MD5
	 * @param opUserid 操作员帐号, 默认为商户号
	 * @return 
	 * <xml></p>
		   <return_code><![CDATA[SUCCESS]]></return_code></p>
		   <return_msg><![CDATA[OK]]></return_msg></p>
		   <appid><![CDATA[wx2421b1c4370ec43b]]></appid></p>
		   <mch_id><![CDATA[10000100]]></mch_id></p>
		   <nonce_str><![CDATA[NfsMFbUFpdbEhPXP]]></nonce_str></p>
		   <sign><![CDATA[B7274EB9F8925EB93100DD2085FA56C0]]></sign></p>
		   <result_code><![CDATA[SUCCESS]]></result_code></p>
		   <transaction_id><![CDATA[1008450740201411110005820873]]></transaction_id></p>
		   <out_trade_no><![CDATA[1415757673]]></out_trade_no></p>
		   <out_refund_no><![CDATA[1415701182]]></out_refund_no></p>
		   <refund_id><![CDATA[2008450740201411110000174436]]></refund_id></p>
		   <refund_channel><![CDATA[]]></refund_channel></p>
		   <refund_fee>1</refund_fee> </p>
		</xml></p>
	 */
	public Map<String,Object> refund(WXPayConfig config ,String outTradeNo,String outRefundNo,Integer totalFee,Integer refundFee );

	/**
	 * 企业付款
	 *  
	 * @param openid
	 * @param amount
	 * @return
	 */
	public Tips businessPayment(WXPayConfig config,String openid,double amount,String desc);
	
	/**
	 * 商户在小程序中先调用该接口在微信支付服务后台添加分账接收方。</p>
	 * URL地址：https://api.mch.weixin.qq.com/pay/profitsharingaddreceiver</p>
	
	 * @param mchId 微信支付分配的商户号
	 * @param outTradeNo  商户系统内部的订单号,32个字符内、可包含字母, 其他说明见商户订单号
	 * @param sign 签名，详见签名生成算法
	 * @param receiver 分账接收方
	 * @return
	 * <p> "举例</p>
	 * <xml></p>
		   <return_code><![CDATA[SUCCESS]]></return_code></p>
		   <return_msg><![CDATA[OK]]></return_msg></p>
		   <appid><![CDATA[wx2421b1c4370ec43b]]></appid></p>
		   <mch_id><![CDATA[10000100]]></mch_id></p>
		   <nonce_str><![CDATA[IITRi8Iabbblz1Jc]]></nonce_str></p>
		   <sign><![CDATA[7921E432F65EB8ED0CE9755F0E86D72F]]></sign></p>
		   <receiver><![CDATA[{
			"type": "MERCHANT_ID",
			"account":"190001001",
			"name": "示例商户全称",
			"relation_type": "STORE_OWNER"
			}]]>
			</receiver></p>
		</xml></p>"
	 */
	public Map<String,Object> profitsharingaddreceiver(String appid,String mchId,String key,String receiver)throws Exception;
	
	/**
	 * 服务商代子商户发起分账请求，将结算后的钱分到分账接收方。</p>
	 * URL地址：https://api.mch.weixin.qq.com/secapi/pay/multiprofitsharing</p>
	
	 * @param mchId 微信支付分配的商户号
	 * @param sub_mch_id 子商户号
	 * @param appid 微信分配的公众账号ID 
	 * @param nonce_str 随机字符串
	 * @param sign 签名，详见签名生成算法
	 * @param transaction_id  微信支付订单号 
	 * @param out_order_no 商户系统内部的分账单号
	 * @param receivers 分账接收方列表
	 * @return
	 * <p> "举例</p>
	 * <xml>
		   <return_code><![CDATA[SUCCESS]]></return_code>
		    <return_msg><![CDATA[OK]]></return_msg>
			<result_code><![CDATA[SUCCESS]]></result_code>
		   <mch_id>10000100</mch_id>
			<appid>wx2421b1c4370ec43b</appid>
		   <sub_mch_id>1415701182</sub_mch_id>
			<sub_appid>wx2203b1494370e08cm</sub_appid>
		   <nonce_str>6cefdb308e1e2e8aabd48cf79e546a02</nonce_str> 
		   <out_order_no>P20150806125346</out_order_no>
		   <transaction_id>4006252001201705123297353072</transaction_id>
		   <order_id>3008450740201411110007820472</order_id>
		   <sign>FE56DD4AA85C0EECA82C35595A69E153</sign>
		 </xml> </p>"
	 */
	public Map<String,Object> multiprofitsharing(String mchId,String appid,String key,String transactionId,String outOrderNo,String receivers) throws Exception;
	
}
