package com.mdp.tpa.wechat.service.cache.redis;

import java.util.concurrent.TimeUnit;

import com.mdp.core.api.CacheHKVService;
import com.mdp.core.api.CacheService;
import org.springframework.beans.factory.annotation.Autowired;
 
public abstract class DefaultRedisCacheService<T>  implements CacheService<T> {
	
	@Autowired
	CacheHKVService cacheHKVService;
	 

	@Override
	public void put(String key, T o) {
		String okey=this.getCacheKey()+":"+key;
		cacheHKVService.put(okey, key, o);
		cacheHKVService.expire(okey, 1, TimeUnit.DAYS);
	}

	@Override
	public T get(String key) {
		String okey=this.getCacheKey()+":"+key;
		return  (T) cacheHKVService.get(okey, key);
	}

	@Override
	public void remove(String key) {
		cacheHKVService.delete(getCacheKey(), key); 
	}

	@Override
	public boolean containsKey(String key) {
		// TODO Auto-generated method stub
		return cacheHKVService.containsKey(getCacheKey(), key);
	}

	@Override
	public void refresh() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean containsValue(T value) {
		// TODO Auto-generated method stub
		return false;
	}
	
	public abstract String getCacheKey() ;
	
	@Override
	public boolean expire(long milliseconds) {
		// TODO Auto-generated method stub
		return cacheHKVService.expire(getCacheKey(), milliseconds, TimeUnit.MILLISECONDS);
	}

}
