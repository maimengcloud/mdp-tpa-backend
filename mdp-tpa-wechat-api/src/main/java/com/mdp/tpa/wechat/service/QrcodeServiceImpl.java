package com.mdp.tpa.wechat.service;

import java.util.HashMap;
import java.util.Map;

import com.mdp.tpa.wechat.HttpRequestUtil;
import com.mdp.tpa.wechat.api.AccessTokenService;
import com.mdp.tpa.wechat.api.ApiUrls;
import com.mdp.tpa.wechat.api.QrcodeService;
import org.springframework.beans.factory.annotation.Autowired;

public class QrcodeServiceImpl implements QrcodeService {

	@Autowired
	AccessTokenService tokenService;
	
	@Override
	public Map<String, Object> createTemporary(String actionName, String expireSeconds, String sceneId,
			String sceneStr) {
		Map<String,Object> p=new HashMap<>();
		p.put("action_name",actionName);
		Map<String,Object> action_info=new HashMap<>();
		Map<String,Object> scene=new HashMap<>();
		scene.put("scene_str",sceneStr);
		action_info.put("scene",scene);
		p.put("action_info",action_info);
		p.put("expire_seconds",expireSeconds);
		String url=String.format(ApiUrls.QRCODE_CREATE,tokenService.getToken());
		Map<String,Object>	result= HttpRequestUtil.sendPostJson(url,p);
		return result;
	}

	@Override
	public Map<String, Object> createForever(String sceneStr) {
		Map<String,Object> p=new HashMap<>();
		p.put("action_name","QR_LIMIT_STR_SCENE");
		Map<String,Object> action_info=new HashMap<>();
		Map<String,Object> scene=new HashMap<>();
		scene.put("scene_str",sceneStr);
		action_info.put("scene",scene);
		p.put("action_info",action_info);
		String url=String.format(ApiUrls.QRCODE_CREATE,tokenService.getToken());
		Map<String,Object>	result= HttpRequestUtil.sendPostJson(url,p);
		return result;
	}

}
