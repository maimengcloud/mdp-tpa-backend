package com.mdp.tpa.wechat.service;

import static com.mdp.core.utils.BaseUtils.map;

import java.util.Map;

import com.mdp.tpa.wechat.HttpRequestUtil;
import com.mdp.tpa.wechat.api.AccessTokenService;
import com.mdp.tpa.wechat.api.ApiUrls;
import com.mdp.tpa.wechat.api.CardConsumeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mdp.core.entity.Tips;

@Service
public class CardConsumeServiceImpl implements CardConsumeService {
	
	@Autowired
	AccessTokenService tokenService;
	
	@Override
	public Map<String, Object> codeGet( String cardId, String code, boolean checkConsume) {
		Map<String,Object> p=map("card_id",cardId,"code",code,"check_consume",checkConsume);
		String url=String.format(ApiUrls.CARD_CODE_GET,tokenService.getToken());
		return HttpRequestUtil.sendGetJson(url,p);
	}

	@Override
	public Tips codeUnavailable( String cardId, String code, String reason) {
		Tips tips =new Tips("设置卡券失效成功");
		Map<String,Object> p=map("code",code,"reason",reason);
		String url=String.format(ApiUrls.CARD_CODE_UNAVAILABLE, tokenService.getToken());
		Map<String,Object>	result= HttpRequestUtil.sendPostJson(url,p);
		Integer errcode= (Integer) result.get("errcode");
		String errmsg= (String) result.get("errmsg");
		if(errcode!=0){
			tips.setErrMsg(errmsg);
		}
		return tips;
	}

	@Override
	public Map<String, Object> codeConsume( String code) {
		Tips tips =new Tips("卡券核销成功");
		Map<String,Object> p=map("code",code);
		String url=String.format(ApiUrls.CARD_CODE_CONSUME, tokenService.getToken());
		Map<String,Object>	result= HttpRequestUtil.sendPostJson(url,p);
		return result;
	}

}
