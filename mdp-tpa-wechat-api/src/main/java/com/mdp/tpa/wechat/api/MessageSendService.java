package com.mdp.tpa.wechat.api;

import java.util.List;
import java.util.Map;

/**
 * 向微信用户发送消息接口
 * @author Administrator
 *
 */
public interface MessageSendService {
	

	
	/**
	 * 根据标签群发投放图文消息  </p>
	 * http请求方式: POST</p>
     * https://api.weixin.qq.com/cgi-bin/message/mass/sendall?access_token=ACCESS_TOKEN
     * @param mediaId 用于群发的消息的media_id
	 * @param tagId 标签分类 群发到的标签的tag_id，参加用户管理中用户分组接口，若is_to_all值为true，可不填写tag_id
	 * @param isToAll 是否全发 用于设定是否向全部用户发送，值为true或false，选择true该消息群发给所有用户，选择false可根据tag_id发送给指定群组的用户
	 * @return
	 * {</p>
		   "errcode":0,</p>
		   "errmsg":"send job submission success",</p>
		   "msg_id":34182, </p>
		   "msg_data_id": 206227730</p>
		}</p>
	 */
	public Map<String,Object> sendMpNewsByTag(  String mediaId,int tagId);
	
	public Map<String,Object> sendMpNewsToAll(  String mediaId);

	
	/**
	 * 根据标签群发投放图文消息  </p>
	 * http请求方式: POST</p>
     * https://api.weixin.qq.com/cgi-bin/message/mass/sendall?access_token=ACCESS_TOKEN
     * @param content 文字内容
	 * @param tagId 标签分类 群发到的标签的tag_id，参加用户管理中用户分组接口，若is_to_all值为true，可不填写tag_id
	 * @param isToAll 是否全发 用于设定是否向全部用户发送，值为true或false，选择true该消息群发给所有用户，选择false可根据tag_id发送给指定群组的用户
	 * @return
	 * {</p>
		   "errcode":0,</p>
		   "errmsg":"send job submission success",</p>
		   "msg_id":34182, </p>
		   "msg_data_id": 206227730</p>
		}</p>
	 */
	public Map<String,Object> sendTextByTag(  String content,int tagId);
	
	public Map<String,Object> sendTextToAll(  String content);

	
	/**
	 * 根据标签群发投放图文消息  </p>
	 * http请求方式: POST</p>
     * https://api.weixin.qq.com/cgi-bin/message/mass/sendall?access_token=ACCESS_TOKEN
     * @param mediaId 用于群发的消息的media_id
	 * @param tagId 标签分类 群发到的标签的tag_id，参加用户管理中用户分组接口，若is_to_all值为true，可不填写tag_id
	 * @param isToAll 是否全发 用于设定是否向全部用户发送，值为true或false，选择true该消息群发给所有用户，选择false可根据tag_id发送给指定群组的用户
	 * @return
	 * {</p>
		   "errcode":0,</p>
		   "errmsg":"send job submission success",</p>
		   "msg_id":34182, </p>
		   "msg_data_id": 206227730</p>
		}</p>
	 */
	public Map<String,Object> sendVoiceByTag(  String mediaId,int tagId);
	public Map<String,Object> sendVoicToAll(  String mediaId );

	
	
	/**
	 * 根据标签群发投放图文消息  </p>
	 * http请求方式: POST</p>
     * https://api.weixin.qq.com/cgi-bin/message/mass/sendall?access_token=ACCESS_TOKEN
     * @param mediaId 用于群发的消息的media_id
	 * @param tagId 标签分类 群发到的标签的tag_id，参加用户管理中用户分组接口，若is_to_all值为true，可不填写tag_id
	 * @param isToAll 是否全发 用于设定是否向全部用户发送，值为true或false，选择true该消息群发给所有用户，选择false可根据tag_id发送给指定群组的用户
	 * @return
	 * {</p>
		   "errcode":0,</p>
		   "errmsg":"send job submission success",</p>
		   "msg_id":34182, </p>
		   "msg_data_id": 206227730</p>
		}</p>
	 */
	public Map<String,Object> sendMpVideoByTag(  String mediaId,int tagId);
	public Map<String,Object> sendMpVideoToAll(  String mediaId);

	/**
	 * 根据标签群发投放图文消息  </p>
	 * http请求方式: POST</p>
     * https://api.weixin.qq.com/cgi-bin/message/mass/sendall?access_token=ACCESS_TOKEN
     * @param cardId 用于群发的消息的cardId
	 * @param tagId 标签分类 群发到的标签的tag_id，参加用户管理中用户分组接口，若is_to_all值为true，可不填写tag_id
	 * @param isToAll 是否全发 用于设定是否向全部用户发送，值为true或false，选择true该消息群发给所有用户，选择false可根据tag_id发送给指定群组的用户
	 * @return
	 * {</p>
		   "errcode":0,</p>
		   "errmsg":"send job submission success",</p>
		   "msg_id":34182, </p>
		   "msg_data_id": 206227730</p>
		}</p>
	 */
	public Map<String,Object> sendWxCardByTag(  String cardId,int tagId); 
	public Map<String,Object> sendWxCardToAll(  String cardId); 

	/**
	 * 根据OpenID列表群发</p>
	 * http请求方式: POST</p>
     * https://api.weixin.qq.com/cgi-bin/message/mass/sendall?access_token=ACCESS_TOKEN</p>
	 * @param mediaId 用于群发的消息的media_id
	 * @param openids openid列表
	 * @return
	 * {</p>
		   "errcode":0,</p>
		   "errmsg":"send job submission success",</p>
		   "msg_id":34182, </p>
		   "msg_data_id": 206227730</p>
		}</p>
	 */
	public Map<String,Object> sendMpNewsByOpenids(  String mediaId,List<String> openids);
	public Map<String,Object> sendMpNewsByOpenid(  String mediaId,String openid);

	/**
	 * 根据OpenID列表群发</p>
	 * http请求方式: POST</p>
     * https://api.weixin.qq.com/cgi-bin/message/mass/sendall?access_token=ACCESS_TOKEN</p>
	 * @param mediaId 用于群发的消息的media_id
	 * @param openids openid列表
	 * @return
	 * {</p>
		   "errcode":0,</p>
		   "errmsg":"send job submission success",</p>
		   "msg_id":34182, </p>
		   "msg_data_id": 206227730</p>
		}</p>
	 */
	public Map<String,Object> sendTextByOpenids(  String content,List<String> openids);
	public Map<String,Object> sendTextByOpenid(  String content,String openid);

	
	/**
	 * 根据OpenID列表群发</p>
	 * http请求方式: POST</p>
     * https://api.weixin.qq.com/cgi-bin/message/mass/sendall?access_token=ACCESS_TOKEN</p>
	 * @param mediaId 用于群发的消息的media_id
	 * @param openids openid列表
	 * @return
	 * {</p>
		   "errcode":0,</p>
		   "errmsg":"send job submission success",</p>
		   "msg_id":34182, </p>
		   "msg_data_id": 206227730</p>
		}</p>
	 */
	public Map<String,Object> sendVoiceByOpenids(  String mediaId,List<String> openids);
	public Map<String,Object> sendVoiceByOpenid(  String mediaId,String openid);

	
	
	/**
	 * 根据OpenID列表群发</p>
	 * http请求方式: POST</p>
     * https://api.weixin.qq.com/cgi-bin/message/mass/sendall?access_token=ACCESS_TOKEN</p>
	 * @param mediaId 用于群发的消息的media_id
	 * @param openids openid列表
	 * @return
	 * {</p>
		   "errcode":0,</p>
		   "errmsg":"send job submission success",</p>
		   "msg_id":34182, </p>
		   "msg_data_id": 206227730</p>
		}</p>
	 */
	public Map<String,Object> sendMpVideoByOpenids( String  mediaId,String title,String description, List<String> openids);
	public Map<String,Object> sendMpVideoByOpenid( String  mediaId,String title,String description, String openids);

	
	/**
	 * 根据OpenID列表群发</p>
	 * http请求方式: POST</p>
     * https://api.weixin.qq.com/cgi-bin/message/mass/sendall?access_token=ACCESS_TOKEN</p>
	 * @param cardId 用于群发的消息的卡编号
	 * @param openids openid列表
	 * @return
	 * {</p>
		   "errcode":0,</p>
		   "errmsg":"send job submission success",</p>
		   "msg_id":34182, </p>
		   "msg_data_id": 206227730</p>
		}</p>
	 */
	public Map<String,Object> sendWxCardByOpenids(  String cardId,List<String> openids);
	public Map<String,Object> sendWxCardByOpenid(  String cardId,String openid);

	/**
	 * 根据标签群发投放图文消息  </p>
	 * http请求方式: POST</p>
     * https://api.weixin.qq.com/cgi-bin/message/mass/sendall?access_token=ACCESS_TOKEN
     * @param mediaId 用于群发的消息的media_id
	 * @param tagId 标签分类 群发到的标签的tag_id，参加用户管理中用户分组接口，若is_to_all值为true，可不填写tag_id
	 * @param isToAll 是否全发 用于设定是否向全部用户发送，值为true或false，选择true该消息群发给所有用户，选择false可根据tag_id发送给指定群组的用户
	 * @return
	 * {</p>
		   "errcode":0,</p>
		   "errmsg":"send job submission success",</p>
		   "msg_id":34182, </p>
		   "msg_data_id": 206227730</p>
		}</p>
	 */ 
	Map<String, Object> sendImageByTag(  List<String> mediaIds, String recommend, int tagId);
	Map<String, Object> sendImageToAll(  List<String> mediaIds, String recommend);

	/**
	 * 根据OpenID列表群发</p>
	 * http请求方式: POST</p>
     * https://api.weixin.qq.com/cgi-bin/message/mass/sendall?access_token=ACCESS_TOKEN</p>
	 * @param mediaId 用于群发的消息的media_id
	 * @param openids openid列表
	 * @return
	 * {</p>
		   "errcode":0,</p>
		   "errmsg":"send job submission success",</p>
		   "msg_id":34182, </p>
		   "msg_data_id": 206227730</p>
		}</p>
	 */ 
	Map<String, Object> sendImageByOpenids(  List<String> mediaIds, String recommend,
			List<String> openids); 
	Map<String, Object> sendImageByOpenid(  List<String> mediaIds, String recommend,
			String openid); 

	/**
	 * 发送模板消息
	 * @param accessToken
	 * @param templateId 从公众号上获取
	 * @param openid 接收人
	 * @param url 点击消息跳转路径，可为空
	 * @param data 模板中定义的变量及数据
	 * @return
	 */
	Map<String, Object> sendTemplateMessage(  String templateId,String openid,String url,Map<String,Object> data); 
	
	/**
	 * 拉取私有模板
	 * @param accessToken 
	 * @return
	 * 
	 * {	<p>
		     "template_list": [{</p>
			      "template_id": "iPk5sOIt5X_flOVKn5GrTFpncEYTojx6ddbt8WYoV5s",</p>
			      "title": "领取奖金提醒",</p>
			      "primary_industry": "IT科技",</p>
			      "deputy_industry": "互联网|电子商务",</p>
			      "content": "{ {result.DATA} }\n\n领奖金额:{ {withdrawMoney.DATA} }\n领奖  时间:    { {withdrawTime.DATA} }\n银行信息:{ {cardInfo.DATA} }\n到账时间:  { {arrivedTime.DATA} }\n{ {remark.DATA} }",</p>
			      "example": "您已提交领奖申请\n\n领奖金额：xxxx元\n领奖时间：2013-10-10 12:22:22\n银行信息：xx银行(尾号xxxx)\n到账时间：预计xxxxxxx\n\n预计将于xxxx到达您的银行卡"</p>
			   }]</p>
		}
	 */
	Map<String, Object> getAllTemplate( );
}
