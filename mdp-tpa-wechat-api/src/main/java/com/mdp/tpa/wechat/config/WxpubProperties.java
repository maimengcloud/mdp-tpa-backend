package com.mdp.tpa.wechat.config;

import java.util.Date;
import java.util.Properties;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
@Data
@ConfigurationProperties(prefix = "mdp.apps.wxpub")
public class WxpubProperties extends AppProperties{


}