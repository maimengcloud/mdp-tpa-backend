package com.mdp.tpa.wechat.api;

public class ApiUrls {

	public static String TOKEN="https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid=%s&secret=%s";
	public static String SNS_ACCESS_TOKEN_BY_CODE="https://api.weixin.qq.com/sns/oauth2/access_token?appid=%s&secret=%s&code=%s&grant_type=authorization_code"; 
	public static String SNS_ACCESS_TOKEN_REFRESH="https://api.weixin.qq.com/sns/oauth2/refresh_token?appid=%s&grant_type=refresh_token&refresh_token=%s"; 
	public static String WXA_JSCODE2SESSION="https://api.weixin.qq.com/sns/jscode2session?appid=%s&secret=%s&js_code=%s&grant_type=authorization_code"; 

	//user
	public static String USER_INFO="https://api.weixin.qq.com/cgi-bin/user/info?access_token=%s&openid=%s&lang=zh_CN"; 
	public static String SNS_USER_INFO="https://api.weixin.qq.com/sns/userinfo?access_token=%s&openid=%s&lang=zh_CN"; 
	public static String USER_GET="https://api.weixin.qq.com/cgi-bin/user/get?access_token=%s&next_openid=%s"; 
	
	//replyMessage
 
 
	public static String MENU_CREATE="https://api.weixin.qq.com/cgi-bin/menu/create?access_token=%s";
	public static String MENU_DELETE="https://api.weixin.qq.com/cgi-bin/menu/delete?access_token=%s"; 
	
	//wxa
	public static String  WXA_GETWXACODEUNLIMIT="https://api.weixin.qq.com/wxa/getwxacodeunlimit?access_token=%s";  

	//ticket
	public static String TICKET_GET="https://api.weixin.qq.com/cgi-bin/ticket/getticket?access_token=%s&type=%s"; 
	
	
	//pay
	public static String PAY_UNIFIEDORDER="https://api.mch.weixin.qq.com/pay/unifiedorder"; 
	
	//发送个人消息，群发消息/cgi-bin/message/mass/send
	public static String MSG_SEND="https://api.weixin.qq.com/cgi-bin/message/mass/send?access_token=%s";
	public static String MSG_SEND_ALL="https://api.weixin.qq.com/cgi-bin/message/mass/sendall?access_token=%s";
	public static String MSG_SEND_TEMPLATE="https://api.weixin.qq.com/cgi-bin/message/template/send?access_token=%s";
	public static String MSG_TEMPLATE_GET_ALL="https://api.weixin.qq.com/cgi-bin/template/get_all_private_template?access_token=%s";

	//用户订阅消息，业务服务器主动推送消息
	public static String MSG_SUBSCRIBE_BIZSEND="https://api.weixin.qq.com/cgi-bin/message/subscribe/bizsend?access_token=%s";

	//公众号和小程序共用的消息下发接口--新接口
	public static String UNIFORM_SEND="https://api.weixin.qq.com/cgi-bin/message/wxopen/template/uniform_send?access_token=%s";


	// 卡券相关
	//卡券-基础操作
	public static String CARD_CREATE="https://api.weixin.qq.com/card/create?access_token=%s";
	public static String CARD_GET="https://api.weixin.qq.com/card/get?access_token=%s";
	public static String CARD_DELETE="https://api.weixin.qq.com/card/delete?access_token=%s";
	public static String CARD_BATCHGET="https://api.weixin.qq.com/card/batchget?access_token=%s";
	public static String CARD_UPDATE="https://api.weixin.qq.com/card/update?access_token=%s";
	public static String CARD_MODIFYSTOCK="https://api.weixin.qq.com/card/modifystock?access_token=%s";
	public static String CARD_PAYCELL_SET="https://api.weixin.qq.com/card/paycell/set?access_token=%s";
	public static String CARD_SELFCONSUMECELL_SET="https://api.weixin.qq.com/card/selfconsumecell/set?access_token=%s";
	public static String CGI_BIN_MEDIA_UPLOADIMG="https://api.weixin.qq.com/cgi-bin/media/uploadimg?access_token=%s";
	public static String CARD_USER_GETCARDLIST="https://api.weixin.qq.com/card/user/getcardlist?access_token=%s";



	//卡券-数据分析
	public static String CARD_DATACUBE_GETCARDBIZUININFO="https://api.weixin.qq.com/datacube/getcardbizuininfo?access_token=%s";
	public static String CARD_DATACUBE_GETCARDCARDINFO="https://api.weixin.qq.com/datacube/getcardcardinfo?access_token=%s";
	public static String CARD_DATACUBE_GETCARDMEMBERCARDDETAIL="https://api.weixin.qq.com/datacube/getcardmembercarddetail?access_token=%s";
	public static String CARD_DATACUBE_GETCARDMEMBERCARDINFO="https://api.weixin.qq.com/datacube/getcardmembercardinfo?access_token=%s";

	//卡券核销码
	public static String CARD_CODE_GET="https://api.weixin.qq.com/card/code/get?access_token=%s";
	public static String CARD_CODE_CONSUME="https://api.weixin.qq.com/card/code/consume?access_token=%s";
	public static String CARD_CODE_UPDATE="https://api.weixin.qq.com/card/code/update?access_token=%s";
	public static String CARD_CODE_UNAVAILABLE="https://api.weixin.qq.com/card/code/unavailable?access_token=%s";


	//素材相关
	public static String MATERIAL_BATCHGET="https://api.weixin.qq.com/cgi-bin/material/batchget_material?access_token=%s";
	public static String MATERIAL_MATERIALCOUNT="https://api.weixin.qq.com/cgi-bin/material/get_materialcount?access_token=%s";

	//门店
	public static String POI_ADDPOI="https://api.weixin.qq.com/cgi-bin/poi/addpoi?access_token=%s";

	//用户标签
	public static String TAGS_CREATE="https://api.weixin.qq.com/cgi-bin/tags/create?access_token=%s";
	public static String TAGS_UPDATE="https://api.weixin.qq.com/cgi-bin/tags/update?access_token=%s";
	public static String TAGS_DELETE="https://api.weixin.qq.com/cgi-bin/tags/delete?access_token=%s";
	public static String TAGS_GET="https://api.weixin.qq.com/cgi-bin/tags/get?access_token=%s";
	public static String TAGS_GETIDLIST="https://api.weixin.qq.com/cgi-bin/tags/getidlist?access_token=%s";
	public static String TAGS_USER_TAG_GET="https://api.weixin.qq.com/cgi-bin/user/tag/get?access_token=%s";
	public static String TAGS_BATCHTAGGING="https://api.weixin.qq.com/cgi-bin/tags/members/batchtagging?access_token=%s";
	public static String TAGS_BATCHUNTAGGING="https://api.weixin.qq.com/cgi-bin/tags/members/batchuntagging?access_token=%s";



 	// 二维码
	public static String QRCODE_CREATE="https://api.weixin.qq.com/cgi-bin/qrcode/create?access_token=%s";




}
