package com.mdp.tpa.wechat.api;

import com.mdp.tpa.wechat.api.cache.Oauth2CodeCache;
import com.mdp.tpa.wechat.api.cache.Oauth2TokenCache;
import com.mdp.tpa.wechat.config.AppProperties;
import com.mdp.tpa.wechat.entity.Oauth2AccessToken;

/**
 * 公众号网页版登录使用，通过code获取AccessToken，再获取userInfo
 * @author Administrator
 *
 */
public interface Oauth2AccessTokenService{
	 
	
	/**
	 * code 5分钟内过期，并且只能用一次
	 * 这里通过code换取的是一个特殊的网页授权access_token,与基础支持中的access_token（该access_token用于调用其他接口）不同。公众号可通过下述接口来获取网页授权access_token。如果网页授权的作用域为snsapi_base，则本步骤中获取到网页授权access_token的同时，也获取到了openid，snsapi_base式的网页授权流程即到此为止。
	 * https://api.weixin.qq.com/sns/oauth2/access_token?appid=APPID&secret=SECRET&code=CODE&grant_type=authorization_code 
	 * @param code  此code为微信授权后回调本系统时带上的code参数
	 * @return AccessToken 错误时返回null
	 * 接口返回数据：
	 * { "access_token":"ACCESS_TOKEN",    
		 "expires_in":7200,    
		 "refresh_token":"REFRESH_TOKEN",    
		 "openid":"OPENID",    
		 "scope":"SCOPE" 
	   } 
	 * 错误时：
	 * {"errcode":40029,"errmsg":"invalid code"} 
	 */
	public Oauth2AccessToken getSnsOauth2AccessToken(AppProperties appProperties, String code);
	
	/**
	 * AccessToken可持续30天不再授权，刷新间隔时间2小时 即7200秒
	 * 刷新access_token（如果需要）
	 * 由于access_token拥有较短的有效期，当access_token超时后，可以使用refresh_token进行刷新，refresh_token有效期为30天，当refresh_token失效之后，需要用户重新授权。
	 * https://api.weixin.qq.com/sns/oauth2/refresh_token?appid=APPID&grant_type=refresh_token&refresh_token=REFRESH_TOKEN
	 * @param refreshToken  此code为微信授权后回调本系统时带上的code参数
	 * @return AccessToken 
	 * 接口返回数据：
	 * { "access_token":"ACCESS_TOKEN",    
		 "expires_in":7200,    
		 "refresh_token":"REFRESH_TOKEN",    
		 "openid":"OPENID",    
		 "scope":"SCOPE" 
	   } 
	 * 错误时：
	 * {"errcode":40029,"errmsg":"invalid code"} 
	 */
	public Oauth2AccessToken refreshSnsOauth2AccessToken(String appid,String refreshToken);

	/**
	 * AccessToken可持续30天不再授权，刷新间隔时间2小时 即7200秒
	 * @param accessToken
	 * @return Oauth2AccessToken
	 * 异常返回 null
	 */
	Oauth2AccessToken getSnsOauth2AccessToken(AppProperties appProperties, Oauth2AccessToken accessToken);
	

 


	public void setOauth2AccessTokenCache(Oauth2TokenCache oauth2AccessTokenCache) ;
 


	public void setOauth2CodeCache(Oauth2CodeCache oauth2CodeCache) ;
	 
}
