package com.mdp.tpa.wechat.service;
import java.util.List;
import java.util.Map;

import com.mdp.tpa.wechat.api.MessageReplyService;
import org.springframework.stereotype.Service;

@Service
public class MessageReplyServiceImpl implements MessageReplyService {

	
	String ReplyText=" <xml> <ToUserName><![CDATA[%s]]></ToUserName> <FromUserName><![CDATA[%s]]></FromUserName> <CreateTime>%s</CreateTime> <MsgType><![CDATA[text]]></MsgType> <Content><![CDATA[%s]]></Content></xml> ";
	String ReplyNews=" <xml> <ToUserName><![CDATA[%s]]></ToUserName> <FromUserName><![CDATA[%s]]></FromUserName> <CreateTime>%s</CreateTime> <MsgType><![CDATA[news]]></MsgType> <ArticleCount><![CDATA[%s]]></ArticleCount><Articles>%s</Articles></xml> ";
	String ReplyImage="<xml> <ToUserName><![CDATA[%s]]></ToUserName> <FromUserName><![CDATA[%s]]></FromUserName> <CreateTime>%s</CreateTime> <MsgType><![CDATA[image]]></MsgType> <Image><MediaId>%s</MediaId></Image></xml> ";
	String ReplyVoice="<xml> <ToUserName><![CDATA[%s]]></ToUserName> <FromUserName><![CDATA[%s]]></FromUserName> <CreateTime>%s</CreateTime> <MsgType><![CDATA[voice]]></MsgType> <Voice><MediaId>%s</MediaId></Voice></xml> ";
	String ReplyMusic="<xml> <ToUserName><![CDATA[%s]]></ToUserName> <FromUserName><![CDATA[%s]]></FromUserName> <CreateTime>%s</CreateTime> <MsgType><![CDATA[voice]]></MsgType> <Music><Title>%s</Title><Description>%s</Description><MusicUrl>%s</MusicUrl><HQMusicUrl>%s</HQMusicUrl><ThumbMediaId>%s</ThumbMediaId></Music></xml> ";
	String ReplyVideo="<xml> <ToUserName><![CDATA[%s]]></ToUserName> <FromUserName><![CDATA[%s]]></FromUserName> <CreateTime>%s</CreateTime> <MsgType><![CDATA[voice]]></MsgType> <Video><MediaId>%s</MediaId><Title>%s</Title><Description>%s</Description></Video></xml> ";
 
 
	@Override
	public String text( String toUserName, String fromUserName, Integer createTime, String content) {
		String replyContent="您好！唛盟是一个多产品、多项目、多团队协同管理的整体解决方案！适用于大中型研发项目管理。唛盟首创众包+内研模式，极大缓解招人难、管人难、协调难、范围失控等问题，成本实现明显下降,效率实现大幅提升!";
		String replyText=String.format(ReplyText,toUserName,fromUserName,createTime,replyContent);
		 return replyText;
	}

	@Override
	public String image( String toUserName, String fromUserName, Integer createTime, String mediaId) {
		return String.format(ReplyImage, toUserName,fromUserName,createTime,mediaId);
 	}

	@Override
	public String voice( String toUserName, String fromUserName, Integer createTime, String mediaId) {
		return String.format(ReplyVoice, toUserName,fromUserName,createTime,mediaId);
	}

	@Override
	public String video( String toUserName, String fromUserName, Integer createTime, String mediaId, String title,
			String description) {
		return String.format(ReplyVideo, toUserName,fromUserName,createTime,mediaId,title,description);
	}

	@Override
	public String music( String toUserName, String fromUserName, Integer createTime, String mediaId, String title,
			String description, String musicUrl, String hqMusicUrl, String thumbMediaId) {
		return String.format(ReplyVideo, toUserName,fromUserName,createTime,mediaId,title,description,musicUrl,hqMusicUrl,thumbMediaId);

	}

	@Override
	public String news( String toUserName, String fromUserName, Integer createTime, List<Map<String, Object>> items) {
		
 
		StringBuffer itemSb=new StringBuffer(); 
		String itemFormat="<item><Title>%s</Title><Description>%s</Description><PicUrl>%s</PicUrl><Url>%s</Url></item>";
		
		for (Map<String, Object> map : items) {
			itemSb.append(String.format(itemFormat, map.get("archiveTitle"),map.get("archiveAbstract"),map.get("titleImgUrl"),map.get("url")));
		}
		String replyNews=String.format(ReplyNews,toUserName,fromUserName,createTime,items.size(),itemSb.toString());
		 return replyNews; 
	}

}
