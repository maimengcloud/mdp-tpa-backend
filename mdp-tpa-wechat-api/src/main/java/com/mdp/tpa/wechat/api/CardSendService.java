package com.mdp.tpa.wechat.api;

import java.util.List;
import java.util.Map;

import com.mdp.core.entity.Tips;

/**
 * 卡券投放接口
 * @author Administrator
 *
 */
public interface CardSendService {
	
	/**
	 * 根据openid列表投放卡券
	 * @param cardId 卡编号
	 * @param openids openid列表
	 * @return
	 * {
		   "errcode":0,
		   "errmsg":"send job submission success",
		   "msg_id":34182, 
		   "msg_data_id": 206227730
		}
	 */
	public Map<String,Object> sendByOpenids( String cardId,List<String> openids);
	
	/**
	 * 根据标签群发投放卡券
	 * @param cardId 卡编号 
	 * @param tag 标签分类
	 */
	public Tips sendByTag( String cardId,String tag);
	
	/**
	 * 开发者可调用该接口生成一张卡券二维码供用户扫码后添加卡券到卡包。</p>
	 * @param cardId 卡编号  
	 * @param expireSeconds 指定二维码的有效时间，范围是60 ~ 1800秒。不填默认为365天有效
	 * @param isUniqueCode 指定下发二维码，生成的二维码随机分配一个code，领取后不可再次扫描。填写true或false。默认false，注意填写该字段时，卡券须通过审核且库存不为0。
	 * @param outerId 领取场景值，用于领取渠道的数据统计，默认值为0，字段类型为整型，长度限制为60位数字。用户领取卡券后触发的事件推送中会带上此自定义场景值。
	 * @param outerStr outer_id字段升级版本，字符串类型，用户首次领卡时，会通过领取事件推送给商户；对于会员卡的二维码，用户每次扫码打开会员卡后点击任何url，会将该值拼入url中，方便开发者定位扫码来源
	 * 
	 * @return 
	 * {</p>
		 "errcode": 0,</p>
		 "errmsg": "ok",</p>
		 "ticket":      "gQHB8DoAAAAAAAAAASxodHRwOi8vd2VpeGluLnFxLmNvbS9xL0JIV3lhX3psZmlvSDZmWGVMMTZvAAIEsNnKVQMEIAMAAA==",//获取ticket后需调用换取二维码接口获取二维码图片，详情见字段说明。</p>
		 "expire_seconds": 1800,</p>
		 "url": "http://weixin.qq.com/q/BHWya_zlfioH6fXeL16o ",</p>
		 "show_qrcode_url": " https://mp.weixin.qq.com/cgi-bin/showqrcode?ticket=gQH98DoAAAAAAAAAASxodHRwOi8vd2VpeGluLnFxLmNvbS9xL0czVzRlSWpsamlyM2plWTNKVktvAAIE6SfgVQMEgDPhAQ%3D%3D" </p>
		}</p>
	 */
	public Map<String,Object> sendByQrcode( String cardId,int expireSeconds,boolean isUniqueCode,String outerId,String outerStr);
	
	/**
	 * 开发者可调用该接口生成一张卡券二维码供用户扫码后添加卡券到卡包。</p>
	 * @param cardId 卡编号  
	 * @param openid 指定领取者的openid，只有该用户能领取。bind_openid字段为true的卡券必须填写，非指定openid不必填写。
	 * @param expireSeconds 指定二维码的有效时间，范围是60 ~ 1800秒。不填默认为365天有效
	 * @param isUniqueCode 指定下发二维码，生成的二维码随机分配一个code，领取后不可再次扫描。填写true或false。默认false，注意填写该字段时，卡券须通过审核且库存不为0。
	 * @param outerId 领取场景值，用于领取渠道的数据统计，默认值为0，字段类型为整型，长度限制为60位数字。用户领取卡券后触发的事件推送中会带上此自定义场景值。
	 * @param outerStr outer_id字段升级版本，字符串类型，用户首次领卡时，会通过领取事件推送给商户；对于会员卡的二维码，用户每次扫码打开会员卡后点击任何url，会将该值拼入url中，方便开发者定位扫码来源
	 * 
	 * @return 
	 * {</p>
		 "errcode": 0,</p>
		 "errmsg": "ok",</p>
		 "ticket":      "gQHB8DoAAAAAAAAAASxodHRwOi8vd2VpeGluLnFxLmNvbS9xL0JIV3lhX3psZmlvSDZmWGVMMTZvAAIEsNnKVQMEIAMAAA==",//获取ticket后需调用换取二维码接口获取二维码图片，详情见字段说明。</p>
		 "expire_seconds": 1800,</p>
		 "url": "http://weixin.qq.com/q/BHWya_zlfioH6fXeL16o ",</p>
		 "show_qrcode_url": " https://mp.weixin.qq.com/cgi-bin/showqrcode?ticket=gQH98DoAAAAAAAAAASxodHRwOi8vd2VpeGluLnFxLmNvbS9xL0czVzRlSWpsamlyM2plWTNKVktvAAIE6SfgVQMEgDPhAQ%3D%3D" </p>
		}</p>
	 */
	public Map<String,Object> sendByQrcodeBindOpenid( String cardId,String openid,int expireSeconds,boolean isUniqueCode,String outerId,String outerStr);
	
	
	/**
	 * 开发者需调用该接口创建货架链接，用于卡券投放。创建货架时需填写投放路径的场景字段。</p>
	 * HTTP请求方式: POST</p>
     * URL:https://api.weixin.qq.com/card/landingpage/create?access_token=$TOKEN</p>
	 * @return 
	 * {</p>
		 "errcode": 0,</p>
		 "errmsg": "ok",</p>
		 "ticket":      "gQHB8DoAAAAAAAAAASxodHRwOi8vd2VpeGluLnFxLmNvbS9xL0JIV3lhX3psZmlvSDZmWGVMMTZvAAIEsNnKVQMEIAMAAA==",//获取ticket后需调用换取二维码接口获取二维码图片，详情见字段说明。</p>
		 "expire_seconds": 1800,</p>
		 "url": "http://weixin.qq.com/q/BHWya_zlfioH6fXeL16o ",</p>
		 "show_qrcode_url": " https://mp.weixin.qq.com/cgi-bin/showqrcode?ticket=gQH98DoAAAAAAAAAASxodHRwOi8vd2VpeGluLnFxLmNvbS9xL0czVzRlSWpsamlyM2plWTNKVktvAAIE6SfgVQMEgDPhAQ%3D%3D" </p>
		}</p>
	 */
	public void landingPageCreate( );
	
	/**
	 * 图文消息群发卡券</p>
	 * 支持开发者调用该接口获取卡券嵌入图文消息的标准格式代码，将返回代码填入上传图文素材接口中content字段，即可获取嵌入卡券的图文消息素材。</p>
	 * HTTP请求方式: POST</p>
     * URL:https://api.weixin.qq.com/card/mpnews/gethtml?access_token=TOKEN</p>
	 * @param cardId 卡编号
	 * @return
	 *  {
		"errcode":0,
		"errmsg":"ok",
		"content":"<iframeclass=\"res_iframecard_iframejs_editor_card\"data-src=\"http: \/\/mp.weixin.qq.com\/bizmall\/appmsgcard?action=show&biz=MjM5OTAwODk4MA%3D%3D&cardid=p1Pj9jnXTLf2nF7lccYScFUYqJ0&wechat_card_js=1#wechat_redirect\">"
		}
	 */
	public String mpNewsGethtml( String cardId);
	
	/**
	 * 通过图文消息发送卡券 
	 * @see MaterialService addNews
	 * @see MaterialService uploadimg
	 * @param title 标题
	 * @param thumbMediaId 图文消息的封面图片素材id（必须是永久mediaID） 必输
	 * @param author 作者 非必输
	 * @param digest 非必输  图文消息的摘要，仅有单图文消息才有摘要，多图文此处为空。如果本字段为没有填写，则默认抓取正文前64个字。
	 * @param showCoverPic  必输 是否显示封面，0为false，即不显示，1为true，即显示
	 * @param contentSourceUrl   必输 图文消息的原文地址，即点击“阅读原文”后的URL
	 * @return
	 * {
		  "media_id":MEDIA_ID
		}
	 */
	public String sendByNews( String cardId,String title,String thumbMediaId,String author,String digest,String showCoverPic,String contentSourceUrl);
	 
}
