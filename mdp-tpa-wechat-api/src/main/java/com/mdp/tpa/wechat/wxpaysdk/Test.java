package com.mdp.tpa.wechat.wxpaysdk;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.mdp.core.err.BizException;

public class Test {

	public static  Log log=LogFactory.getLog(Test.class);
	
	//qqkj
//	public static String appid="wxdc4792ea8b97cd10";
//	public static String mchId="1489796752";
//	public static String payKey="ffffffffgfhfgjhdfgsdfgsghdshgdfs";
	
	//keyun
	public static String appid="wxf57f8625a9f88554";
	public static String mchId="1499315292";
	public static String payKey="b73019a2720a401dbf207a5ec020821d";
	
	//123baby
//	public static String appid="wx260025f18c49b7c9";
//	public static String mchId="1526507141";
//	public static String payKey="f46903e6b134400897efdfe06f6f1df8";
	
	public static void main(String[] args) {
		WXPayConfig config;
		try {
			config = new MyWxPayConfig(appid,mchId,payKey); 
		} catch (Exception e) {
			 log.error("",e);
			 throw new BizException("mchId_cert_is_notfind_or_error", e.getMessage());
		}
   
        WXPay wxpay = new WXPay(config);

        Map<String, String> data = new HashMap<String, String>(); 
        data.put("out_trade_no", "2xjaz4h314");
        data.put("out_refund_no", "test002");
        data.put("total_fee", "30");
        data.put("refund_fee", "30");
    	
        Map<String, String> resp;
		try {
			resp = wxpay.refund(data);
			log.info(resp);
		} catch (Exception e) {
			log.error("",e);
			 throw new BizException("wxpay_refund_err", e.getMessage());
		}
	}

}
