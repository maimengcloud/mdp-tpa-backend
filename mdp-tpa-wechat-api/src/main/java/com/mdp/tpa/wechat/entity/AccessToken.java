package com.mdp.tpa.wechat.entity;

import java.io.Serializable;

public class AccessToken implements Serializable{


	private static final long serialVersionUID = 8841352663486854942L;
	
	String accessToken;
	/**
	 * access_token接口调用凭证超时时间，单位（秒）7200秒=120分=2小时
	 */
	Integer expiresIn;
	
	Long ctime;
	
	
	public String getAccessToken() {
		return accessToken;
	}
	public void setAccessToken(String accessToken) {
		this.accessToken = accessToken;
	}
	public Integer getExpiresIn() {
		return expiresIn;
	}
	public void setExpiresIn(Integer expiresIn) {
		this.expiresIn = expiresIn;
	}
	public Long getCtime() {
		return ctime;
	}
	public void setCtime(Long ctime) {
		this.ctime = ctime;
	}
	
	
	
	
}
