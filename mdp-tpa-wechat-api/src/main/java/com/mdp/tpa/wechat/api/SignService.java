package com.mdp.tpa.wechat.api;

import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.List;
import java.util.Map;
/**
 * 附录1-JS-SDK使用权限签名算法
 * 当页面需要调用http://res.wx.qq.com/open/js/jweixin-1.0.0.js时，首先需要对页面进行注册，签名验证
 * https://mp.weixin.qq.com/wiki
 * @author cyc
 *
 */ 
public interface SignService  { 
	 
	
	/***
	 * 向微信服务器申请访问jsapi前对参数进行签名
	 * @param url 需要签名到url,不带＃
	 * @return
	 */
	public Map<String,Object> signUrl(String url);

	
	/***
	 * 向微信服务器申请微信支付前对数据进行加密签名
	 * @param HashMap p 需要加密的参数
	 * @param String key 密钥
	 * @return
	 */
	public String signPayMd5(Map<String,Object> p,String key);
	
	/***
	 * 向微信服务器申请微信支付前对数据进行加密签名 HMAC-SHA256
	 * @param HashMap p 需要加密的参数
	 * @param String key 密钥
	 * @return
	 */
	public String paySignDesposit(Map<String,String> p,String key) throws UnsupportedEncodingException, NoSuchAlgorithmException, InvalidKeyException;
	
	/**
	 *  1.将 api_ticket、timestamp、card_id、code、openid、nonce_str的value值进行字符串的字典序排序。
		2.将所有参数字符串拼接成一个字符串进行sha1加密，得到signature。
		3.signature中的timestamp，nonce字段和card_ext中的timestamp，nonce_str字段必须保持一致。
		4.code=1434008071，timestamp=1404896688，card_id=pjZ8Yt1XGILfi-FUsewpnnolGgZk， api_ticket=ojZ8YtyVyr30HheH3CM73y7h4jJE ，nonce_str=123 则signature=sha1(12314048966881434008071ojZ8YtyVyr30HheH3CM73y7h4jJEpjZ8Yt1XGILfi-FUsewpnnolGgZk)=f137ab68b7f8112d20ee528ab6074564e2796250。
	 * @param cardList
	 * @param openid
	 * @return
	 */
	public List<Map<String,Object>> signForAddCard(List<String> cardList,String openid,String outerStr);
	
}
