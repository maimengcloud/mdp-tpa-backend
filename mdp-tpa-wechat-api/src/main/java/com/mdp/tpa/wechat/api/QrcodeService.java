package com.mdp.tpa.wechat.api;

import java.util.Map;

public interface QrcodeService {
	/** 
	 * 创建临时二维码ticket</p>
	 * 获取二维码ticket后，开发者可用ticket换取二维码图片。请注意，本接口无须登录态即可调用。</p>
	 * http请求方式: POST</p>
	 * URL: https://api.weixin.qq.com/cgi-bin/qrcode/create?access_token=TOKENPOST数据格式：json</p>
	 * 显示二维码图片请求：HTTP GET请求（请使用https协议）https://mp.weixin.qq.com/cgi-bin/showqrcode?ticket=TICKET提醒：TICKET记得进行UrlEncode</p>
	 * @param actionName  二维码类型，QR_SCENE为临时的整型参数值，QR_STR_SCENE为临时的字符串参数值，QR_LIMIT_SCENE为永久的整型参数值，QR_LIMIT_STR_SCENE为永久的字符串参数值
	 * @param expireSeconds 该二维码有效时间，以秒为单位。 最大不超过2592000（即30天），此字段如果不填，则默认有效期为30秒。
	 * @param sceneId 场景值ID，临时二维码时为32位非0整型，永久二维码时最大值为100000（目前参数只支持1--100000）
	 * @param sceneStr 场景值ID（字符串形式的ID），字符串类型，长度限制为1到64
	 * @return
	 * 正确的Json返回结果:</p>
		{</p>
			"ticket":"gQH47joAAAAAAAAAASxodHRwOi8vd2VpeGluLnFxLmNvbS9xL2taZ2Z3TVRtNzJXV1Brb3ZhYmJJAAIEZ23sUwMEmm3sUw==",</p>
			"expire_seconds":60,</p>
			"url":"http:\/\/weixin.qq.com\/q\/kZgfwMTm72WWPkovabbI"</p>
		}</p>
	 */
	public Map<String,Object> createTemporary( String actionName,String expireSeconds,String sceneId,String sceneStr);
	
	
	/** 
	 * 创建永久二维码ticket</p>
	 * 获取二维码ticket后，开发者可用ticket换取二维码图片。请注意，本接口无须登录态即可调用。</p>
	 * HTTP GET请求（请使用https协议）https://mp.weixin.qq.com/cgi-bin/showqrcode?ticket=TICKET提醒：TICKET记得进行UrlEncode</p>
	 * @param sceneStr 场景值ID（字符串形式的ID），字符串类型，长度限制为1到64
	 * @return
	 * 正确的Json返回结果:</p>
		{</p>
			"ticket":"gQH47joAAAAAAAAAASxodHRwOi8vd2VpeGluLnFxLmNvbS9xL2taZ2Z3TVRtNzJXV1Brb3ZhYmJJAAIEZ23sUwMEmm3sUw==",</p>
			"expire_seconds":60,</p>
			"url":"http:\/\/weixin.qq.com\/q\/kZgfwMTm72WWPkovabbI"</p>
		}</p>
	 */
	public Map<String,Object> createForever( String sceneStr);
	
}
