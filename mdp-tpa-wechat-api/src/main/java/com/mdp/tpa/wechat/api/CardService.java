package com.mdp.tpa.wechat.api;

import java.util.Date;
import java.util.Map;

import com.mdp.core.entity.Tips;
/**
 * 向微信拉取或者更新数据接口到第三方
 * 由第三方主动发起
 * @author Administrator
 *
 */
public interface CardService {
	/**
	 * 批量查询卡券列表<P/>
	 * HTTP请求方式: POST<P/>
	 * URL:https://api.weixin.qq.com/card/batchget?access_token=TOKEN<P/>
	 * 1.未传入筛选条件时，该接口默认传回该商户名下所有状态的卡券；<P/>
	 * 2.开发者可以请求之后调用查看卡券详情接口确定卡券状态；<P/>
	 * 返回：<P/>
	 * {<P/>
	 *	  "errcode":0,<P/>
	 *	  "errmsg":"ok",<P/>
	 *	  "card_id_list":["ph_gmt7cUVrlRk8swPwx7aDyF-pg"],<P/>
	 *	  "total_num":1<P/>
	 *	}<P/>
	 * @param offset 查询卡列表的起始偏移量，从0开始，即offset: 5是指从从列表里的第六个开始读取。
	 * @param count 需要查询的卡片的数量（数量最大50）。
	 * @param status 支持开发者拉出指定状态的卡券列表“CARD_STATUS_NOT_VERIFY”,待审核；“CARD_STATUS_VERIFY_FAIL”,审核失败；“CARD_STATUS_VERIFY_OK”，通过审核；“CARD_STATUS_DELETE”，卡券被商户删除；“CARD_STATUS_DISPATCH”，在公众平台投放过的卡券；
	 */
	public Map<String,Object> cardBatchGet( int offset,int count,String status);
	
	/**
	 * 创建会员卡卡券<P/>
	 * 创建卡券接口是微信卡券的基础接口，用于创建一类新的卡券，获取card_id，创建成功并通过审核后，商家可以通过文档提供的其他接口将卡券下发给用户，每次成功领取，库存数量相应扣除。<P/>
	 * HTTP请求方式: POST<P/>
	 * URL: https://api.weixin.qq.com/card/create?access_token=ACCESS_TOKEN<P/>
	 * @param cardInfo
	 * @return
	 */
	public Map<String,Object> createMemberCard( Map<String,Object> cardInfo);
	
	/**
	 * 创建优惠券<P/>
	 * 创建卡券接口是微信卡券的基础接口，用于创建一类新的卡券，获取card_id，创建成功并通过审核后，商家可以通过文档提供的其他接口将卡券下发给用户，每次成功领取，库存数量相应扣除。<P/>
	 * HTTP请求方式: POST<P/>
	 * URL: https://api.weixin.qq.com/card/create?access_token=ACCESS_TOKEN<P/>
	 * @param cardInfo
	 * @return
	 */
	public Map<String,Object> createCoupon( Map<String,Object> cardInfo);
		
	/**
	 * 查看卡券详情<P/>
	 * 开发者可以调用该接口查询某个card_id的创建信息、审核状态以及库存数量。<P/>
	 * HTTP请求方式: POST<P/>
	 * URL:https://api.weixin.qq.com/card/get?access_token=TOKEN<P/>
	 * 返回：<P/>
	 * {<P/>
		  "errcode": 0,<P/>
		  "errmsg": "ok",<P/>
		  "card": {<P/>
		    "card_type": "DISCOUNT",<P/>
		    "discount": {<P/>
		      "base_info": {<P/>
		        "id": "pbLatjnP97_F9PudzBARQhn7xR7A",<P/>
		        "logo_url": "http://mmbiz.qpic.cn/mmbiz/p98FjXy8LafmY25YclQ7vw5noBxeVH3DG5AKFR1ZsRgMgsvjll7EkUsZib00J964AEpTjkNXF2HorJHt5mtt45Q/0?wx_fmt=png",<P/>
		        "code_type": "CODE_TYPE_NONE",<P/>
		        "brand_name": "微信餐厅",<P/>
		        "title": "9折优惠券",<P/>
		        "date_info": {<P/>
		          "type": "DATE_TYPE_FIX_TERM",<P/>
		          "fixed_term": 30,<P/>
		          "fixed_begin_term": 0<P/>
		        },<P/>
		        "color": "#10AD61",<P/>
		        "notice": "到店使用",<P/>
		        "description": "",<P/>
		        "location_id_list": [<P/>
		          218384742,<P/>
		          402521653,<P/>
		          402521608<P/>
		        ],<P/>
		        "get_limit": 3,<P/>
		        "can_share": true,<P/>
		        "can_give_friend": true,<P/>
		        "status": "CARD_STATUS_VERIFY_OK",<P/>
		        "sku": {<P/>
		          "quantity": 100096,<P/>
		          "total_quantity": 100100<P/>
		        },<P/>
		        "create_time": 1457525546,<P/>
		        "update_time": 1457526240,<P/>
		        "area_code_list": []<P/>
		      },<P/>
		      "discount": 10,<P/>
		      "advanced_info": {<P/>
		        "time_limit": [<P/>
		          {<P/>
		            "type": "MONDAY"<P/>
		          },<P/>
		          {<P/>
		            "type": "TUESDAY"<P/>
		          }<P/>
		        ],<P/>
		        "text_image_list": [],<P/>
		        "business_service": [],<P/>
		        "consume_share_card_list": [],<P/>
		        "abstract": {<P/>
		          "abstract": "点击了解更多",<P/>
		          "icon_url_list": [<P/>
		            "http://mmbiz.qpic.cn/mmbiz/p98FjXy8LafiawSeJeqBzk8qC40iaKIwUPm4TSCelulzEbAywKr7tWjkd5vRjbmFloUFeThfwhwMUZIXmsCtJpyQ/0?wx_fmt=jpeg"<P/>
		          ]<P/>
		        },<P/>
		        "share_friends": false<P/>
		      }<P/>
		    }<P/>
		  }<P/>
		}<P/>
	 * @param cardId 
	 */
	public Map<String,Object> cardGet( String cardId);
	

	
	/**
	 * 删除卡券接口<P/>
	 * 删除卡券接口允许商户删除任意一类卡券。删除卡券后，该卡券对应已生成的领取用二维码、添加到卡包JS API均会失效。 注意：如用户在商家删除卡券前已领取一张或多张该卡券依旧有效。即删除卡券不能删除已被用户领取，保存在微信客户端中的卡券。。<P/>
	 * HTTP请求方式: POST <P/>
	 * URL:https://api.weixin.qq.com/card/delete?access_token=TOKEN<P/>
	 * 返回：<P/>
	 *  {<P/>
		"errcode":0,<P/>
		"errmsg":"ok"<P/>
		}<P/>
     * @param cardId
	 */
	public Tips cardDelete( String cardId);
	
	/**
	 * 更改卡券信息接口<P/>
	 * 支持更新所有卡券类型的部分通用字段及特殊卡券（会员卡、飞机票、电影票、会议门票）中特定字段的信息。<P/>
	 * HTTP请求方式: POST<P/>
	 * URL:https://api.weixin.qq.com/card/update?access_token=TOKEN<P/>
	 * 1. 请开发者注意需要重新提审的字段，开发者调用更新接口时，若传入了提审字段则卡券需要重新进入审核状态；<P/>
	 * 2. 接口更新方式为覆盖更新：即开发者只需传入需要更改的字段，其他字段无需填入，否则可能导致卡券重新提审；<P/>
     * 3. 若开发者置空某些字段，可直接在更新时传“”（空）；<P/>
     * 4. 调用该接口后更改卡券信息后，请务必调用查看卡券详情接口验证是否已成功更改，<P/>
     * 5.未列出的字段不支持修改更新。<P/>
     * 返回：<P/>
     * {<P/>
		   "errcode":0,<P/>
		   "errmsg":"ok",<P/>
		   "send_check":false   //是否提交审核，false为修改后不会重新提审，true为修改字段后重新提审，该卡券的状态变为审核中。<P/>
		}<P/>
     * @param cardId
     * @param cardInfo 需要上送的各种数据
     * 
	 */
	public Tips cardUpdate( String cardId,Map<String,Object> cardInfo);
	
	/**
	 * 修改库存接口<P/>
	 * 调用修改库存接口增减某张卡券的库存<P/>
	 * HTTP请求方式: POST<P/>
	 * URL:https://api.weixin.qq.com/card/modifystock?access_token=TOKEN<P/>
	 * @param cardId
	 * @param increaseStockValue
	 * @param reduceStockValue
	 */
	public Tips modifyStock( String cardId,int increaseStockValue,int reduceStockValue);
	
	/**
	 * 获取用户已领取卡券接口<P/>
	 * 用于获取用户卡包里的，属于该appid下所有可用卡券，包括正常状态和异常状态。<P/>
	 * HTTP请求方式: POST<P/>
	 * URL:https://api.weixin.qq.com/card/user/getcardlist?access_token=TOKEN<P/>
	 * 返回：<P/>
	 *  {<P/>
		"errcode":0,<P/>
		"errmsg":"ok",<P/>
		"card_list": [<P/>
		      {"code": "xxx1434079154", "card_id": "xxxxxxxxxx"},<P/>
		      {"code": "xxx1434079155", "card_id": "xxxxxxxxxx"}<P/>
		      ]，<P/>
		       "has_share_card": true<P/>
		}<P/>
		@param cardId
		@param openid
	 */
	public Map<String,Object> userGetCardList( String cardId,String openid);
	

	
	
	/**
	 * 拉取卡券概况数据接口<P/>
	 * 支持调用该接口拉取本商户的总体数据情况，包括时间区间内的各指标总量。<P/>
	 * HTTP请求方式: POST <P/>
	 * URL:https://api.weixin.qq.com/datacube/getcardbizuininfo?access_token=ACCESS_TOKEN <P/>
	 * 1. 查询时间区间需<=62天，否则报错{errcode: 61501，errmsg: "date range error"}；<P/>
	 * 2. 传入时间格式需严格参照示例填写”2015-06-15”，否则报错{errcode":61500,"errmsg":"date format error"}<P/>
     * 3. 该接口只能拉取非当天的数据，不能拉取当天的卡券数据，否则报错。<P/>
	 * 返回：<P/>
		{<P/>
		    "list": [<P/>
		       {<P/>
		           "ref_date": "2015-06-23",<P/>
		           "view_cnt": 1,<P/>
		           "view_user": 1,<P/>
		           "receive_cnt": 1,<P/>
		           "receive_user": 1,<P/>
		           "verify_cnt": 0,<P/>
		           "verify_user": 0,<P/>
		           "given_cnt": 0,<P/>
		           "given_user": 0,<P/>
		           "expire_cnt": 0,<P/>
		           "expire_user": 0<P/>
		       }<P/>
		   ] <P/>
		}<P/>
     * @param beginDate  2015-06-15 请开发者按示例格式填写日期，否则会报错date format error
     * @param endDate 2015-06-30 请开发者按示例格式填写日期，否则会报错date format error
     * @param condSource 卡券来源，0为公众平台创建的卡券数据、1是API创建的卡券数据
	 */
	public Map<String,Object> getCardBizuinInfo( Date beginDate,Date endDate,int condSource);
	
	
	/**
	 * 获取免费券数据接口<P/>
	 * 支持开发者调用该接口拉取免费券（优惠券、团购券、折扣券、礼品券）在固定时间区间内的相关数据。<P/>
	 * HTTP请求方式: POST <P/>
	 * URL:https://api.weixin.qq.com/datacube/getcardcardinfo?access_token=ACCESS_TOKEN <P/>
	 * 1. 查询时间区间需<=62天，否则报错{errcode: 61501，errmsg: "date range error"}；<P/>
	 * 2. 传入时间格式需严格参照示例填写”2015-06-15”，否则报错{errcode":61500,"errmsg":"date format error"}<P/>
     * 3. 该接口只能拉取非当天的数据，不能拉取当天的卡券数据，否则报错。<P/>
	 * 返回：<P/>
		{<P/>
		    "list": [<P/>
		       {<P/>
		           "ref_date": "2015-06-23",<P/>
		           "card_id": "po8pktyDLmakNY2fn2VyhkiEPqGE",<P/>
		           "card_type":3,<P/>
		           "view_cnt": 1,<P/>
		           "view_user": 1,<P/>
		           "receive_cnt": 1,<P/>
		           "receive_user": 1,<P/>
		           "verify_cnt": 0,<P/>
		           "verify_user": 0,<P/>
		           "given_cnt": 0,<P/>
		           "given_user": 0,<P/>
		           "expire_cnt": 0,<P/>
		           "expire_user": 0<P/>
		       }<P/>
		   ]<P/>
		}<P/>
	 * @param cardId 卡券ID。填写后，指定拉出该卡券的相关数据。
     * @param beginDate  2015-06-15 请开发者按示例格式填写日期，否则会报错date format error
     * @param endDate 2015-06-30 请开发者按示例格式填写日期，否则会报错date format error
     * @param condSource 卡券来源，0为公众平台创建的卡券数据、1是API创建的卡券数据
	 */
	public Map<String,Object> getCardInfo( String cardId,Date beginDate,Date endDate,int condSource);
	
	
	/**
	 * 拉取卡券概况数据接口<P/>
	 * 支持调用该接口拉取本商户的总体数据情况，包括时间区间内的各指标总量。<P/>
	 * HTTP请求方式: POST<P/>
	 * URL:https://api.weixin.qq.com/datacube/getcardmembercardinfo?access_token=ACCESS_TOKEN <P/>
	 * 1. 查询时间区间需<=62天，否则报错{errcode: 61501，errmsg: "date range error"}；<P/>
	 * 2. 传入时间格式需严格参照示例填写”2015-06-15”，否则报错{errcode":61500,"errmsg":"date format error"}<P/>
     * 3. 该接口只能拉取非当天的数据，不能拉取当天的卡券数据，否则报错。<P/>
	 * 返回：<P/>
		{<P/>
		   "list": [<P/>
		       {<P/>
		           "ref_date": "2015-06-23",<P/>
		           "view_cnt": 0,<P/>
		              "view_user": 0,<P/>
		              "receive_cnt": 0,<P/>
		              "receive_user": 0,<P/>
		              "active_user": 0,<P/>
		              "verify_cnt": 0,<P/>
		              "verify_user": 0,<P/>
		              "total_user": 86,<P/>
		              "total_receive_user": 95<P/>
		   ]<P/>
		}<P/>
     * @param beginDate  2015-06-15 请开发者按示例格式填写日期，否则会报错date format error
     * @param endDate 2015-06-30 请开发者按示例格式填写日期，否则会报错date format error
     * @param condSource 卡券来源，0为公众平台创建的卡券数据、1是API创建的卡券数据
	 */
	public Map<String,Object> getMemberCardInfo( Date beginDate,Date endDate,int condSource);
	
	
	/**
	 * 拉取单张会员卡数据接口<P/>
	 * 支持开发者调用该接口拉取API创建的会员卡数据情况<P/>
	 * HTTP请求方式: POST <P/>
	 * URL:https://api.weixin.qq.com/datacube/getcardmembercarddetail?access_token=ACCESS_TOKEN <P/>
	 * 1. 查询时间区间需<=62天，否则报错{errcode: 61501，errmsg: "date range error"}；<P/>
	 * 2. 传入时间格式需严格参照示例填写”2015-06-15”，否则报错{errcode":61500,"errmsg":"date format error"}<P/>
     * 3. 该接口只能拉取非当天的数据，不能拉取当天的卡券数据，否则报错。<P/>
	 * 返回：<P/>
		{<P/>
		    "list": [<P/>
		       {<P/>
		           "ref_date": "2015-06-23",<P/>
		           "card_id": "po8pktyDLmakNY2fn2VyhkiEPqGE",<P/>
		           "card_type":3,<P/>
		           "view_cnt": 1,<P/>
		           "view_user": 1,<P/>
		           "receive_cnt": 1,<P/>
		           "receive_user": 1,<P/>
		           "verify_cnt": 0,<P/>
		           "verify_user": 0,<P/>
		           "given_cnt": 0,<P/>
		           "given_user": 0,<P/>
		           "expire_cnt": 0,<P/>
		           "expire_user": 0<P/>
		       }<P/>
		   ]<P/>
		}<P/>
	 * @param cardId 卡券ID。填写后，指定拉出该卡券的相关数据。非必输
     * @param beginDate  2015-06-15 请开发者按示例格式填写日期，否则会报错date format error
     * @param endDate 2015-06-30 请开发者按示例格式填写日期，否则会报错date format error 
	 */
	public Map<String,Object> getMemberCardDetail( String cardId,Date beginDate,Date endDate);
	
	
	
	public Map<String,Object> uploadimg( String path);
	
	/**
	 * 通过下载网络文件，上传到微信
	 * @param accessToken
	 * @param url
	 * @return
	 */
	public Map<String,Object> uploadimgByUrl( String url);
}
