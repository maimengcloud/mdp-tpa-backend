package com.mdp.tpa.wechat.api.cache;

import com.mdp.core.api.CacheService;
import com.mdp.tpa.wechat.entity.Oauth2AccessToken;

public interface Oauth2TokenCache  extends CacheService<Oauth2AccessToken> {
	
}
