package com.mdp.tpa.wechat.api;

import java.util.List;
import java.util.Map;

public interface WxUserService {
	/**
	 * 网页受权
	 * 第四步：拉取用户信息(需scope为 snsapi_userinfo)
	 * 如果网页授权作用域为snsapi_userinfo，则此时开发者可以通过access_token和openid拉取用户信息
	 * @see AccessTokenService .getSnsOauth2AccessToken
	 * @param openid
	 * @return 
	 * 正确时返回的JSON数据包如下：
		{    "openid":" OPENID",  
			 "nickname": NICKNAME,   
			 "sex":"1",   
			 "province":"PROVINCE"   
			 "city":"CITY",   
			 "country":"COUNTRY",    
			 "headimgurl":    "http://wx.qlogo.cn/mmopen/g3MonUZtNHkdmzicIlibx6iaFqAc56vxLSUfpb6n5WKSYVY0ChQKkiaJSgQ1dZuTOgvLLrhJbERQQ4eMsv84eavHiaiceqxibJxCfHe/46",  
			 "privilege":[ "PRIVILEGE1" "PRIVILEGE2"],    
			 "unionid": "o6_bmasdasdsad6_2sgVt7hMZOPfL" 
		} 
		错误时：
		{"errcode":40003,"errmsg":" invalid openid "} 
	 */
	public Map<String,Object> getSnsUserinfo( String openid);
	
	/**
	 * 通过普通的accessToken获取用户基本信息（包括UnionID机制）
	 * @param openid
	 * @return 
	 * 正确时返回的JSON数据包如下：
		{
		   "subscribe": 1, 
		   "openid": "o6_bmjrPTlm6_2sgVt7hMZOPfL2M", 
		   "nickname": "Band", 
		   "sex": 1, 
		   "language": "zh_CN", 
		   "city": "广州", 
		   "province": "广东", 
		   "country": "中国", 
		   "headimgurl":  "http://wx.qlogo.cn/mmopen/g3MonUZtNHkdmzicIlibx6iaFqAc56vxLSUfpb6n5WKSYVY0ChQKkiaJSgQ1dZuTOgvLLrhJbERQQ4
		eMsv84eavHiaiceqxibJxCfHe/0",
		  "subscribe_time": 1382694957,
		  "unionid": " o6_bmasdasdsad6_2sgVt7hMZOPfL"
		  "remark": "",
		  "groupid": 0,
		  "tagid_list":[128,2]
		}
		错误时：
		{"errcode":40003,"errmsg":" invalid openid "} 
	 */
	public Map<String,Object> getUserinfo( String openid);
	
	/**
	 * 批量获取用户基本信息
		开发者可通过该接口来批量获取用户基本信息。最多支持一次拉取100条。
		接口调用请求说明
		http请求方式: POST
		https://api.weixin.qq.com/cgi-bin/user/info/batchget?access_token=ACCESS_TOKEN
	 * @param openids
	 * @return
	 * {
		  "user_info_list": [
		      {
		          "subscribe": 1, 
		          "openid": "otvxTs4dckWG7imySrJd6jSi0CWE", 
		          "nickname": "iWithery", 
		          "sex": 1, 
		          "language": "zh_CN", 
		          "city": "揭阳", 
		          "province": "广东", 
		          "country": "中国", 
		          "headimgurl": "http://wx.qlogo.cn/mmopen/xbIQx1GRqdvyqkMMhEaGOX802l1CyqMJNgUzKP8MeAeHFicRDSnZH7FY4XB7p8XHXIf6uJA2SCun
		TPicGKezDC4saKISzRj3nz/0",
		          "subscribe_time": 1434093047, 
		          "unionid": "oR5GjjgEhCMJFyzaVZdrxZ2zRRF4", 
		          "remark": "", 
		          "groupid": 0,
		          "tagid_list":[128,2]
		      }, 
		      {
		          "subscribe": 0, 
		          "openid": "otvxTs_JZ6SEiP0imdhpi50fuSZg", 
		          "unionid": "oR5GjjjrbqBZbrnPwwmSxFukE41U", 
		      }
		  ]
		}
	 */
	public Map<String,Object> batchgetUserInfo( List<String> openids);
	
	
	/**
	 * 公众号可通过本接口来获取帐号的关注者列表，关注者列表由一串OpenID（加密后的微信号，每个用户对每个公众号的OpenID是唯一的）组成。一次拉取调用最多拉取10000个关注者的OpenID，可以通过多次拉取的方式来满足需求。
	 * http请求方式: GET（请使用https协议）
       https://api.weixin.qq.com/cgi-bin/user/get?access_token=ACCESS_TOKEN&next_openid=NEXT_OPENID 
	 * @param nextOpenid
	 * @return
	 * {"total":2,
		"count":2,
		"data":{
		"openid":["OPENID1","OPENID2"]},
		"next_openid":"NEXT_OPENID"
		}
	 */
	public Map<String,Object> getUserOpenidList( String nextOpenid);
}
