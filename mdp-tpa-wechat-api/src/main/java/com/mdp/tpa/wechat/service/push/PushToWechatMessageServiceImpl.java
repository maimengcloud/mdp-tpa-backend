package com.mdp.tpa.wechat.service.push;

import com.mdp.core.entity.Tips;
import com.mdp.core.utils.BaseUtils;
import com.mdp.tpa.wechat.HttpRequestUtil;
import com.mdp.tpa.wechat.api.AccessTokenService;
import com.mdp.tpa.wechat.api.ApiUrls;
import com.mdp.tpa.wechat.api.push.PushToWechatMessageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.HashMap;
import java.util.Map;

@Service
public class PushToWechatMessageServiceImpl implements PushToWechatMessageService {

    @Autowired
    AccessTokenService tokenService;

    /**
     *
     * 发送订阅通知
     * POST
     * page 和 miniprogram 同时不填，无跳转；page 和 miniprogram 同时填写，优先跳转小程序；
     * @param touser 	接收者（用户）的 openid
     * @param templateId 所需下发的订阅模板id
     * @param page 跳转网页时填写
     * @param miniprogram 跳转小程序时填写，格式如{ "appid": "pagepath": { "value": any } }
     * @param data 模板内容，格式形如 { "key1": { "value": any }, "key2": { "value": any } }
     * @return tips
     * 其中微信返回格式：{ errcode:Number,errmsg:''}
     * 40003	touser字段openid为空或者不正确
     * 40037	订阅模板id为空不正确
     * 43101	用户拒绝接受消息，如果用户之前曾经订阅过，则表示用户取消了订阅关系
     * 47003	模板参数不准确，可能为空或者不满足规则，errmsg会提示具体是哪个字段出错
     * 41030	page路径不正确
     *
     * 举例：
     * 发送数据到微信：
     *{
     *   "touser": "OPENID",
     *   "template_id": "TEMPLATEID",
     *   "page": "mp.weixin.qq.com",
     *   "miniprogram":{
     *              "appid":"APPID",
     *              "pagepath":"index?foo=bar"
     *   },
     *   "data": {
     *       "name1": {
     *           "value": "广州腾讯科技有限公司"
     *       },
     *       "thing8": {
     *           "value": "广州腾讯科技有限公司"
     *       },
     *        "time7": {
     *           "value": "2019年8月8日"
     *       }
     *      }
     * }
     * 微信返回：
     * {
     *    "errcode": 0,
     *    "errmsg": "ok",
     * }
     */
    @Override
    public Tips bizsend( String touser, String templateId, String page, Map<String, Object> miniprogram, Map<String, Object> data) {
        Tips tips = new Tips("成功");
        String url=String.format(ApiUrls.MSG_SUBSCRIBE_BIZSEND, tokenService.getToken());
        Map<String,Object> params=new HashMap<>();
        params.put("touser",touser);
        params.put("template_id",templateId);
        if(StringUtils.hasText(page)){
            params.put("page",params);
        }
        if(miniprogram!=null && !miniprogram.isEmpty()){
            params.put("miniprogram",miniprogram);
        }
        params.put("data",data);

        Map<String, Object> result= HttpRequestUtil.sendPostJson(url,params);
        tips=BaseUtils.mapToTips(result);
        return tips;
    }
}
