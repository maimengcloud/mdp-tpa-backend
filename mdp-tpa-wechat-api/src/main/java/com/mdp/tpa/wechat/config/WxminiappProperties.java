package com.mdp.tpa.wechat.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

import java.util.Properties;

@Data
@ConfigurationProperties(prefix = "mdp.apps.wxminiapp")
public class WxminiappProperties extends AppProperties {


	
}