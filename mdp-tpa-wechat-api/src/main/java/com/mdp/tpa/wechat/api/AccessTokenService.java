package com.mdp.tpa.wechat.api;

import com.mdp.tpa.wechat.entity.AccessToken;

public interface AccessTokenService {

	public String getToken();
	
	/**
	 * 一般的访问api的accessToken
	 * https请求方式: GET
	 * https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid=APPID&secret=APPSECRET
	 * @return AccessToken 错误时返回 null
	 * 接口返回数据：
	 * {"access_token":"ACCESS_TOKEN","expires_in":7200} 
	 * 错误时：
	 * {"errcode":40013,"errmsg":"invalid appid"}
	 */
	public AccessToken getAccessToken();
	
	/**
	 * 一般的访问api的accessToken
	 * https请求方式: GET
	 * https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid=APPID&secret=APPSECRET
	 * @param appid
	 * @param secret
	 * @return AccessToken 错误时返回 null
	 * 接口返回数据：
	 * {"access_token":"ACCESS_TOKEN","expires_in":7200} 
	 * 接口返回错误时：
	 * {"errcode":40013,"errmsg":"invalid appid"}
	 */
	public AccessToken getAccessToken(String appid,String secret);
	 
	 
}
