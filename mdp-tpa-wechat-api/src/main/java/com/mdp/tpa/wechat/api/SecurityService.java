package com.mdp.tpa.wechat.api;

import java.util.Map;

import qq.weixin.mp.aes.AesException;

public interface SecurityService { 
	
	/****
	 * 首次服务器验证 微信发过来的信息
	 * {timestamp=1478231323, echostr=2293813022107307999, nonce=320121891, signature=ef7a44ee63872eef9aeb01e5a26f7ed13f216eef}
	 */
	public String verifyUrl( String msgSignature, String timeStamp, String nonce, String echoStr);
	
	/**
	 * 检验消息的真实性，并且获取解密后的明文.
	 * <ol>
	 * 	<li>利用收到的密文生成安全签名，进行签名验证</li>
	 * 	<li>若验证通过，则提取xml中的加密消息</li>
	 * 	<li>对消息进行解密</li>
	 * </ol>
	 * 
	 * @param msgSignature 签名串，对应URL参数的msg_signature
	 * @param timeStamp 时间戳，对应URL参数的timestamp
	 * @param nonce 随机串，对应URL参数的nonce
	 * @param postData 密文，对应POST请求的数据
	 * 
	 * @return 解密后的原文
	 * @throws AesException 执行失败，请查看该异常的错误码和具体的错误信息
	 */
	public String decryptMsg( String msgSignature, String timeStamp, String nonce, String postData);
	
	/**
	 * 解密客户端获取得到的用户手机号码加密数据，返回正确的明文字符串
	 * @return
	 * {
		    "phoneNumber": "13580006666",  
		    "purePhoneNumber": "13580006666", 
		    "countryCode": "86",
		    "watermark":
		    {
		        "appid":"APPID",
		        "timestamp":TIMESTAMP
		    }
		}
	 */
	public Map<String,Object> decryptPhoneNo(String sessionKey,String encryptedData,String iv);
	
	
	public void clearCache( ) ;
}
