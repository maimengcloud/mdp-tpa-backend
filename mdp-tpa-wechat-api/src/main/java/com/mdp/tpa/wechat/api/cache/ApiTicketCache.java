package com.mdp.tpa.wechat.api.cache;

import com.mdp.core.api.CacheService;
import com.mdp.tpa.wechat.entity.ApiTicket;

public interface ApiTicketCache extends CacheService<ApiTicket> {

}
