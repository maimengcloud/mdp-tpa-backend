package com.mdp.tpa.wechat.util;

import java.util.Map;

import com.mdp.core.entity.Tips;
import com.mdp.core.utils.NumberUtil;

public class WxResultParseUtil {
	
	public static Tips mapToTips(Map<String,Object> result) {
		Tips tips=new Tips("ok");
		Integer errcode=NumberUtil.getInteger(result.get("errcode"));
		if(errcode==0) {
			tips.setOkMsg((String) result.get("errmsg"));
		}else {
			tips.setErrMsg(errcode.toString(), "errcode", (String) result.get("errmsg"));
		}
		return tips;
	}
}
