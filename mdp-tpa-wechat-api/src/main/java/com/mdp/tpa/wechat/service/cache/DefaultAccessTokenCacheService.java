package com.mdp.tpa.wechat.service.cache;

import com.mdp.tpa.wechat.api.cache.AccessTokenCache;
import com.mdp.tpa.wechat.entity.AccessToken;
import com.mdp.core.service.DefaultCacheService;

public class DefaultAccessTokenCacheService extends DefaultCacheService<AccessToken> implements AccessTokenCache {

}
