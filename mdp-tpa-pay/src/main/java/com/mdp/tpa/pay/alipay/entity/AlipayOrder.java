package com.mdp.tpa.pay.alipay.entity;

public class AlipayOrder {
    /**
     * 商户订单编号
     */
    String outTradeNo;

    /**
     * 支付成功跳转页面
     */
    String returnUrl;

    public String getOutTradeNo() {
        return outTradeNo;
    }

    public void setOutTradeNo(String outTradeNo) {
        this.outTradeNo = outTradeNo;
    }

    public String getReturnUrl() {
        return returnUrl;
    }

    public void setReturnUrl(String returnUrl) {
        this.returnUrl = returnUrl;
    }
}
