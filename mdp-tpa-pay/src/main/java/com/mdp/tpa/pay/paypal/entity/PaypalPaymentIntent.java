package com.mdp.tpa.pay.paypal.entity;

/**
 * 付款类型
 *  sale：直接付款
 *  authorize：授权付款
 *  order：订单付款
 */
public enum PaypalPaymentIntent {
    sale,
    authorize,
    order
}