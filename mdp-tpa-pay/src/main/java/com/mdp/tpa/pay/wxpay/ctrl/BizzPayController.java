package com.mdp.tpa.pay.wxpay.ctrl;


import com.mdp.core.entity.LangTips;
import com.mdp.core.entity.Result;
import com.mdp.core.entity.Tips;
import com.mdp.core.err.BizException;
import com.mdp.tpa.pay.cm.client.AcPaymentPushService;
import com.mdp.tpa.pay.wxpay.entity.BizzPayVo;
import com.mdp.tpa.wechat.api.PayService;
import com.mdp.tpa.wechat.wxpaysdk.WXPayConfig;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ResponseHeader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

@RestController("tpa.pay.BizzPayController")
@RequestMapping(value="/**/pay/wxpay")
@Api(tags = "微信企业付款api v2")
public class BizzPayController {

    @Autowired
    WXPayConfig wxPayConfig;

    @Autowired
    PayService payService;

    @Autowired
    AcPaymentPushService acPaymentPushService;

    private Logger log= LoggerFactory.getLogger(BizzPayController.class);



    @ApiOperation("企业通过微信付款给个人")
    @RequestMapping(value="/bizzPay",method= RequestMethod.POST)
    public Result bizzPay(@RequestBody  BizzPayVo vo){
        log.info("企业通过微信付款 v2");
        Tips tips = new Tips("成功");
        Map<String,Object> m=new HashMap<>();
        try {
            tips=payService.businessPayment(wxPayConfig,vo.getOpenid(),vo.getPayAt(),vo.getDesc());
            return Result.build(LangTips.fromTips(tips));
        }catch (BizException be){
            return Result.error(be);
        }catch (Exception e){
            return Result.error(e);
        }
    }

}
