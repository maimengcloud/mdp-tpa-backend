package com.mdp.tpa.pay.cm.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

/**
 * 组织 com  顶级模块 ac 大模块 tpa  小模块 <br> 
 * 实体 PaymentSeq所有属性名: <br>
 *	"id","交易流水号","gsn","全局交易流水号如果前端没有上传，则自己创建","mechId","付款方商户编号","accountId","付款方账户","userid","付款方客户编号","username","付款方客户名称","outType","付款类型","incomeUserid","收款方客户编号","incomeUsername","收款方客户名称","incomeAccountId","收款方账户","orderId","定单编号(具体的需要关联的业务编号)","remarks","备注","status","状态0待付款1已付款待记账2已记账待结算3已结算待记账4已记账待清算","createTime","创建时间","payStatus","付款状态0待付款1已付款2取消3失败","incomeMechId","收款方机构编号","amountType","币种CNY","payOpenid","付款方第三方账号","bankType","收款银行类型","bankCode","收款银行编号","payType","付款类型","prepayId","第三方预付订单号","deptid","部门编号","payAt","实际付款金额","orderAt","订单总金额","branchId","付款方云用户机构编号","bizType","业务类型0","refundStatus","退款状态","refundId","退款单号","refundDate","退款时间","acctPrjType","结算项目类型platform/shop/location","unfreezeLockNo","批量解冻批次号","unfreezeStatus","解冻状态0待解冻1已解冻2解冻失败3解冻执行中","exeUnfreezeTime","执行解冻时间","companyId","付款方记账机构","incomeCompanyId","收款方记账机构","incomeBranchId","收款方userid对应的机构编号","payId","付款流水号(内部生成，传给第三方原样传回，如果不正确，不允许更新数据库，防止作弊)","tranId","第三方付款事务号(由第三方系统产生)","bankCardCode","收款银行卡号","payTime","付款时间","otype","订单类型0-电商商城商品，1-应用模块使用购买，2-vip购买会员，3-任务相关的保证金、佣金、分享赚佣金、加急热搜金额等、4-服务商付款服务保障押金","orderStatus","订单状态","extParams","扩展参数JSON字符串","ip","付款方ip地址","name","订单名称","prepayTime","第三方下单时间";<br>
 * 当前主键(包括多主键):<br>
 *	id;<br>
 */
 @Data
@ApiModel(description="客户付款交易流水")
public class PaymentSeq implements java.io.Serializable {
	
	private static final long serialVersionUID = 1L;
	
	@ApiModelProperty(notes="交易流水号,主键",allowEmptyValue=true,example="",allowableValues="")
	String id;
  	
	
	@ApiModelProperty(notes="全局交易流水号如果前端没有上传，则自己创建",allowEmptyValue=true,example="",allowableValues="")
	String gsn;
	
	@ApiModelProperty(notes="付款方商户编号",allowEmptyValue=true,example="",allowableValues="")
	String mechId;
	
	@ApiModelProperty(notes="付款方账户",allowEmptyValue=true,example="",allowableValues="")
	String accountId;
	
	@ApiModelProperty(notes="付款方客户编号",allowEmptyValue=true,example="",allowableValues="")
	String userid;
	
	@ApiModelProperty(notes="付款方客户名称",allowEmptyValue=true,example="",allowableValues="")
	String username;
	
	@ApiModelProperty(notes="付款类型",allowEmptyValue=true,example="",allowableValues="")
	String outType;
	
	@ApiModelProperty(notes="收款方客户编号",allowEmptyValue=true,example="",allowableValues="")
	String incomeUserid;
	
	@ApiModelProperty(notes="收款方客户名称",allowEmptyValue=true,example="",allowableValues="")
	String incomeUsername;
	
	@ApiModelProperty(notes="收款方账户",allowEmptyValue=true,example="",allowableValues="")
	String incomeAccountId;
	
	@ApiModelProperty(notes="定单编号(具体的需要关联的业务编号)",allowEmptyValue=true,example="",allowableValues="")
	String orderId;
	
	@ApiModelProperty(notes="备注",allowEmptyValue=true,example="",allowableValues="")
	String remarks;
	
	@ApiModelProperty(notes="状态0待付款1已付款待记账2已记账待结算3已结算待记账4已记账待清算",allowEmptyValue=true,example="",allowableValues="")
	String status;
	
	@ApiModelProperty(notes="创建时间",allowEmptyValue=true,example="",allowableValues="")
	Date createTime;
	
	@ApiModelProperty(notes="付款状态0待付款1已付款2取消3失败",allowEmptyValue=true,example="",allowableValues="")
	String payStatus;
	
	@ApiModelProperty(notes="收款方机构编号",allowEmptyValue=true,example="",allowableValues="")
	String incomeMechId;
	
	@ApiModelProperty(notes="币种CNY",allowEmptyValue=true,example="",allowableValues="")
	String amountType;
	
	@ApiModelProperty(notes="付款方第三方账号",allowEmptyValue=true,example="",allowableValues="")
	String payOpenid;
	
	@ApiModelProperty(notes="收款银行类型",allowEmptyValue=true,example="",allowableValues="")
	String bankType;
	
	@ApiModelProperty(notes="收款银行编号",allowEmptyValue=true,example="",allowableValues="")
	String bankCode;
	
	@ApiModelProperty(notes="付款类型",allowEmptyValue=true,example="",allowableValues="")
	String payType;
	
	@ApiModelProperty(notes="第三方预付订单号",allowEmptyValue=true,example="",allowableValues="")
	String prepayId;
	
	@ApiModelProperty(notes="部门编号",allowEmptyValue=true,example="",allowableValues="")
	String deptid;
	
	@ApiModelProperty(notes="实际付款金额",allowEmptyValue=true,example="",allowableValues="")
	BigDecimal payAt;
	
	@ApiModelProperty(notes="订单总金额",allowEmptyValue=true,example="",allowableValues="")
	BigDecimal orderAt;
	
	@ApiModelProperty(notes="付款方云用户机构编号",allowEmptyValue=true,example="",allowableValues="")
	String branchId;
	
	@ApiModelProperty(notes="业务类型0",allowEmptyValue=true,example="",allowableValues="")
	String bizType;
	
	@ApiModelProperty(notes="退款状态",allowEmptyValue=true,example="",allowableValues="")
	String refundStatus;
	
	@ApiModelProperty(notes="退款单号",allowEmptyValue=true,example="",allowableValues="")
	String refundId;
	
	@ApiModelProperty(notes="退款时间",allowEmptyValue=true,example="",allowableValues="")
	Date refundDate;
	
	@ApiModelProperty(notes="结算项目类型platform/shop/location",allowEmptyValue=true,example="",allowableValues="")
	String acctPrjType;
	
	@ApiModelProperty(notes="批量解冻批次号",allowEmptyValue=true,example="",allowableValues="")
	String unfreezeLockNo;
	
	@ApiModelProperty(notes="解冻状态0待解冻1已解冻2解冻失败3解冻执行中",allowEmptyValue=true,example="",allowableValues="")
	String unfreezeStatus;
	
	@ApiModelProperty(notes="执行解冻时间",allowEmptyValue=true,example="",allowableValues="")
	Date exeUnfreezeTime;
	
	@ApiModelProperty(notes="付款方记账机构",allowEmptyValue=true,example="",allowableValues="")
	String companyId;
	
	@ApiModelProperty(notes="收款方记账机构",allowEmptyValue=true,example="",allowableValues="")
	String incomeCompanyId;
	
	@ApiModelProperty(notes="收款方userid对应的机构编号",allowEmptyValue=true,example="",allowableValues="")
	String incomeBranchId;
	
	@ApiModelProperty(notes="付款流水号(内部生成，传给第三方原样传回，如果不正确，不允许更新数据库，防止作弊)",allowEmptyValue=true,example="",allowableValues="")
	String payId;
	
	@ApiModelProperty(notes="第三方付款事务号(由第三方系统产生)",allowEmptyValue=true,example="",allowableValues="")
	String tranId;
	
	@ApiModelProperty(notes="收款银行卡号",allowEmptyValue=true,example="",allowableValues="")
	String bankCardCode;
	
	@ApiModelProperty(notes="付款时间",allowEmptyValue=true,example="",allowableValues="")
	Date payTime;
	
	@ApiModelProperty(notes="订单类型0-电商商城商品，1-应用模块使用购买，2-vip购买会员，3-任务相关的保证金、佣金、分享赚佣金、加急热搜金额等、4-服务商付款服务保障押金",allowEmptyValue=true,example="",allowableValues="")
	String otype;
	
	@ApiModelProperty(notes="订单状态",allowEmptyValue=true,example="",allowableValues="")
	String orderStatus;
	
	@ApiModelProperty(notes="扩展参数JSON字符串",allowEmptyValue=true,example="",allowableValues="")
	String extParams;
	
	@ApiModelProperty(notes="付款方ip地址",allowEmptyValue=true,example="",allowableValues="")
	String ip;
	
	@ApiModelProperty(notes="订单名称",allowEmptyValue=true,example="",allowableValues="")
	String name;
	
	@ApiModelProperty(notes="第三方下单时间",allowEmptyValue=true,example="",allowableValues="")
	Date prepayTime;

	/**
	 *交易流水号
	 **/
	public PaymentSeq(String id) {
		this.id = id;
	}
    
    /**
     * 客户付款交易流水
     **/
	public PaymentSeq() {
	}

}