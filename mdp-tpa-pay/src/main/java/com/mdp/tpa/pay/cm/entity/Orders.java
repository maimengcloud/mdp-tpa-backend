package com.mdp.tpa.pay.cm.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.aspectj.weaver.ast.Or;

import java.math.BigDecimal;
import java.util.Date;

@ApiModel(description="订单数据明细")
@Data
public class Orders {


    public Orders(String id){
        this.id=id;
    }

    public Orders(){

    }

    /**
     * 名称
     */
    @ApiModelProperty(notes="名称",allowEmptyValue=true,example="",allowableValues="")
    String name;

    /**
     * 订单编号
     */
    @ApiModelProperty(notes="订单编号",allowEmptyValue=true,example="",allowableValues="")
    String id;

    /**
     * 付款人编号-客户编号
     */
    @ApiModelProperty(notes="付款人编号-客户编号",allowEmptyValue=true,example="",allowableValues="")
    String userid;

    /**
     * 付款人姓名-客户姓名
     */
    @ApiModelProperty(notes="付款人姓名-客户姓名",allowEmptyValue=true,example="",allowableValues="")
    String username;

    /**
     * 订单状态
     * 0-待支付，1-已支付
     */
    @ApiModelProperty(notes="订单状态 0-待支付，1-已支付",allowEmptyValue=true,example="",allowableValues="")
    String orderStatus;

    /**
     * 付款状态
     * 0-未支付，1-已支付
     */
    @ApiModelProperty(notes="付款状态0-未支付，1-已支付",allowEmptyValue=true,example="",allowableValues="")
    String payStatus;

    /**
     * 支付时间
     */
    @ApiModelProperty(notes="支付时间",allowEmptyValue=true,example="",allowableValues="")
    Date payTime;

    /**
     * 付款类型  0-余额支付，1-微信，2-支付宝，3-银联，4-手工
     */
    @ApiModelProperty(notes="付款类型  0-余额支付，1-微信，2-支付宝，3-银联，4-手工",allowEmptyValue=true,example="",allowableValues="")
    String payType;

    /**
     * 订单类型0-电商商城商品，1-应用模块使用购买，2-vip购买会员，3-任务相关的保证金、佣金、分享赚佣金、加急热搜金额等、4-服务商付款服务保障押金
     */
    @ApiModelProperty(notes="订单类型0-电商商城商品，1-应用模块使用购买，2-vip购买会员，3-任务相关的保证金、佣金、分享赚佣金、加急热搜金额等、4-服务商付款服务保障押金",allowEmptyValue=true,example="",allowableValues="")
    String otype;

    /**
     * 付款流水号(内部生成，传给第三方原样传回，如果不正确，不允许更新数据库，防止作弊)
     */
    @ApiModelProperty(notes="付款流水号(内部生成，传给第三方原样传回，如果不正确，不允许更新数据库，防止作弊)",allowEmptyValue=true,example="",allowableValues="")
    String payId;

    /**
     * 第三方系统如微信支付中的订单号，支付宝支付中的订单号
     */
    @ApiModelProperty(notes="第三方系统如微信支付中的订单号，支付宝支付中的订单号",allowEmptyValue=true,example="",allowableValues="")
    String prepayId;

    /**
     * 第三方支付系统响应码
     */
    @ApiModelProperty(notes="第三方支付系统响应码",allowEmptyValue=true,example="",allowableValues="")
    String payCode;

    /**
     * 第三方系统下单时间
     */
    @ApiModelProperty(notes="第三方系统下单时间",allowEmptyValue=true,example="",allowableValues="")
    Date prepayTime;

    /**
     * 支付金额
     */
    @ApiModelProperty(notes="支付金额",allowEmptyValue=true,example="",allowableValues="")
    BigDecimal payAt;

    /**
     * 备注
     */
    @ApiModelProperty(notes="备注",allowEmptyValue=true,example="",allowableValues="")
    String remarks;

}
