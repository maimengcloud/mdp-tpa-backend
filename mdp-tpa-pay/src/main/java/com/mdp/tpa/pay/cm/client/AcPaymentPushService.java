package com.mdp.tpa.pay.cm.client;

import com.mdp.core.entity.Tips;
import com.mdp.mq.queue.Push;
import org.apache.commons.lang3.RandomStringUtils;
import org.slf4j.MDC;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.Map;
/**
 * 给会计系统推送付款记录(不包括充值)
 */
@Service
public class AcPaymentPushService {


    public static String PAYMENT_QUEUE_KEY="TP-PAY->AC-CORE-PAYMENT";

    public static String RECHARGE_QUEUE_KEY="TP-PAY->AC-CORE-RECHARGE";

    @Autowired
    Push push;

    /**
     * 给会计系统推送付款记录(不包括充值)
     * @param orders {action:createOrder/updatePrepayId/payNotify,payId:付款流水号必须,id:'订单编号必须',otype:'订单类型'}
     */
    public void pushPaymentOrders(Map<String,Object> orders){
        String gloNo=MDC.get("gloNo");
        if(!StringUtils.hasText(gloNo)){
            gloNo= RandomStringUtils.randomNumeric(15);
        }
        orders.put("gloNo",gloNo);
        push.leftPush(PAYMENT_QUEUE_KEY,orders);
    }

    /**
     * 给会计系统推送充值记录
     * @param orders {action:createOrder/updatePrepayId/payNotify,payId:付款流水号必须,id:'订单编号必须',otype:'订单类型 充值为5'}
     */
    public void pushRechargeOrders(Map<String,Object> orders){
        String gloNo=MDC.get("gloNo");
        if(!StringUtils.hasText(gloNo)){
            gloNo= RandomStringUtils.randomNumeric(15);
        }
        orders.put("gloNo",gloNo);
        push.leftPush(RECHARGE_QUEUE_KEY,orders);
    }
}
