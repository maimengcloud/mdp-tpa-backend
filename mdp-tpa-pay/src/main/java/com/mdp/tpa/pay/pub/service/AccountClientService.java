package com.mdp.tpa.pay.pub.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mdp.core.entity.Tips;
import com.mdp.core.err.BizException;
import com.mdp.core.utils.BaseUtils;
import com.mdp.micro.client.CallBizService;
import com.mdp.tpa.pay.pub.entity.Account;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class AccountClientService {

	@Autowired
	CallBizService callBizService;
	
	@Autowired
	ObjectMapper om;
	
	@Value("${mdp.api-gate:http://gate}")
	String apiGate="";
	
	Log log=LogFactory.getLog(AccountClientService.class);
	
	/**
	 * 根据用户编号查询该用户下的预收账款账号
	 */
	public Account queryPersonalDepositAccount(String userid) {
		Map<String,Object> m= callBizService.getForMap(apiGate+"/accore/accore/acct/account/queryPersonalDepositAccount?userid="+userid);
		Tips tips= (Tips) m.get("tips");
		if(!tips.isOk()){
			throw new BizException(tips);
		}
		return om.convertValue(m.get("data"), Account.class); 
	}
	
	/**
	 * 根据用户编号查询该用户下的所有账户
	 */
	public List<Account> queryAccounts(String userid) {
		Map<String,Object> m= callBizService.getForMap(apiGate+"/accore/accore/acct/account/queryAccounts?userid="+userid);
		Tips tips= BaseUtils.mapToTips(m);
		if(!tips.isOk()){
			throw new BizException(tips);
		}
		List<Map<String,Object>> alm=(List<Map<String, Object>>) m.get("data");
		List<Account> al=new ArrayList<>();
		for (Map<String, Object> map : alm) {
			al.add(om.convertValue(map, Account.class));
		}
		return al; 
	}
	
	/**
	 * 根据机构编号查询该机构下的预收账款账号
	 */
	public Account queryCompanyDepositAccount(String branchId,String companyId) {
		Map<String,Object> m= callBizService.getForMap(apiGate+"/accore/accore/acct/account/queryCompanyDepositAccount?branchId="+branchId+"&companyId="+companyId);
		Tips tips= BaseUtils.mapToTips(m);
		if(!tips.isOk()){
			throw new BizException(tips);
		}
		return om.convertValue(m.get("data"), Account.class); 
	}

	
	/**
	 * 根据账户编号查找账号
	 * @return
	 */
	public Account queryAccountById(String accountId) {
		Map<String,Object> m= callBizService.getForMap(apiGate+"/accore/accore/acct/account/queryAccountById?accountId="+accountId);
		Tips tips= BaseUtils.mapToTips(m);
		if(!tips.isOk()){
			throw new BizException(tips);
		}
		return om.convertValue(m.get("data"), Account.class); 
	}
	
	/**
	 * 开立公司充值账户-预收账款
	 * @param branchId 云机构编号
	 * @param companyId 公司编号
	 * @param companyName 公司名称
	 * @return
	 */
	Account openCompanyDepositAccount(String branchId,String companyId,String companyName) {
	   	 Map<String,Object> request=new HashMap<>();
	   	 request.put("branchId",branchId);
	   	 request.put("companyId",companyId);
	   	 request.put("companyName", companyName);
	   	 

      Map<String,Object> rest=callBizService.postForMap(apiGate+"/accore/accore/acct/account/openCompanyDepositAccount",request);
     
		Tips tips= (Tips) rest.get("tips");
		if(!tips.isOk()){
			throw new BizException(tips);
		}
		return om.convertValue(rest.get("data"), Account.class);
	}
	/**
	 * 开立个人账户
	 * @param branchId 科目编号
	 * @param userid 用户编号
	 * @param username 用户名称
	 * @param shopId 商户编号
	 * @param locationId 门店编号
	 * @return
	 */
	public Account openPersonalDepositAccount(String branchId,String userid,String username,String shopId,String locationId) {
	   	 Map<String,Object> request=new HashMap<>();
	   	 request.put("branchId",branchId);
	   	 request.put("userid",userid);
	   	 request.put("username", username);
	   	 request.put("shopId",shopId); // 商户编号
	   	 request.put("locationId",locationId); // 门店编号

		Map<String,Object> rest = callBizService.postForMap(apiGate+"/accore/accore/acct/account/openPersonalDepositAccount",request);
    
		Tips tips= (Tips) rest.get("tips");
		if(!tips.isOk()){
			throw new BizException(tips);
		}
		return om.convertValue(rest.get("data"), Account.class);
	}
}
