package com.mdp.tpa.pay.pub.service;

import org.springframework.stereotype.Service;

@Service("wxpub.payCacheService")
public class PayCacheService extends TpaPayCacheService {

	@Override
	public String getCacheKey() {
		// TODO Auto-generated method stub
		return "pay_order_cache";
	}

	@Override
	public void put(String key, Object value) {

	}

	@Override
	public Object get(String key) {
		return null;
	}

	@Override
	public boolean containsValue(Object value) {
		return false;
	}
}
