package com.mdp.tpa.pay.cm.entity;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiOperation;
import lombok.Data;
import springfox.documentation.annotations.ApiIgnore;

import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;

@ApiModel(description="订单统一下单接口")
@Data
public class UniOrder {

    @ApiModelProperty(notes="订单编号",allowEmptyValue=true,example="",allowableValues="")
    String id;



    @ApiModelProperty(notes="订单类型0-电商商城商品，1-应用模块使用购买，2-vip购买会员，3-任务相关的保证金、佣金、分享赚佣金、加急热搜金额等、4-服务商付款服务保障押金、5-充值",allowEmptyValue=true,example="",allowableValues="")
    String otype;


    @ApiModelProperty(notes="支付成功跳转页面",allowEmptyValue=true,example="",allowableValues="")
    String returnUrl;

    @ApiModelProperty(notes="充值金额，当otype=5时必填",allowEmptyValue=true,example="",allowableValues="")
    BigDecimal rechargeAt;
}
