package com.mdp.tpa.pay.wxpay.entity;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;

@Data
@ApiModel("企业付款接口")
public class BizzPayVo {


    @ApiModelProperty(notes="微信openid",allowEmptyValue=true,example="",allowableValues="")
    String openid;

    @ApiModelProperty(notes="付款金额",allowEmptyValue=true,example="",allowableValues="")
    Double payAt;

    @ApiModelProperty(notes="付款描述",allowEmptyValue=true,example="",allowableValues="")
    String desc;
}
