package com.mdp.tpa.pay.wxpay.service;

import com.mdp.core.api.CacheHKVService;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.concurrent.TimeUnit;

@Service
public class RedisCacheLock {

    Log logger= LogFactory.getLog(this.getClass());

    @Autowired
    private CacheHKVService cacheHKVService;

    public void setKeyValueTimeOut(String key,String value,Long minutes){
        //设置值
        cacheHKVService.setValue(key,value,minutes,TimeUnit.MINUTES);

    }

    public String getKey(String key){
        //获取值
        return (String) cacheHKVService.getValue(key);
    }

    public boolean containKey(String key){
        return cacheHKVService.containsKey(key);
    }
}
