package com.mdp.tpa.pay.paypal.entity;

/**
 * 收款方式
 *  credit_card：信用卡收款
 *  paypal：余额收款
 */
public enum PaypalPaymentMethod {
    credit_card,
    paypal
}