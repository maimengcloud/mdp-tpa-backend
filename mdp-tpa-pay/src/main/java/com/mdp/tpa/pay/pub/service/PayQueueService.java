package com.mdp.tpa.pay.pub.service;

import com.mdp.mq.queue.Pop;
import com.mdp.mq.queue.Push;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service("wxpub.payQueueService")
public class PayQueueService {
	
	@Autowired
	Push push;
	
	@Autowired
	Pop pop;
	
	Log log=LogFactory.getLog(PayQueueService.class);
	
	/**
	 * 压支付订单
	 * @param shopId 订单归属商户
	 * @param locationId 订单门店编号,总部店为0
	 * @param orderId	订单id
	 * @param orderSn	订单编号
	 * @param userid	用户编号
	 * @param actualPrice	实际付款金额
	 * @param remark	留言
	 */
	public void pushOrderPay(String shopId,String locationId,String orderId,String orderSn,String userid,BigDecimal totalAmount,String remark){ 
		if(!StringUtils.isEmpty(locationId)) {
			Map<String,Object> map = new HashMap<>();	
			map.put("orderId", orderId);
			map.put("orderSn", orderSn);
			map.put("userid", userid);
			map.put("actualPrice", totalAmount);
			map.put("remark", remark);
			map.put("shopId",shopId);
			push.leftPush( "pay_order_"+shopId+"_"+locationId,map); 
		}	
	}  
	
	/**
	 * 弹支付订单
	 * @param shopId 订单归属商户
	 * @param locationId 订单门店编号,总部店为0
	 * @return
	 */
	public  List<Map<String,Object>> popPayOrder(String shopId,String locationId){
		List<Map<String,Object>> range = new ArrayList<>();
		if(!StringUtils.isEmpty(locationId)) {
			String key = "pay_order_"+shopId+"_"+locationId;
			Long size = pop.size(key);
			range = pop.range(key, 0, size);
			for(int i = 0;i<size;i++) {
				pop.rightPop(key);
			}
		}
		return range;
	}
	
}
