package com.mdp.tpa.pay.wxpay.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum WxNotifyType {

	/**
	 * 支付通知
	 */
	NATIVE_NOTIFY("/tpa/pay/wxpay/native/notify"),


	/**
	 * 退款结果通知，暂无此接口
	 */
	REFUND_NOTIFY("/tpa/pay/wxpay/refunds/notify");

	/**
	 * 类型
	 */
	private final String type;
}
