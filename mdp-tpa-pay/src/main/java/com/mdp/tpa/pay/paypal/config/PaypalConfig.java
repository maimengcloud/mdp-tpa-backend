package com.mdp.tpa.pay.paypal.config;

import com.paypal.base.Constants;
import com.paypal.base.rest.APIContext;
import com.paypal.base.rest.PayPalRESTException;
import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * 配置类，注入PayPal需要的认证信息
 */
@Configuration
@Data
public class PaypalConfig {
    @Value("${paypal.client.app}")
    private String clientId;
    @Value("${paypal.client.secret}")
    private String clientSecret;
    @Value("${paypal.mode}")
    private String mode;
    @Value("${paypal.webscr}")
    private String webscr;
    @Value("${paypal.webhook-id}")
    private String webhookId ;



    /**
     * 创建支付回调地址参数
     */
    public static final String SUCCESS_URL = "success";  // 成功回调地址PDT
    public static final String CANCEL_URL = "cancel";    // 取消回调地址PDT

    /**
     * IPN异步验证返回
     */
    public static final String PAYMENT_IPN_VERIFIED = "VERIFIED";
    public static final String PAYMENT_IPN_COMPLETED_STATUS = "COMPLETED_STATUS";
    public static final String PAYMENT_IPN_REFUNDED_STATUS = "REFUNDED_STATUS";
    public static final String PAYMENT_IPN_INVALID = "INVALID";

    /**
     * IPN异步通知返回通知消息类型
     */
    public static final String PAYMENT_STATUS_PENDING = "PENDING";
    public static final String PAYMENT_STATUS_VOIDED = "VOIDED ";
    public static final String PAYMENT_STATUS_COMPLETED = "COMPLETED ";
    public static final String PAYMENT_STATUS_REFUNDED = "REFUNDED ";

    /**
     * APP的认证信息
     * clientId、Secret，开发者账号创建APP时提供
     */
    @Bean
    public APIContext apiContext() throws PayPalRESTException {
        APIContext apiContext = new APIContext(clientId, clientSecret, mode);
        // Set the webhookId that you received when you created this webhook.
        apiContext.addConfiguration(Constants.PAYPAL_WEBHOOK_ID, webhookId);
        return apiContext;
    }
}