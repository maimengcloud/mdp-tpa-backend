package com.mdp.tpa.pay.cm.ctrl;

import com.mdp.core.entity.Result;
import com.mdp.core.entity.Tips;

import com.mdp.tpa.pay.cm.entity.Orders;
import com.mdp.tpa.pay.cm.entity.UniOrder;
import com.mdp.tpa.pay.cm.service.OrdersService;
import com.mdp.safe.client.entity.User;
import com.mdp.safe.client.utils.LoginUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

@RestController("mall.tpa.pub.PayStatusController")
@RequestMapping(value="/**/tpa/pay")
public class PayStatusController {

    @Autowired
    OrdersService ordersService;
    /**
     * 查询订单支付状态
     */
    @RequestMapping(value="/payStatus",method= RequestMethod.GET)
    public Map<String,Object> payStatus(@RequestParam UniOrder order) {

        User user= LoginUtils.getCurrentUserInfo();
        String outTradeNo= order.getId();
        if(!StringUtils.hasText(outTradeNo)){
            return Result.error("outTradeNo-is-null","订单编号不能为空");
        }
        Orders orders=this.ordersService.getOrdersById(order.getOtype(),order.getId());
        if(orders==null){
            return Result.error("orders-is-not-exists","订单不存在");
        }else if(!orders.getUserid().equals(user.getUserid())){
            return Result.error("orders-is-not-yours","订单不属于您的");
        } else {
            Tips tips=new Tips("查询成功");
            Map<String,Object> m=new HashMap<>();
            m.put("tips",tips);
            Map<String,Object> data=new HashMap<>();
            data.put("payStatus",orders.getPayStatus());
            data.put("payTime",orders.getPayTime());
            m.put("data",data);
            return m;

        }
    }


}
