package com.mdp.tpa.pay.cm.client;

import com.mdp.core.entity.Tips;
import com.mdp.core.err.BizException;
import com.mdp.core.utils.BaseUtils;
import com.mdp.micro.client.CallBizService;
import com.mdp.tpa.pay.cm.entity.PaymentSeq;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static com.mdp.core.utils.BaseUtils.map;

@Service
public class AcClient {

    @Autowired
    CallBizService callBizService;

    /**
     * 查询1800分钟-30分钟前的未支付订单
     * @return
     */
   public List<PaymentSeq> overtimeNoPayList(){
        Map<String,Object> result=callBizService.getForMap("/accore/tpa/paymentSeq/overtimeNoPayList",map());
        Tips tips= (Tips) result.get("tips");
        if(tips.isOk()){
            List<Map<String,Object>> dataList= (List<Map<String, Object>>) result.get("data");
            if(dataList==null || dataList.size()==0){
                return new ArrayList<>();
            }else{
                return dataList.stream().map(k-> BaseUtils.fromMap(k,PaymentSeq.class)).collect(Collectors.toList());
            }
        }else{
            throw new BizException(tips);
        }

    }
}
