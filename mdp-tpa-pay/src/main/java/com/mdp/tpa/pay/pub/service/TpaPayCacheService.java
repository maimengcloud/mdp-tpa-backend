package com.mdp.tpa.pay.pub.service;

import com.mdp.core.api.CacheHKVService;
import com.mdp.core.api.CacheService;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.concurrent.TimeUnit;

public abstract class TpaPayCacheService<T> implements CacheService<T> {

	@Autowired
	public CacheHKVService cacheHKVService;
	
	 Log log=LogFactory.getLog(this.getClass());
	 
	public Long getDefaultTimeoutMinutes() {
		return (long) 60;
	}

	@Override
	public   void put(String key, T o) {
		String okey=getExpireCacheKey(key);
		cacheHKVService.expire(okey, getDefaultTimeoutMinutes(), TimeUnit.MINUTES);
		cacheHKVService.put(okey,key, o);
		
	}

	@Override
	public  T get(String key) {
		String okey=getExpireCacheKey(key);
		return  (T) cacheHKVService.get(okey,key);
	}

	@Override
	public void remove(String key) {
		cacheHKVService.delete(getExpireCacheKey(key), key);
	}

	@Override
	public boolean containsKey(String key) {
		// TODO Auto-generated method stub
		return cacheHKVService.containsKey(getExpireCacheKey(key), key);
	}

	@Override
	public void refresh() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean containsValue(T value) {
		// TODO Auto-generated method stub
		return false;
	}
	
	public abstract  String getCacheKey() ;
	  
	@Override
	public boolean expire(long milliseconds) {
		// TODO Auto-generated method stub
		//return redisTemplate.expire(getCacheKey(), milliseconds, TimeUnit.MILLISECONDS);
		return false;
	} 
	
	public boolean expire(String key) {
		// TODO Auto-generated method stub
		return cacheHKVService.expire(getExpireCacheKey(key), getDefaultTimeoutMinutes(), TimeUnit.MILLISECONDS);
	}
	/**
	 * 通过订单编号,存入缓存
	 * @param key
	 * @return
	 */
	public String getExpireCacheKey(String key) { 
		return this.getCacheKey()+":"+key;
	}
	
}
