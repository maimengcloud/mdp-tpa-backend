package com.mdp.tpa.pay.cm.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(description="小程序订单统一下单接口")
public class MiniAppUniOrder extends UniOrder{
    /**
     * 微信openid
     */
    @ApiModelProperty(notes="微信openid",allowEmptyValue=true,example="",allowableValues="")
    String payOpenid;
}
