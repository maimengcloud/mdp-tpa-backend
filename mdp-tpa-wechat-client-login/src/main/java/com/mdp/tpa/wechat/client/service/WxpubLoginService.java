package com.mdp.tpa.wechat.client.service;

import java.util.Map;

import com.mdp.tpa.wechat.api.Oauth2AccessTokenService;
import com.mdp.tpa.wechat.api.WxUserService;
import com.mdp.tpa.wechat.api.cache.Oauth2TokenCache;
import com.mdp.tpa.wechat.config.AppProperties;
import com.mdp.tpa.wechat.entity.Oauth2AccessToken;
import com.mdp.tpa.wechat.service.cache.DefaultOauth2TokenCacheService;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
public class WxpubLoginService implements InitializingBean{
	Log log=LogFactory.getLog(WxpubLoginService.class);

	
	@Value("${mdp.wx.wxpub-auth-url:https://open.weixin.qq.com/connect/oauth2/authorize?appid=%s&redirect_uri=%s&response_type=code&scope=snsapi_base&state=123#wechat_redirect}")
	public String wxpubAuthUrl="";
	
	
	@Autowired
	Oauth2AccessTokenService wxApiOauth2AccessToken;
	
	@Autowired
	WxUserService wxApiUser;
	
	@Autowired(required=false)
	Oauth2TokenCache cacheService=new DefaultOauth2TokenCacheService();
	
	/***
	 * 根据code获取微信用户基本信息
	 * {country=中国, unionid=otoXpv_1CmMZYg7gbrm7EWuKdBOc, province=广东, city=广州, openid=o4lAmuHOrN1IWrloisfvQzaxjPAM, sex=1, nickname=陈裕财, headimgurl=http://wx.qlogo.cn/mmopen/vi_32/rhQjvS0AwXKZhK02y8hTiaEf3Lld3psJuMYqLgWY3B4UbviaAR8nTjp8g36pNJN4iaLkgECp4AavNRAXYRWC55FyQ/0, language=zh_CN, privilege=[]}
	 */
	public Map<String,Object> getSnsUserInfoByOpenid(Oauth2AccessToken token){
 
		Map<String,Object> data=wxApiUser.getSnsUserinfo( token.getOpenid());
		return data;
		//Assert.assertEquals(true, data.containsKey("openid")||"陈裕财".equals(data.get("nickname")));
	}
	/***
	 * 根据code获取微信用户基本信息
	 * {access_token=BSQYVUKUTeqNBBb9isdNDv94GMFr0sovkJqHLM6Hgo6mlcc9FDYsous_ZrEcJeeq0J-V3MX844XijW-Y4OY1MA, refresh_token=eVIvgXCBaBRhebbfvFMCJLzP2q5ZRPTsAoxQImoCEjilAfjBXw2hNtvUUTwnbY1L2ISLCHSnkcjaZMwh1_Z34w, unionid=otoXpv_1CmMZYg7gbrm7EWuKdBOc, openid=o4lAmuHOrN1IWrloisfvQzaxjPAM, scope=snsapi_userinfo, expires_in=7200}
	 */
	public  Oauth2AccessToken getOauth2AccessToken(AppProperties appProperties, String code){
		Oauth2AccessToken token=wxApiOauth2AccessToken.getSnsOauth2AccessToken(appProperties, code);
		return token; 
		//Assert.assertEquals(true, data.containsKey("openid")||"陈裕财".equals(data.get("nickname")));
	}
	
	/***
	 * 根据code获取微信用户基本信息
	 * {access_token=BSQYVUKUTeqNBBb9isdNDv94GMFr0sovkJqHLM6Hgo6mlcc9FDYsous_ZrEcJeeq0J-V3MX844XijW-Y4OY1MA, refresh_token=eVIvgXCBaBRhebbfvFMCJLzP2q5ZRPTsAoxQImoCEjilAfjBXw2hNtvUUTwnbY1L2ISLCHSnkcjaZMwh1_Z34w, unionid=otoXpv_1CmMZYg7gbrm7EWuKdBOc, openid=o4lAmuHOrN1IWrloisfvQzaxjPAM, scope=snsapi_userinfo, expires_in=7200}
	 */
	public  Oauth2AccessToken getOauth2AccessToken(Oauth2AccessToken token){  
		Oauth2AccessToken token2=cacheService.get(token.getAccessToken()); 
		return token2; 
	}
	@Override
	public void afterPropertiesSet() throws Exception {
		// TODO Auto-generated method stub
		wxApiOauth2AccessToken.setOauth2AccessTokenCache(cacheService); 
	}

}
