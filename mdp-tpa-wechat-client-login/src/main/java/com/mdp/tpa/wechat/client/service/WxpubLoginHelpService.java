package com.mdp.tpa.wechat.client.service;

import com.mdp.core.entity.Result;
import com.mdp.core.err.BizException;
import com.mdp.core.utils.LogUtils;
import com.mdp.safe.client.jwt.JwtLocalLoginService;
import com.mdp.safe.client.pwd.SafePasswordEncoder;
import com.mdp.tpa.pub.entity.SysUser;
import com.mdp.tpa.pub.entity.SysUserTpa;
import com.mdp.tpa.pub.service.UserDataBaseService;
import com.mdp.tpa.wechat.client.miniapp.login.service.WechatMiniAppLoginHelpService;
import com.mdp.safe.client.cache.TpaCodeRedisCacheService;
import com.mdp.safe.client.dict.AuthType;
import com.mdp.core.entity.Tips;
import com.mdp.safe.client.entity.CommonUserDetails;
import com.mdp.safe.client.entity.User;
import com.mdp.safe.client.utils.LoginUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.oauth2.client.registration.ClientRegistration;
import org.springframework.security.oauth2.client.registration.ClientRegistrationRepository;
import org.springframework.security.oauth2.core.endpoint.MapOAuth2AccessTokenResponseConverter;
import org.springframework.security.oauth2.core.endpoint.OAuth2AccessTokenResponse;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.web.reactive.function.client.WebClientResponseException;

import java.nio.charset.Charset;
import java.util.*;

@Service
public class WxpubLoginHelpService {


    Logger logger = LoggerFactory.getLogger(WechatMiniAppLoginHelpService.class);

    @Autowired
    JwtLocalLoginService jwtLoginService;

    @Autowired
    ClientRegistrationRepository clientRegistrationRepository;

    @Autowired
    UserDataBaseService userDataBaseService;


    PasswordEncoder passwordEncoder=new SafePasswordEncoder();


    MapOAuth2AccessTokenResponseConverter converter=new MapOAuth2AccessTokenResponseConverter();


    @Autowired
    TpaCodeRedisCacheService tpaCodeRedisCacheService;







    public Result loginByWechatWxpub(String openid, String code, String userid, String phoneno, String unionid) {
         try {
        ClientRegistration client = clientRegistrationRepository.findByRegistrationId("client-login");
        Map<String,Object> params=new HashMap<>();
        params.put("userloginid",openid);//userid,openid,phoneno,email等
        params.put("password",code);//密码，短信验证码，微信登陆时对应的code等
        params.put("userid",userid);//密码，短信验证码，微信登陆时对应的code等
        params.put("authType", AuthType.wechat_wxpub);//passwod，sms,email,wechat_mini_app等
        params.put("grantType","password");
        params.put("phoneno",phoneno);
        params.put("unionid",unionid);

        params.put("gloNo", LogUtils.getGloNo(true));//全局跟踪号
        tpaCodeRedisCacheService.put(code,code);
        ClientRegistration finalClient = client;
        Map messages = WebClient.create()
                .post()
                .uri(client.getProviderDetails().getTokenUri()+"?grant_type={grantType}&auth_type={authType}&userloginid={userloginid}&password={password}&userid={userid}&phoneno={phoneno}&unionid={unionid}&gloNo={gloNo}",params)
                .headers(h->{
                    h.add("Authorization","Basic "+ Base64.getEncoder().encodeToString((finalClient.getClientId()+":"+ finalClient.getClientSecret()).getBytes(Charset.forName("UTF-8"))));
                })
                .retrieve()
                .bodyToMono(Map.class)
                .block();
        OAuth2AccessTokenResponse response=converter.convert(messages);
         Tips tips = jwtLoginService.doLocalLoginUseJwtToken(response.getAccessToken().getTokenValue());
         Result.assertIsFalse(tips);
        User user=LoginUtils.getCurrentUserInfo();
         CommonUserDetails userDetails=jwtLoginService.getUserDetails();
         tpaCodeRedisCacheService.remove(code);
        return Result.ok().setData(response).put("userInfo",user).put("roles",userDetails.getAuthorities());
         }catch ( WebClientResponseException.Unauthorized e1){
            logger.error("账户密码错误",e1);
            return Result.error("accountId-or-password-error","账户或者密码错误").put("moreMsg",e1.getMessage());
        }catch (WebClientResponseException.InternalServerError e1){
            logger.error("服务器错误",e1);
              return Result.error("server-error","服务器错误").put("moreMsg",e1.getMessage());
        }catch (WebClientResponseException.BadGateway e1){
            logger.error("网关错误",e1);
            return Result.error("gate-error","网关错误").put("moreMsg",e1.getMessage());
         }catch (WebClientResponseException.BadRequest e1){
            logger.error("不正确的请求",e1);
            return Result.error("request-error","不正确的请求").put("moreMsg",e1.getMessage());
        }catch (BizException e1){
            logger.error("",e1);
            return Result.error(e1);
        }catch (Exception e1){
            logger.error("获取令牌错误",e1);
            return Result.error("get-token-error","获取令牌错误").put("moreMsg",e1.getMessage());
        }

    }
    @Transactional
    public SysUser autoRegisterByUserTpa(String inviteId,SysUserTpa userTpa, boolean tpadbExists){
        return userDataBaseService.registerUserByTpa(inviteId,userTpa,tpadbExists);
    }


    public SysUserTpa wechatUserInfoConvertToUserTpa(Map<String,Object> wechatUserInfo){
        SysUserTpa tpa=new SysUserTpa();
        tpa.setUnionid((String) wechatUserInfo.get("unionid"));
        tpa.setOpenid((String) wechatUserInfo.get("openid"));
        tpa.setCountry((String) wechatUserInfo.get("country"));
        tpa.setUsername((String) wechatUserInfo.get("nickname"));
        tpa.setNickname((String) wechatUserInfo.get("nickname"));
        tpa.setProvince((String) wechatUserInfo.get("province"));
        tpa.setCity((String) wechatUserInfo.get("city"));
        tpa.setHeadimgurl((String) wechatUserInfo.get("headimgurl"));

        if(wechatUserInfo.containsKey("sex")){
            tpa.setGender(wechatUserInfo.get("sex")+"");
        }
        return tpa;
    }

    public SysUser invite(String state, SysUserTpa tpa, boolean tpadbExists) {
         return userDataBaseService.invite(state,tpa,tpadbExists);
    }

    public SysUser bind(String state, SysUserTpa tpa, boolean tpadbExists) {
         return userDataBaseService.bind(state,tpa,tpadbExists);
    }

    public SysUser claim(String state, SysUserTpa tpa, boolean tpadbExists) {
        return userDataBaseService.claim(state,tpa,tpadbExists);
    }
}
