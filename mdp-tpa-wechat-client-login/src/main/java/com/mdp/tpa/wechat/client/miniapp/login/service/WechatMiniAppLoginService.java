package com.mdp.tpa.wechat.client.miniapp.login.service;

import java.util.Map;

import com.mdp.tpa.wechat.api.WxaService;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class WechatMiniAppLoginService  {
	
	private Log log=LogFactory.getLog(WechatMiniAppLoginService.class);
	 
	@Autowired
	WxaService wxApiWxa;

	/**
	 * @param code
	 * @param appid 
	 * @param appSecret
	 * @return {openid=oBi4O0dwNZOYJ5HP4REmTnhL05D0, session_key=PtSra+OdMH0/67OtuFK0jg==, expires_in=7200}
	 */
	public Map<String,Object> jscode2session(String appid,String appSecret,String code){ 
		return wxApiWxa.jscode2session(appid,appSecret,code);
	}

}
