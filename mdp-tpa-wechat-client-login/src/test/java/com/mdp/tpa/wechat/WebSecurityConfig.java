package com.mdp.tpa.wechat;


import com.mdp.safe.client.pwd.SafePasswordEncoder;
import org.springframework.context.annotation.Bean;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.password.PasswordEncoder;

@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {


    @Bean
    public PasswordEncoder passwordEncoder() {
        return new SafePasswordEncoder();
    }

    @Override
    public void configure(WebSecurity web) throws Exception {
        web.ignoring().antMatchers("/webjars/**");
    }

    /**
     * 允许匿名访问所有接口 主要是 oauth 接口
     * @param http
     * @throws Exception
     */
    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.authorizeRequests().mvcMatchers("/authorize","/authorized","/","/index","/oauth2/login/token","/oauth2/login/token/web").permitAll().and().oauth2Client().and().logout().disable();
        http.authorizeRequests().antMatchers("/actuator/**","/**/common/login","/**/common/user/reg/web","/",
                "/index.html","/login","/error","/","/index","/**/safe/app/auth**").permitAll();
        http.oauth2Login();
        http.oauth2ResourceServer().jwt();
        http.csrf().disable();
    }
}